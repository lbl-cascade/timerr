model
{

	for(i in 1:M){
	  
	  zA[i] ~ dbinom( pN[i]*RR[i], n[i] )
	  zN[i] ~ dbinom( pN[i], n[i] )
	
	  log(RR[i]) <- betaR[1] + betaR[2]*covariates1[i] + betaR[3]*covariates2[i] + alpha[i]

	  logit(pN[i]) <- betaN[1] + betaN[2]*covariates1[i] + betaN[3]*covariates2[i] + delta[i]

	}

	for(j in 1:M){

		alpha[j] ~ dnorm(0,sigmasq.alpha)
		delta[j] ~ dnorm(0,sigmasq.delta)

		}

	for(k in 1:P.R){
		betaR[k] ~ dnorm(0.0, 0.00000001)
		}

	for(k in 1:P.N){
		betaN[k] ~ dnorm(0.0, 0.00000001)
		}
		
	sigmasq.alpha <- 1/tausq.alpha
	sigmasq.delta <- 1/tausq.delta

	tausq.alpha ~ dunif(0, 10)
	tausq.delta ~ dunif(0, 10)

}