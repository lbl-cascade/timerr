#==================================================================================================
# Models for time-varying risk ratio
# Mark Risser / LBL / CASCADE postdoctoral fellowship
# September, 2015
# Test: model for 1997-2013 with only 100 simulations
#==================================================================================================

# Necessary libraries
library(ncdf4)
library(boot)
library(lme4)
library(metafor)
library(ggplot2)
library(gridExtra)
library(rjags)
library(RColorBrewer)

#==================================================================================================
# Load data: ALL and NAT model simulations only, ENSO
#==================================================================================================

setwd("~/timerr/texas_data")

# TEMPERATURE ===================================

allnc_temp <- nc_open('tas_MarAug_LBNL_CAM5-1-1degree_All-Hist_est1_v2-0_196001-201312.nc')
natnc_temp <- nc_open('tas_MarAug_LBNL_CAM5-1-1degree_Nat-Hist_CMIP5-est1_v2-0_196001-201312.nc')
all_temp <- ncvar_get(allnc_temp, 'tas', start=c(1,1), count = c(-1,-1))
nat_temp <- ncvar_get(natnc_temp, 'tas', start=c(1,1), count = c(-1,-1))

# Use only data from 1997 - 2013
startYr <- 1997
endYr <- 2013
allYrs <- startYr:endYr
indYr <- allYrs - startYr + 1 + (nrow(all_temp) - max(allYrs - startYr + 1))

# Use only non-missing model runs
modelRuns <- 1:100

all_tempUse <- all_temp[indYr,modelRuns]
nat_tempUse <- nat_temp[indYr,modelRuns]

# No standardization necessary -- using percentiles of the ALL simulations

temp_event <- quantile(all_tempUse, 0.95)

# Create matrices of event indicators
allTemp_occur <- matrix(as.numeric(all_tempUse > temp_event),nrow=nrow(all_tempUse),byrow=F)
natTemp_occur <- matrix(as.numeric(nat_tempUse > temp_event),nrow=nrow(nat_tempUse),byrow=F)

# Empirical probabilities
allTemp_probs <- rowMeans(allTemp_occur)
natTemp_probs <- rowMeans(natTemp_occur)

# PRECIPITATION =================================

allnc_precip <- nc_open('pr_MarAug_LBNL_CAM5-1-1degree_All-Hist_est1_v2-0_196001-201312.nc')
natnc_precip <- nc_open('pr_MarAug_LBNL_CAM5-1-1degree_Nat-Hist_CMIP5-est1_v2-0_196001-201312.nc')

all_precip <- ncvar_get(allnc_precip, 'pr', start=c(1,1), count = c(-1,-1))
nat_precip <- ncvar_get(natnc_precip, 'pr', start=c(1,1), count = c(-1,-1))

# Select years/models and scale to be mm/6mos (raw: m/mon)
all_precipUse <- all_precip[indYr,modelRuns]*6*1000
nat_precipUse <- nat_precip[indYr,modelRuns]*6*1000

# No standardization necessary -- using percentiles of the ALL simulations

precip_event <- quantile(all_precipUse, 0.10)

# Create matrices of event indicators
allprecip_occur <- matrix(as.numeric(all_precipUse < precip_event),nrow=nrow(all_precipUse),byrow=F)
natprecip_occur <- matrix(as.numeric(nat_precipUse < precip_event),nrow=nrow(nat_precipUse),byrow=F)

# Empirical probabilities
allprecip_probs <- rowMeans(allprecip_occur)
natprecip_probs <- rowMeans(natprecip_occur)

# ENSO ==========================================

ENSO.raw <- read.table( "http://www.cpc.ncep.noaa.gov/data/indices/sstoi.indices",
                        header = TRUE )

ENSO.index <- matrix(NA,nrow=length(allYrs),ncol=2)
for(i in 1:length(allYrs)){
  ENSO.index[i,1] <- mean(ENSO.raw$NINO3.4[ENSO.raw$YR == allYrs[i]
                                           & ENSO.raw$MON %in% 3:8])
  ENSO.index[i,2] <- mean(ENSO.raw$ANOM.3[ENSO.raw$YR == allYrs[i]
                                          & ENSO.raw$MON %in% 3:8])
}

# From Daithi: use the anomalies, averaged over March - August (i.e., ENSO.index[,2])

# Look at the average anomalies
# ENSOplot <- ggplot( data.frame( ENSO=ENSO.index[,2], year = allYrs ), 
#                     aes(x = year, y=ENSO) ) +
#   geom_line() +
#   ylab("Nino 3.4 anomaly (season average)") +
#   xlab("Year")

# pdf( file = "ENSO.pdf", width = 10, height = 4 )
# ENSOplot
# dev.off()

# Summarize empirical estimates and ENSO:
summaryDF <- data.frame(
  year = allYrs,
  zA = rowSums(allprecip_occur),
  zN = rowSums(natprecip_occur),
  pA = allprecip_probs,
  pN = natprecip_probs,
  RR = round((allprecip_probs/natprecip_probs),2),
  logRR = round(log(allprecip_probs/natprecip_probs),2),
  v = round((1-allprecip_probs)/(100*allprecip_probs) 
            + (1-natprecip_probs)/(100*natprecip_probs),2),
  ENSO = round(ENSO.index[,2],2)
)

# Scatterplots of empirical estimates vs. ENSO
# par(mfrow=c(1,3))
# plot(summaryDF$ENSO, summaryDF$pA, main="pA (red) and pN (blue) vs. Nino3.4", 
#      xlab="Nino3.4 anomaly", ylab="Event probability", col="red", ylim=c(0,0.3))
# points(summaryDF$ENSO, summaryDF$pN, pch="+", col="blue")
# plot(summaryDF$ENSO, summaryDF$logRR, main="logRR vs. Nino3.4", 
#      xlab="Nino3.4 anomaly", ylab="logRR", pch="+", ylim=c(-0.25,2.25))
# plot(summaryDF$ENSO, exp(summaryDF$logRR), main="RR vs. Nino3.4", 
#      xlab="Nino3.4 anomaly", ylab="RR", pch="+", col="darkgreen", ylim=c(0,8) )

#==================================================================================================
# Year-by-year estimates
#==================================================================================================

# Frequentist (delta method) esimates =====================
Freq_YBY_results <- data.frame(
  estimation = "Year by year",
  type = "Freq",
  year = allYrs,
  pA = allprecip_probs,
  pAlb = allprecip_probs - 1.96*sqrt(allprecip_probs*(1-allprecip_probs)/ncol(allprecip_occur)),
  pAub = allprecip_probs + 1.96*sqrt(allprecip_probs*(1-allprecip_probs)/ncol(allprecip_occur)),
  pN = natprecip_probs,
  pNlb = natprecip_probs - 1.96*sqrt(natprecip_probs*(1-natprecip_probs)/ncol(natprecip_occur)),
  pNub = natprecip_probs + 1.96*sqrt(natprecip_probs*(1-natprecip_probs)/ncol(natprecip_occur)),
  RR = allprecip_probs/natprecip_probs,
  RRlb = exp( log(allprecip_probs/natprecip_probs) 
             - 1.96*((1-allprecip_probs)/(ncol(allprecip_occur)*allprecip_probs) + 
                       (1-natprecip_probs)/(ncol(natprecip_occur)*natprecip_probs)) ),
  RRub = exp( log(allprecip_probs/natprecip_probs) 
              + 1.96*((1-allprecip_probs)/(ncol(allprecip_occur)*allprecip_probs) + 
                        (1-natprecip_probs)/(ncol(natprecip_occur)*natprecip_probs)) )
)

# Bayesian estimates ======================================

# Data: zA = count from ALL simulations ~ Bin(pA,n)
#       zN = count from NAT simulations ~ Bin(pN,n)
# Parameters to estimate: pA and pN, with priors
#       p(pA) = Beta(aA,bA), p(pN) = Beta(aN,bN)
# In this case the posteriors are
#       p(pA|zA) = Beta( aA + zA, bA + n - zA )
#       p(pN|zN) = Beta( aN + zN, bN + n - zN )

n <- ncol(allprecip_occur)
nSamp <- 10000
pAsamps <- pNsamps <- matrix(NA,nSamp,length(allYrs))

# Fixed hyperparameters (uniform priors)
aA <- bA <- aN <- bN <- 1

# Data
zA <- rowSums(allprecip_occur)
zN <- rowSums(natprecip_occur)

for(j in 1:length(allYrs)){
  pAsamps[,j] <- rbeta(n = nSamp, shape1 = aA + zA[j], shape2 = bA + n - zA[j])
  pNsamps[,j] <- rbeta(n = nSamp, shape1 = aN + zN[j], shape2 = bN + n - zN[j])
}
RRsamps <- pAsamps/pNsamps

lowerPerc <- 0.025
upperPerc <- 0.975

Bayes_YBY_results <- data.frame(
  estimation = "Year by year",
  type = "Bayes",
  year = allYrs,
  pA = round(apply(pAsamps,2,median),3),
  pAlb = round(apply(pAsamps,2,function(x){ quantile(x,lowerPerc) }),3),
  pAub = round(apply(pAsamps,2,function(x){ quantile(x,upperPerc) }),3),
  pN = round(apply(pNsamps,2,median),3),
  pNlb = round(apply(pNsamps,2,function(x){ quantile(x,lowerPerc) }),3),
  pNub = round(apply(pNsamps,2,function(x){ quantile(x,upperPerc) }),3),
  RR = round(apply(RRsamps,2,median),3),
  RRlb = round(apply(RRsamps,2,function(x){ quantile(x,lowerPerc) }),3),
  RRub = round(apply(RRsamps,2,function(x){ quantile(x,upperPerc) }),3)
)

# Compare
pA_FvB <- ggplot( rbind(Freq_YBY_results,Bayes_YBY_results),
                  aes(x = year, y = pA) ) +
  ylab("pA\n") + xlab("\nYear") +
  ggtitle("Year-by-year estimates and 95% CIs") +
  ylim(-0.05,0.38) +
  geom_pointrange( aes(x = year, y = pA, ymin = pAlb, ymax = pAub,
                       colour = type ), 
                   position = position_dodge(width=0.5) )

pN_FvB <- ggplot( rbind(Freq_YBY_results,Bayes_YBY_results),
                  aes(x = year, y = pN) ) +
  ylab("pN\n") + xlab("\nYear") +
  ggtitle("Year-by-year estimates and 95% CIs") +
  ylim(-0.05,0.38) +
  geom_pointrange( aes(x = year, y = pN, ymin = pNlb, ymax = pNub,
                       colour = type ), 
                   position = position_dodge(width=0.5) )

RR_FvB <- ggplot( rbind(Freq_YBY_results,Bayes_YBY_results),
                  aes(x = year, y = RR) ) +
  ylab("RR\n") + xlab("\nYear") +
  ggtitle("Year-by-year estimates and 95% CIs") +
  scale_y_log10(limits = c(0.1, 100), breaks = c(0.1, 1, 10, 100)) +
  geom_pointrange( aes(x = year, y = RR, ymin = RRlb, ymax = RRub,
                       colour = type ), 
                   position = position_dodge(width=0.5) )

setwd("~/timerr/method/figures")
pdf( file = "YearByYear_FvsB.pdf", width = 8, height = 12 )
grid.arrange(pA_FvB,pN_FvB,RR_FvB,ncol=1)
dev.off()


#==================================================================================================
# Pooling strength across years
#==================================================================================================

# (Frequentist) meta-analysis model for RR ================

n <- ncol(allprecip_occur)

# Plug the RR and variance estimates into a meta-analysis model
RRmm <- rma( yi = log(allprecip_probs/natprecip_probs), 
             vi =((1-allprecip_probs)/(n*allprecip_probs)
                  +(1-natprecip_probs)/(n*natprecip_probs)) )
confint(RRmm)

# With ENSO
RRmmENSO <- rma(yi = log(allprecip_probs/natprecip_probs), 
                vi = ((1-allprecip_probs)/(n*allprecip_probs)
                      +(1-natprecip_probs)/(n*natprecip_probs)),
                mods = ENSO.index[,2] )
confint(RRmmENSO)


# Full Bayesian model for RR ==============================

# Summarize empirical estimates and ENSO:
riskratioDF <- data.frame(
  year = allYrs,
  zA = rowSums(allprecip_occur),
  zN = rowSums(natprecip_occur),
  ENSO = ENSO.index[,2]
)

setwd("~/timerr/TXshort")

P.R <- 2
P.N <- 2
M <- nrow(allprecip_occur) # Number of years
n <- ncol(allprecip_occur) # Number of simulations per year

# Create a data list
dataList = list(
  "zA" = riskratioDF$zA,              # Counts for ALL forcings
  "zN" = riskratioDF$zN,              # Counts for NAT forcings
  "nino3.4" = riskratioDF$ENSO,       # ENSO covariate
  "n" = n,                            # Number of climate simulations per year
  "M" = M,                            # Number of years
  "P.R" = P.R,                        # Number of covariates for the logRR (intercept, ENSO)
  "P.N" = P.N                         # Number of covariates for the logit(pN) (intercept, ENSO)
)

# List of parameters to be monitored
parameters <- c(
  "betaR",
  "betaN",
  "alpha",
  "tausq.alpha",
  "delta",
  "tausq.delta" 
)

# Set initial values
initsValues = list(
  "betaR" = rep(0,P.R),
  "betaN" = rep(0, P.N),
  "alpha" = rep(0,M),
  "tausq.alpha" = 1,
  "delta" = rep(0,M),
  "tausq.delta" = 1	
)

# JAGS Set-up
adaptSteps <- 100000      # Number of steps to "tune" the samplers
burnInSteps <- 30000    	# Number of steps to "burn-in" the samplers
nChains <- 1              # Number of chains to run
numSavedSteps <- 20000    # Total number of steps in chains to save
thinSteps <- 1            # Number of steps to "thin" (1=keep every step)
nIter <- ceiling((numSavedSteps*thinSteps )/nChains) 	# Steps per chain

# Create, initialize, and adapt the model
# prt <- proc.time()
set.seed(463200)
jagsModel <- jags.model("RRmodel.txt", 
                        data=dataList, 
                        inits=initsValues, 
                        n.chains=nChains, 
                        n.adapt=adaptSteps)


# Burn-in the algorithm
cat( "Burning in the MCMC chain...\n")
update(jagsModel, n.iter=burnInSteps)

# Run MCMC algorithm
cat("Sampling final MCMC chain...\n" )
codaSamples <- coda.samples(jagsModel, 
                            variable.names=parameters, 
                            n.iter=nIter, 
                            thin=thinSteps)
# tot.time <- proc.time() - prt

# # Make trace plots and density plots
# par(ask=T)
# plot(codaSamples)
# # Calculate numerical summaries for the posterior samples
# summary(codaSamples)

# Retrieve posterior samples
mcmcChain <- as.matrix(codaSamples)

# Calculate the year-specific pA, pN, and risk ratios, WITH Nino3.4
yearlypA <- yearlypN <- yearlyRR <- matrix(NA,ncol=M,nrow=numSavedSteps)
for(j in 1:M){
  yearlyRR[,j] <- exp( mcmcChain[,20] + riskratioDF$ENSO[j]*mcmcChain[,21] + mcmcChain[,j] )
  yearlypN[,j] <- (exp( mcmcChain[,18] + riskratioDF$ENSO[j]*mcmcChain[,19] + mcmcChain[,j+21])
                   /(1 + exp( mcmcChain[,18] + riskratioDF$ENSO[j]*mcmcChain[,19] + mcmcChain[,j+21])))
  yearlypA[,j] <- yearlypN[,j]*yearlyRR[,j]
}

lowerPerc <- 0.025
upperPerc <- 0.975

resultsDF <- data.frame(
  estimation = "Pooled",
  type = "Bayes",
  year = allYrs,
  pA = round(apply(yearlypA,2,median),3),
  pAlb = round(apply(yearlypA,2,function(x){ quantile(x,lowerPerc) }),3),
  pAub = round(apply(yearlypA,2,function(x){ quantile(x,upperPerc) }),3),
  pN = round(apply(yearlypN,2,median),3),
  pNlb = round(apply(yearlypN,2,function(x){ quantile(x,lowerPerc) }),3),
  pNub = round(apply(yearlypN,2,function(x){ quantile(x,upperPerc) }),3),
  RR = round(apply(yearlyRR,2,median),3),
  RRlb = round(apply(yearlyRR,2,function(x){ quantile(x,lowerPerc) }),3),
  RRub = round(apply(yearlyRR,2,function(x){ quantile(x,upperPerc) }),3)
)

# Visualize results
pAplot <- ggplot(resultsDF, aes(x=year, y=pAmed)) +
  ylab("pA\n") + xlab("\nYear") +
  ggtitle("Year-specific 95% credible intervals") +
  ylim(0,0.35) +
  geom_pointrange(aes(x = year, y = pAmed, ymin = pAlb, ymax = pAub))

pNplot <- ggplot(resultsDF, aes(x=year, y=pNmed)) +
  ylab("pN\n") + xlab("\nYear") +
  ggtitle("Year-specific 95% credible intervals") +
  ylim(0,0.35) +
  geom_pointrange(aes(x = year, y = pNmed, ymin = pNlb, ymax = pNub))

RRplot <- ggplot(resultsDF, aes(x=year, y=RRmed)) +
  scale_y_log10(limits = c(0.5, 10), breaks = c(0.5, 1, 2, 10)) +
  ggtitle("Year-specific 95% credible intervals") +
  geom_hline(aes(yintercept=1), color = 'darkgrey') +
  ylab("RR\n") + xlab("\nYear") +
  geom_pointrange(aes(x = year, y = RRmed, ymin = RRlb, ymax = RRub))

# setwd("~/timerr")
# pdf( file = "BayesianCIs_perYr.pdf", width = 8, height = 12 )
grid.arrange(pAplot,pNplot,RRplot,ncol=1)
# dev.off()

#====================================================================
# Analysis #1: compare year-by-year (independent) estimates 
# with pooled (random effect model) estimates

pA_YvP <- ggplot( rbind(Bayes_YBY_results,resultsDF),
                  aes(x = year, y = pA) ) +
  ylab("pA\n") + xlab("\nYear") +
  ggtitle("Yearly estimates and 95% CIs") +
  ylim(0,0.35) + 
  scale_color_manual(name = "Estimation \nmethod", values = brewer.pal(4,"RdBu")[c(1,4)]) +
  geom_pointrange( aes(x = year, y = pA, ymin = pAlb, ymax = pAub,
                       colour = estimation ), 
                   position = position_dodge(width=0.5) )

pN_YvP <- ggplot( rbind(Bayes_YBY_results,resultsDF),
                  aes(x = year, y = pN) ) +
  ylab("pN\n") + xlab("\nYear") +
  ggtitle("Yearly estimates and 95% CIs") +
  ylim(0,0.35) +
  scale_color_manual(name = "Estimation \nmethod", values = brewer.pal(4,"RdBu")[c(1,4)]) +
  geom_pointrange( aes(x = year, y = pN, ymin = pNlb, ymax = pNub,
                       colour = estimation ), 
                   name = "Estimation \nmethod",
                   position = position_dodge(width=0.5) )

RR_YvP <- ggplot( rbind(Bayes_YBY_results,resultsDF),
                  aes(x = year, y = RR) ) +
  ylab("RR\n") + xlab("\nYear") +
  ggtitle("Yearly estimates and 95% CIs") +
  scale_y_log10(limits = c(0.15, 75), breaks = c(0.5, 1, 10, 50)) +
  scale_color_manual(name = "Estimation \nmethod", values = brewer.pal(4,"RdBu")[c(1,4)]) +
  geom_pointrange( aes(x = year, y = RR, ymin = RRlb, ymax = RRub,
                       colour = estimation ), 
                   position = position_dodge(width=0.5) )

setwd("~/timerr/method/TX application/figures")
pdf( file = "YearByYear_vs_Pooled.pdf", width = 8, height = 10 )
grid.arrange(pA_YvP,pN_YvP,RR_YvP,ncol=1)
dev.off()

#====================================================================
# Analysis #2: summarize the impact of covariates

# Parameter estimates
ninoR.dens <- ggplot( data.frame( nino.coef = mcmcChain[,21]), aes(x=nino.coef)) +
  ylab("") + xlab("") +
  # ggtitle("Nino 3.4 anomaly coefficient (for log RR)") +
  geom_density( fill = "#fb6a4a" )

ninoN.dens <- ggplot( data.frame( nino.coef = mcmcChain[,19]), aes(x=nino.coef)) +
  ylab("") + xlab("") +
  ggtitle("Nino 3.4 anomaly coefficient (for logit pN)") +
  geom_density( fill = "#6baed6" )

nino.box <- ggplot( data.frame( nino.coefs = c(mcmcChain[,21], mcmcChain[,19]),
                                type = as.factor(c(rep("\nlog(RR)",numSavedSteps),
                                                   rep("\nlogit(pN)",numSavedSteps)))), 
                    aes(x = type, y = nino.coefs)) +
  ylab("") + xlab("") +
  geom_boxplot( outlier.shape = NA, aes( fill = type ) ) +
  scale_fill_manual( values = c( "\nlog(RR)" = "#fb6a4a",
                                 "\nlogit(pN)" = "#6baed6")) +
  theme( axis.text = element_text( size = 12 ),
         legend.position = "none" )

nino.box2 <- ggplot( data.frame( nino.coefs = c(mcmcChain[,21]),
                                 type = as.factor(rep("    ",numSavedSteps)) ), 
                    aes(x = type, y = nino.coefs)) +
  ylab("") + xlab("") + coord_flip() +
  geom_boxplot( outlier.shape = NA, aes( fill = type ) ) +
  scale_fill_manual( values = c( "    " = "#fb6a4a" )) +
  theme( legend.position = "none" )

# setwd("~/timerr/method/TX application/figures")
pdf( file = "NinoCoefEstimates.pdf", width = 6, height = 3 )
#grid.arrange(ninoR.dens,ninoN.dens,nino.box,layout_matrix = matrix(c(1,1,1,2,2,2,3,3),nrow=1))
grid.arrange(ninoR.dens,nino.box2,layout_matrix = matrix(c(1,1,1,1,2,2),ncol=1))
dev.off()

# tausq.alpha.dens <- ggplot( data.frame( tausq = mcmcChain[,39] ), aes(x=tausq)) +
#   ylab("") + xlab("") +
#   ggtitle("Random effect variance (for log RR)") +
#   geom_density( fill = "#a1d99b" )
# 
# tausq.delta.dens <- ggplot( data.frame( tausq = mcmcChain[,40] ), aes(x=tausq)) +
#   ylab("") + xlab("") +
#   ggtitle("Random effect variance (for logit pN)") +
#   geom_density( fill = "#bcbddc" )
# 
# tausq.box <- ggplot( data.frame( tausq = c(mcmcChain[,39], mcmcChain[,40]),
#                                  type = as.factor(c(rep("\nlog(RR)",numSavedSteps),
#                                                     rep("\nlogit(pN)",numSavedSteps)))), 
#                      aes(x = type, y = tausq)) +
#   ylab("") + xlab("") +
#   ylim(0,0.9) +
#   geom_boxplot( outlier.shape = NA, aes( fill = type ) ) +
#   scale_fill_manual( values = c( "\nlog(RR)" = "#a1d99b",
#                                  "\nlogit(pN)" = "#bcbddc")) +
#   theme( axis.text = element_text( size = 12 ),
#          legend.position = "none" )
# 
# # setwd("~/timerr")
# # pdf( file = "TausqEstimates.pdf", width = 14, height = 4 )
# grid.arrange(tausq.alpha.dens,tausq.delta.dens,tausq.box,layout_matrix = matrix(c(1,1,1,2,2,2,3,3),nrow=1))
# # dev.off()

#====================================================================
# Analysis #3: overall estimates of RRbar, pAbar, pNbar, AFTER 
# correcting for covariates

#=======================
# "Finite population" approach -- use only estimates from years observed

# Calculate the year-specific risk ratios, pA, pN without covariate
yearlyRR.noCov <- yearlypA.noCov <- yearlypN.noCov <- matrix(NA,ncol=M,nrow=numSavedSteps)
for(j in 1:M){
  yearlyRR.noCov[,j] <- exp( mcmcChain[,20] + mcmcChain[,j] )
  yearlypN.noCov[,j] <- (exp( mcmcChain[,18] + mcmcChain[,j+21])
                   /(1 + exp( mcmcChain[,18] + mcmcChain[,j+21])))
  yearlypA.noCov[,j] <- yearlypN.noCov[,j]*yearlyRR.noCov[,j]
}

# Global estimates
timeAvg.RR <- rowMeans(yearlyRR.noCov)
timeAvg.pA <- rowMeans(yearlypA.noCov)
timeAvg.pN <- rowMeans(yearlypN.noCov)

timeAvgs <- rbind(
  postSumm(timeAvg.RR, "RRbar", 0.025, 0.975),
  postSumm(timeAvg.pA, "pAbar", 0.025, 0.975),
  postSumm(timeAvg.pN, "pNbar", 0.025, 0.975)
)

#====================================================================
# Analysis #4: Quantify variability over time

# RR =================
# Method 1: variance
V.post <- rowMeans( (yearlyRR.noCov - timeAvg.RR)^2 )

# Method 2: mean absolute distance
MAD.post <- rowMeans(abs( yearlyRR.noCov - timeAvg.RR ))

# Method 3: mean relative absolute distance
MRAD.post <- rowMeans(abs( (yearlyRR.noCov/timeAvg.RR) - 1 ))

# Method 3: min/max ratios
min.post <- apply( yearlyRR.noCov/timeAvg.RR, 1, min)
max.post <- apply( yearlyRR.noCov/timeAvg.RR, 1, max)

# Summary
all.postSumm <- rbind(
  postSumm(timeAvg.RR, "RRbar", 0.025, 0.975),
  postSumm(V.post, "Var", 0.025, 0.975),
  postSumm(sqrt(V.post), "SD", 0.025, 0.975),
  postSumm(MAD.post, "MAD", 0.025, 0.975),
  postSumm(MRAD.post, "MRAD", 0.025, 0.975),
  postSumm(min.post, "Min", 0.025, 0.975),
  postSumm(max.post, "Max", 0.025, 0.975)
)

# Plots
Vplot <- ggplot( data.frame( measure = V.post ), aes(x=measure)) +
  ylab("") + xlab("V") +
  # xlim(0,unname(quantile((V.post),0.99))) +
  # ggtitle("Posterior distribution of V w/ 95% CI") +
  geom_density( fill = brewer.pal(6, "RdYlBu")[1] ) +
  geom_vline(aes(xintercept=unname(quantile((measure),0.025))), color = 'black') +
  geom_vline(aes(xintercept=unname(quantile((measure),0.975))), color = 'black')

Splot <- ggplot( data.frame( measure = sqrt(V.post) ), aes(x=measure)) +
  ylab("") + xlab("S") +
  # xlim(0,unname(quantile(sqrt(V.post),0.99))) +
  # ggtitle("Posterior distribution of S w/ 95% CI") +
  geom_density( fill = brewer.pal(6, "RdYlBu")[2] ) +
  geom_vline(aes(xintercept=unname(quantile((measure),0.025))), color = 'black') +
  geom_vline(aes(xintercept=unname(quantile((measure),0.975))), color = 'black')

MADplot <- ggplot( data.frame( measure = MAD.post ), aes(x=measure)) +
  ylab("") + xlab("MAD") +
#   xlim(0,unname(quantile(MAD.post,0.99))) +
  # ggtitle("Posterior distribution of MAD w/ 95% CI") +
  geom_density( fill = brewer.pal(6, "RdYlBu")[3] ) +
  geom_vline(aes(xintercept=unname(quantile((measure),0.025))), color = 'black') +
  geom_vline(aes(xintercept=unname(quantile((measure),0.975))), color = 'black')

MRADplot <- ggplot( data.frame( measure = MRAD.post ), aes(x=measure)) +
  ylab("") + xlab("MRAD") +
  #   xlim(0,unname(quantile(MAD.post,0.99))) +
  # ggtitle("Posterior distribution of MAD w/ 95% CI") +
  geom_density( fill = brewer.pal(6, "RdYlBu")[4] ) +
  geom_vline(aes(xintercept=unname(quantile((measure),0.025))), color = 'black') +
  geom_vline(aes(xintercept=unname(quantile((measure),0.975))), color = 'black')

minplot <- ggplot( data.frame( measure = min.post ), aes(x=measure)) +
  ylab("") + xlab("MinR") +
  # xlim(0,unname(quantile(min.post,0.99))) +
  # ggtitle("Posterior distribution of MinR w/ 95% CI") +
  geom_density( fill = brewer.pal(6, "RdYlBu")[5] ) +
  geom_vline(aes(xintercept=unname(quantile((measure),0.025))), color = 'black') +
  geom_vline(aes(xintercept=unname(quantile((measure),0.975))), color = 'black')

maxplot <- ggplot( data.frame( measure = max.post ), aes(x=measure)) +
  ylab("") + xlab("MaxR") +
  # xlim(min(max.post),unname(quantile(max.post,0.99))) +
  # ggtitle("Posterior distribution of MaxR w/ 95% CI") +
  geom_density( fill = brewer.pal(6, "RdYlBu")[6] ) +
  geom_vline(aes(xintercept=unname(quantile((measure),0.025))), color = 'black') +
  geom_vline(aes(xintercept=unname(quantile((measure),0.975))), color = 'black')

# setwd("~/timerr/method/TX application/figures")
# pdf( file = "varPost.pdf", width = 9, height = 5 )
grid.arrange(Vplot,Splot,MADplot,MRADplot,minplot,maxplot,ncol=3)
# dev.off()


#=======================
postSumm <- function( post.samples, var.name, lowerQuan, upperQuan ){
  return(data.frame(
    Variable = var.name,
    Mean = mean(post.samples),
    Median = median(post.samples),
    SD = sd(post.samples),
    CI.LB = unname(quantile(post.samples,lowerQuan)),
    CI.UB = unname(quantile(post.samples,upperQuan))
  ))
}
#=======================

# Predictive summary of time-averaged RR
pred.params <- data.frame(
  beta0 = mcmcChain[,20],
  beta1 = mcmcChain[,21],
  tausq.alpha = mcmcChain[,39]
)

Xmat.pred <- matrix(c(1,0),ncol=2)

pred.logRR <- rep(NA,nrow(pred.params))
for(i in 1:nrow(pred.params)){
  pred.logRR[i] <- Xmat.pred %*% t(as.matrix(pred.params[i,1:2])) + sqrt(pred.params[i,3])*rnorm(1)
}

qplot(pred.logRR, geom="histogram", binwidth = 0.1)
qplot(exp(pred.logRR), geom="histogram", binwidth = 0.25)

mean(pred.logRR)
median(pred.logRR)

mean(exp(pred.logRR))
median(exp(pred.logRR))

logRR.credInt.width <- unname(quantile((pred.logRR),0.975) - quantile((pred.logRR),0.025))
RR.credInt.width <- unname(quantile(exp(pred.logRR),0.975) - quantile(exp(pred.logRR),0.025))

logRRpred <- ggplot( data.frame( logRRpred = pred.logRR ), aes(x=logRRpred)) +
  ylab("") + xlab("Log risk ratio") + xlim(-1.5,2.5) +
  ggtitle("Predictive distribution for time-averaged log RR") +
  geom_density( fill = "#fdae61" ) +
  geom_vline(aes(xintercept=unname(quantile((logRRpred),0.025))), color = 'black') +
  geom_vline(aes(xintercept=unname(quantile((logRRpred),0.975))), color = 'black')

RRpred <- ggplot( data.frame( RRpred = exp(pred.logRR) ), aes(x=RRpred)) +
  ylab("") + xlab("Risk ratio") + xlim(0,8) +
  ggtitle("Predictive distribution for time-averaged RR") +
  geom_density( fill = "#3288bd" ) +
  geom_vline(aes(xintercept=unname(quantile((RRpred),0.025))), color = 'black') +
  geom_vline(aes(xintercept=unname(quantile((RRpred),0.975))), color = 'black')

setwd("~/timerr")
pdf( file = "predRR.pdf", width = 10, height = 4 )
grid.arrange(logRRpred, RRpred, ncol=2)
dev.off()

# Compare predictive log RR to yearly estimates
# Remove the effect of the covariate, from both

# time-averaged estimate,
pred.logRR.noENSO <- rep(NA,nrow(pred.params))
for(i in 1:nrow(pred.params)){
  pred.logRR.noENSO[i] <- pred.params[i,1] + sqrt(pred.params[i,3])*rnorm(1)
}

# and yearly estimates
yearly.logRR.noEnso <- matrix(NA,ncol=M,nrow=numSavedSteps)
for(j in 1:M){
  yearly.logRR.noEnso[,j] <- mcmcChain[,20] + mcmcChain[,j] 
}


deviation <- rowMeans( yearly.logRR.noEnso - pred.logRR.noENSO )

logRRdev <- ggplot( data.frame( RRdev = rowMeans( yearly.logRR.noEnso - pred.logRR.noENSO ) ), 
                 aes(x=RRdev)) +
  ylab("") + xlab("Deviation (log RR)") + 
  ggtitle("Average yearly deviation from time-averaged log RR") +
  geom_density( fill = "#3288bd" ) +
  geom_vline(aes(xintercept=unname(quantile((RRdev),0.025))), color = 'black') +
  geom_vline(aes(xintercept=unname(quantile((RRdev),0.975))), color = 'black')

RRdev <- ggplot( data.frame( RRdev = rowMeans( exp(yearly.logRR.noEnso) - exp(pred.logRR.noENSO) ) ), 
        aes(x=RRdev)) +
  ylab("") + xlab("Deviation (RR)") + xlim(-4,3) + 
  ggtitle("Average yearly deviation from time-averaged RR") +
  geom_density( fill = "#3288bd" ) +
  geom_vline(aes(xintercept=unname(quantile((RRdev),0.025))), color = 'black') +
  geom_vline(aes(xintercept=unname(quantile((RRdev),0.975))), color = 'black')

pdf( file = "RRdev.pdf", width = 10, height = 4 )
grid.arrange(logRRdev, RRdev, ncol=2)
dev.off()



# Calculate an "I^2 statistic"
Q <- rowSums( (yearly.logRR.noEnso - pred.logRR.noENSO)^2 )
Q2 <- (rowMeans(yearly.logRR.noEnso) - pred.logRR.noENSO)^2
hist(Q)
i2 <- 100*(Q-(M-1))/Q
i2[i2 < 0] <- 0
mean(i2)
median(i2)
quantile(i2,0.025)
quantile(i2,0.975)








