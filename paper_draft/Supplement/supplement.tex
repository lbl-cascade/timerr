\documentclass[times,12pt]{article}

%\usepackage{macros}
\usepackage{mdwlist}
\usepackage[utf8]{inputenc}

% load packages
\usepackage{enumitem}
\usepackage{times,natbib}
\usepackage{balance}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{multirow}
\usepackage[margin=1in]{geometry}
\usepackage{subfigure}
\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{breakurl}
\usepackage{natbib,upgreek}
\usepackage{bbm}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{doi}
\DeclareMathOperator{\logit}{logit}

\newcommand{\bfbeta}{{\boldsymbol \beta}}
\newcommand{\bftheta}{{\boldsymbol \theta}}
\newcommand{\bfs}{{\bf s}}
\newcommand{\bfx}{{\bf x}}
\def\bSig\mathbf{\Sigma}

\setitemize[0]{leftmargin=60pt}

%\newcommand{\todo}[1]{\textbf{[\color{green} #1]}}
%\newcommand{\pfc}[1]{\textbf{[\color{blue}Peter: #1]}}
%\newcommand{\rh}[1]{\textbf{[\color{red}Radu: #1]}}
%\newcommand{\vv}[1]{\mbox{\boldmath $#1$}}


\begin{document}

\title{Supplemental materials \\[2ex] {\large Accompaniment to: Quantifying the effect of interannual ocean variability \\ on the attribution of extreme climate events to human influence}}
\date{}
\maketitle  



\doublespacing

\section{Prior specification} \label{prior}

In a Bayesian framework, all unknown parameters must be assigned a prior distribution, which summarizes all knowledge about the parameters \textit{a priori} to observing data. While in general any known prior information relating to the parameters can be incorporated by way of the prior distribution, we instead use non-informative and proper prior distributions, meaning that the priors (1) specify essentially no information about the parameters, but (2) are proper statistical distributions.

Recall that the full parameter vector for our model is $\bftheta = (\boldsymbol{\alpha}, \boldsymbol{\delta}, \boldsymbol{\gamma}, \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, \omega^2)$; we now define the prior distribution $p(\bftheta)$. Note that several components of the prior distribution are specified in the third level of the model, and we can thus decompose the prior as 
\begin{equation} \label{decomp_prior}
p(\bftheta) = p( \boldsymbol{\alpha} | \tau^2) \cdot p( \boldsymbol{\delta} | \sigma^2) \cdot p( \boldsymbol{\gamma} | \omega^2) \cdot  p(\tau^2 )  \cdot p( \sigma^2)  \cdot p(\omega^2 )  \cdot p( \bfbeta_A)  \cdot p( \bfbeta_N).
\end{equation}
Each of these components are as follows:
\[
\begin{array}{c}
p( \boldsymbol{\alpha} | \tau^2) = N_T({\bf 0}, \tau^2{\bf I}_{T}), \hskip2ex p( \boldsymbol{\delta} | \sigma^2) = N_T({\bf 0}, \sigma^2{\bf I}_{T}), \hskip2ex
p( \boldsymbol{\gamma} | \omega^2) = N_{12}({\bf 0}, \omega^2{\bf I}_{12}) \cdot \mathbbm{1}_{\{ \sum_{j} \gamma_j = 0 \} }, \\ [1ex]
p(\tau^2 ) =U(0, 1000), \hskip2ex p( \sigma^2)= U(0, 1000), \hskip2ex p(\omega^2 ) = C(10),  \\[1ex]
p( \bfbeta_A) = N_{2}({\bf 0}, 10^2{\bf I}_{2}) , \hskip2ex p( \bfbeta_N) = N_{2}({\bf 0}, 10^2{\bf I}_{2})
\end{array}
\]
Here, $N_q({\bf a}, {\bf B})$ is a $q$-variate Gaussian distribution with mean ${\bf a}$ and covariance ${\bf B}$; $U(c, d)$ is the uniform distribution over the interval $(c, d)$; $C(s)$ is the half-Cauchy distribution, i.e., the positive portion of a Cauchy distribution centered on 0 and with scale parameter $s$. Note: the prior on $\boldsymbol{\gamma}$ imposes the restriction that the empirical mean is zero.

One additional constraint is imposed on the prior, in order to account for a problem with the mixing of the Markov chain Monte Carlo (MCMC) algorithm for several of the event types and regions. The issue has to do with the fact that the monthly effects are defined on the $\logit$ scale, and therefore the model can have trouble distinguishing between $\logit^{-1}(-10)$ and $\logit^{-1}(-100)$ since these are both essentially zero. This leads to a pseudo ``non-identifiability'' when many of the monthly probabilities are zero; in other words, when the seasonality is such that essentially all of the extreme events occur in a single calendar month (e.g., for hot events in the northern extra-tropics, almost all hot events occur in July). In these cases, some of the monthly effects ($\gamma_j$) trade off with the scenario-specific intercepts ($\beta_{A0}$ and $\beta_{N0}$): these parameters mix very poorly, while trace plots for the probabilities themselves indicate convergence for the MCMC.

To account for this, for events where this poor mixing arises we add an additional constraint on the prior: we require the logit monthly probabilities to lie between $\pm L$, where the constant $L$ is chosen separately for each region and event type to ensure that the $\gamma_j$ and $\beta_{k0}$ mix properly (often, $L = 10$ to $20$). Formally, the prior we use is
\begin{equation} \label{final_prior}
\widetilde{p}(\bftheta) = p(\bftheta) \cdot \mathbbm{1}_{\big\{ \min \{\logit p_{ktj}\} > -L \big\}} \cdot \mathbbm{1}_{\big\{ \max \{\logit p_{ktj}\} <L \big\}},
\end{equation}
where $p(\bftheta)$ is from (\ref{decomp_prior}).

\section{Markov chain Monte Carlo}

The hierarchical statistical model introduced in Section 3 of the main paper is estimated using a Bayesian paradigm. As is often the case, the posterior distribution given in Equation (5) is not available in closed form, and we therefore resort to Markov chain Monte Carlo (MCMC) methods (for a summary see, e.g., Gilks, Richardson, and Spiegelhalter, 1996) to obtain joint samples from the posterior distribution (i.e., the distribution of all parameters conditional on observed data). However, the nature of our model requires several computational tools above and beyond standard MCMC methods (e.g., Gibbs sampling and Metropolis-Hastings), which are now described. 


First, recall that we defined random variables ${\bf Z} = \{ Z_{ktj}:  k \in \{A,N\}; t=1, \dots, T; j = 1, \dots, 12 \}$; now define ${\bf z}$ to represent the observed values of ${\bf Z}$. For clarity, we rewrite the likelihood for each $Z_{ktj}$ in terms of $\bftheta$ as
\begin{equation} \label{indiv_likelihood}
p(Z_{ktj} | \bftheta) = \text{binomial}\big(n_t, \logit^{-1}[{\bf x}_{kt}^\top\bfbeta_k + \alpha_t + \delta_t \mathbbm{1}_{\{k=A\}} + \gamma_j ] \big),
\end{equation}
and the likelihood for all $Z_{ktj}$ is
\begin{equation} \label{likelihood}
p( {\bf Z} | \bftheta) =  \prod_{k \in \{A, N\}} \prod_{t=1}^T \prod_{j=1}^{12} p(Z_{ktj} | \bftheta).
\end{equation}
Then, the posterior distribution of interest is
\begin{equation} \label{post}
p(\bftheta | {\bf Z =  z}) = \frac{p({\bf z} | \bftheta) \widetilde{p}(\bftheta)}{ \int_\bftheta p({\bf z} | \bftheta) \widetilde{p}(\bftheta) d\bftheta} \propto p({\bf z} | \bftheta) \widetilde{p}(\bftheta),
\end{equation}
where $p({\bf z} | \bftheta)$ is from (\ref{likelihood}) and $\widetilde{p}(\bftheta)$ is the prior distribution from (\ref{final_prior}).


Several strategies for sampling from (\ref{post}) in a MCMC framework are as follows.

\subsection{Traditional Gibbs sampler with Metropolis-Hastings}

A traditional MCMC approach to sampling from (\ref{post}) is as follows:

\noindent\hrulefill

\noindent {\rm {\bf Traditional Sampler:} }

\begin{itemize}
\item[\textit{Step 1}:] Draw $(\boldsymbol{\alpha}, \boldsymbol{\delta})$ from $p(\boldsymbol{\alpha}, \boldsymbol{\delta} |  \boldsymbol{\gamma}, \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, \omega^2, {\bf Z} ) = p(\boldsymbol{\alpha}, \boldsymbol{\delta} |  \boldsymbol{\gamma}, \bfbeta_A, \bfbeta_N, {\bf Z} )$

\item[\textit{Step 2}:] Draw $\boldsymbol{\gamma}$ from $p( \boldsymbol{\gamma} | \boldsymbol{\alpha}, \boldsymbol{\delta}, \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, \omega^2, {\bf Z} ) = p( \boldsymbol{\gamma} | \boldsymbol{\alpha}, \boldsymbol{\delta}, \bfbeta_A, \bfbeta_N, {\bf Z} )$

\item[\textit{Step 3 (A)}:] Draw $( \bfbeta_A, \bfbeta_N)$ from $p( \bfbeta_A, \bfbeta_N | \boldsymbol{\alpha}, \boldsymbol{\delta},  \boldsymbol{\gamma}, \tau^2, \sigma^2, \omega^2, {\bf Z} ) = p( \bfbeta_A, \bfbeta_N | \boldsymbol{\alpha}, \boldsymbol{\delta}, \boldsymbol{\gamma}, {\bf Z} )$

\item[\textit{Step 4 (S)}:] (a) Draw $\tau^2$ from $p( \tau^2 | \bfbeta_A, \bfbeta_N, \boldsymbol{\alpha}, \boldsymbol{\delta}, \sigma^2,  \omega^2, {\bf Z} ) = p( \tau^2 | \boldsymbol{\alpha} )$

(b) Draw $\sigma^2$ from $p( \sigma^2 | \bfbeta_A, \bfbeta_N, \boldsymbol{\alpha}, \boldsymbol{\delta}, \tau^2, \omega^2, {\bf Z} ) = p( \sigma^2 | \boldsymbol{\delta} )$

\item[\textit{Step 5}:] Draw $\omega^2$ from $p( \omega^2 |\boldsymbol{\alpha}, \boldsymbol{\delta}, \boldsymbol{\gamma},  \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, {\bf Z} ) = p( \omega^2 | \boldsymbol{\gamma} )$

\end{itemize}

\vskip-2ex

\noindent\hrulefill

\noindent Regardless of prior choice, steps 1-3 will require Metropolis-Hastings; while choosing conjugate priors for $\tau^2$, $\sigma^2$, and $\omega^2$ could allow for closed-form Gibbs updates in steps 4 (a)-(b) and 5, the non-informative uniform priors used (see Supplemental Section \ref{prior}) also necessitate Metropolis-Hastings steps. The \textit{(A)} and \textit{(S)} labels will be explained in the next section.

\subsection{Ancillarity-Sufficiency Interweaving Strategy (ASIS) MCMC}

The problem with using the Traditional Sampler defined above lies in the fact that the regression coefficients and $\bfbeta_k$ and yearly effects $(\boldsymbol{\alpha}, \boldsymbol{\delta})$ are highly correlated, resulting in extremely poor mixing of the Markov chain. In order to fix this problem, we use the Ancillarity-Sufficiency Interweaving Strategy (ASIS) method of Yu and Meng (2011). MCMC for multilevel models can be set up using either a centered parameterization (CP) or a non-centered parameterization (NCP); their approach proposes a strategy that alternates (``interweaves'') sampling from both parameterizations. The ASIS terminology replaces the CP/NCP terminology by framing the problem in terms of the mathematically equivalent data augmentation schemes of \textit{ancillary augmentation} (AA) and \textit{sufficient augmentation} (SA), arguing that this terminology better captures the ``essence'' of the method. Regardless, in the AA scheme (i.e., NCP), the missing data (i.e., latent variables) are an ancillary statistic for the parameter of interest, and in the SA scheme (i.e., CP), the missing data are a sufficient statistic for the parameter of interest.

Note now that the standard augmentation of $\boldsymbol{\alpha}$ and $\boldsymbol{\delta}$ are AA for $( \bfbeta_A, \bfbeta_N)$ but SA for $( \tau^2, \sigma^2)$ (hence, the \textit{A} and \textit{S} notation in the Traditional sampler). To implement the ASIS sampler, we need to find an AA for $( \tau^2, \sigma^2)$ and an SA for $( \bfbeta_A, \bfbeta_N)$.

\vskip3ex
\noindent \underline{SA for $( \bfbeta_A, \bfbeta_N)$:} 

\noindent Define new latent variables $\eta_t = {\bf x}_{Nt}^\top\bfbeta_N + \alpha_t$ and $\nu_t = {\bf x}_{At}^\top\bfbeta_A + \alpha_t + \delta_t$, so that the parameterization in (\ref{indiv_likelihood}) can be rewritten as
\begin{equation} \label{model2a}
p(Z_{ktj} | \bftheta) = \text{binomial}\big(n_t, \logit^{-1}[\eta_t \mathbbm{1}_{\{k=N\}} + \nu_t \mathbbm{1}_{\{k=A\}} + \gamma_j ] \big),
\end{equation}
where now
\begin{equation} \label{model2b}
\eta_t \stackrel{\text{ind}}{\sim} N({\bf x}_{Nt}^\top\bfbeta_N, \tau^2) \hskip3ex \text{and} \hskip3ex \nu_{t} \stackrel{\text{ind}}{\sim} N(  {\bf x}_{At}^\top\bfbeta_A, \tau^2 + \sigma^2).
\end{equation}
Note that $p({\bf Z} | \boldsymbol{\eta}, \boldsymbol{\nu}, \boldsymbol{\gamma}, \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2) = p({\bf Z} | \boldsymbol{\eta}, \boldsymbol{\nu}, \boldsymbol{\gamma})$; therefore $(\boldsymbol{\eta}, \boldsymbol{\nu})$ are SA for $( \bfbeta_A, \bfbeta_N)$ (and, for that matter, also for $( \tau^2, \sigma^2)$).

The traditional sampler can be improved by adding a step that samples $( \bfbeta_A, \bfbeta_N)$ under the SA. Actually, this step will be split into two steps so that Gibbs steps can be used:

\begin{itemize}
\item[\textit{Step 3 (S)}:] (a) Draw $\bfbeta_A$ from $p( \bfbeta_A | \bfbeta_N,  \boldsymbol{\eta}, \boldsymbol{\nu}, \boldsymbol{\gamma}, \tau^2, \sigma^2, \omega^2, {\bf Z} ) = p( \bfbeta_A | \boldsymbol{\nu}, \tau^2, \sigma^2 )$

(b) Draw $\bfbeta_N$ from $p( \bfbeta_N | \bfbeta_A,  \boldsymbol{\eta}, \boldsymbol{\nu}, \boldsymbol{\gamma}, \tau^2, \sigma^2, \omega^2, {\bf Z} ) = p( \bfbeta_N | \boldsymbol{\eta}, \tau^2 )$

\end{itemize}

\noindent Using conjugate (Gaussian) priors for $\bfbeta_N$ and $\bfbeta_A$, these can be sampled in a Gibbs step.

\vskip3ex
\noindent \underline{AA for $( \tau^2, \sigma^2)$:} 

\noindent Define new latent variables $\kappa_t = \alpha_t/\tau$ and $\xi_t = \delta_t/\sigma$; the parameterization in (\ref{indiv_likelihood}) can be rewritten as
\begin{equation} \label{model3a}
p(Z_{ktj} | \bftheta) = \text{binomial}\big(n_t, \logit^{-1}[{\bf x}_{kt}^\top\bfbeta_k + \tau \kappa_t + \sigma \xi_t \mathbbm{1}_{\{k=A\}} + \gamma_j ] \big)
\end{equation}
and
\begin{equation} \label{model3b}
\kappa_t \stackrel{\text{iid}}{\sim} N(0, 1) \hskip3ex \text{and} \hskip3ex \xi_{t} \stackrel{\text{iid}}{\sim} N(  0, 1).
\end{equation}
Because of (\ref{model3b}), we can see that $(\boldsymbol{\kappa}, \boldsymbol{\xi})$ are  AA for $( \tau^2, \sigma^2)$ (and $( \bfbeta_A, \bfbeta_N)$). Hence, we can add the following step:

\begin{itemize}
\item[\textit{Step 4 (A)}:] Draw $( \tau^2, \sigma^2 )$ from $p(  \tau^2, \sigma^2 |  \boldsymbol{\gamma}, \bfbeta_A , \bfbeta_N,  \boldsymbol{\kappa}, \boldsymbol{\xi}, {\bf Z} )$

\end{itemize}

\noindent This step again requires Metropolis-Hastings.

Combining all of the above, the following is a componentwise interweaving sampler using ASIS. The intermediate latent variables are calculated between the $A$ and $S$ step, and the original variables are then updated after the $S$ step. For each of these calculations, the most recently sampled parameter values are used: the ``$*$'' sub- and superscripts are used to explicitly show this. Specific details on how each step is carried out are also provided; for example, many of the yearly and monthly effects are sampled sequentially.

\noindent\hrulefill

\noindent {\rm {\bf Componentwise Interweaving Sampler:} }

\begin{itemize}

\item[\textit{Step 1}:] For $t=1, \dots, T$, draw $(\alpha_t^*, \delta_t^*)$ from $p(\alpha_t, \delta_t | \boldsymbol{\gamma}, \bfbeta_A, \bfbeta_N, {\bf Z} )$

{\small 
For each $t$, this involves a random-walk Metropolis-Hastings (RWMH) step. The bivariate proposal distribution is centered on current values, and the proposal correlation is fixed to a large negative number ($\approx -0.95$) to improve mixing.
}


\item[\textit{Step 2}:] For $j = 1, \dots, 12$, draw $\gamma^*_j$ from $p( \gamma_j | \boldsymbol{\alpha}^*, \boldsymbol{\delta}^*, \bfbeta_A, \bfbeta_N, {\bf Z} )$

{\small 
For each $j$, this again involves a RWMH step. Recall the prior constraint that $\sum_{j=1}^{12} \gamma_j = 0$; this constraint is imposted as follows:
first, a new component $\gamma_j^{\text{prop}}$ is proposed, and define $\widetilde{\boldsymbol{\gamma}} = (\gamma_1^{\text{curr}}, \dots, \gamma_j^{\text{prop}}, \dots, \gamma_{12}^{\text{curr}})$. Then, the other components of $\boldsymbol{\gamma}$ are also adjusted to arrive at a fully adjusted proposal:
\[
\boldsymbol{\gamma}^{\text{prop}} = \widetilde{\boldsymbol{\gamma}} - \frac{1}{12} \sum_{k=1}^{12} \widetilde{{\gamma_k}}.
\]
As long as the initialized value of $\boldsymbol{\gamma}$ is mean-zero, this adjustment will preserve the prior constraint. For each $j$, the entire vector $\boldsymbol{\gamma}^{\text{prop}}$ is either accepted or rejected.
}


\item[\textit{Step 3 (A)}:] Draw $( \bfbeta_A^*, \bfbeta_N^*)$ from $p( \bfbeta_A, \bfbeta_N | \boldsymbol{\alpha}^*, \boldsymbol{\delta}^*, \boldsymbol{\gamma}^*, {\bf Z} )$

{\small 
This step is carried out using RWMH, with a separate bivariate proposal for each $\bfbeta_A$ and $\bfbeta_N$. The within-scenario coefficients are blocked together in order to avoid poor mixing. For wet events, the proposal correlation is 0, as there were no problems with mixing. However, for hot and cold events, there was poor mixing for the intercept and temperature coefficient for the scenario which had essentially all zero counts (NAT for hot events and ALL for cold events). In these cases, the proposal correlation for these scenarios was fixed to a large negative value ($\approx -0.95$).
}

\item[\textbf{Calculate:}]  $\eta_t = {\bf x}_{Nt}^\top\bfbeta_N^* + \alpha_t^*$ and $\nu_t = {\bf x}_{At}^\top\bfbeta_A^* + \alpha_t + \delta_t^*$ for $t=1,\dots, T$.

\item[\textit{Step 3 (S)}:] (a) Draw $\bfbeta_A^{**}$ from $p( \bfbeta_A | \boldsymbol{\nu}, \tau^2, \sigma^2 )$

(b) Draw $\bfbeta_N^{**}$ from $p( \bfbeta_N | \boldsymbol{\eta}, \tau^2 )$

{\small 
Using conjugate Gaussian priors for $\bfbeta_A$ and $\bfbeta_N$, this step can be accomplished with a closed-form Gibbs update.
}

\item[\textbf{Update:}]  $\alpha_t^{**} = \eta_t - {\bf x}_{Nt}^\top\bfbeta_N^{**}$ and $\delta_t^{**} = \nu_t - {\bf x}_{At}^\top\bfbeta_A^{**} - \alpha_t^{**}$ for $t=1,\dots, T$.

\item[\textbf{Calculate:}]  $\kappa_t = \alpha_t^{**}/\tau$ and $\xi_t = \delta_t^{**}/\sigma$ for $t=1,\dots, T$.

\item[\textit{Step 4 (A)}:] Draw $( \tau^2_*, \sigma^2_* )$ from $p(  \tau^2, \sigma^2 |  \boldsymbol{\gamma}^*, \bfbeta_A^{**} , \bfbeta_N^{**},  \boldsymbol{\kappa}, \boldsymbol{\xi}, {\bf Z} )$ (RWMH, separately for $\tau^2$ and $\sigma^2$)

\item[\textbf{Update:}]  $\alpha_t^{***} = \tau_{*}\kappa_t$ and $\delta_t^{***} = \sigma_{*}\xi_t$ for $t=1,\dots, T$.

\item[\textit{Step 4 (S)}:] (a) Draw $\tau^2_{**}$ from $p( \tau^2 | \boldsymbol{\alpha}^{***} )$ (RWMH)

(b) Draw $\sigma^2_{**}$ from $p( \sigma^2 | \boldsymbol{\delta}^{***} )$ (RWMH)

\item[\textit{Step 5}:] Draw $\omega^2_*$ from $p( \omega^2 | \boldsymbol{\gamma}^* )$ (RWMH)

\end{itemize}

%
%\begin{itemize}
%\item[\textit{Step 1}:] Draw $(\boldsymbol{\alpha}^*, \boldsymbol{\delta}^*)$ from $p(\boldsymbol{\alpha}, \boldsymbol{\delta} | \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, {\bf Z} )$
%
%\item[\textit{Step 2 (A)}:] Draw $( \bfbeta_A^*, \bfbeta_N^*)$ from $p( \bfbeta_A, \bfbeta_N | \boldsymbol{\alpha}^*, \boldsymbol{\delta}^*, \tau^2, \sigma^2, {\bf Z} ) = p( \bfbeta_A, \bfbeta_N | \boldsymbol{\alpha}^*, \boldsymbol{\delta}^*, {\bf Z} )$
%
%\item[\textbf{Calculate:}]  $\eta_t = {\bf x}_t^\top\bfbeta_N^* + \alpha_t^*$ and $\nu_t = {\bf w}_t^\top\bfbeta_A^* + \alpha_t + \delta_t^*$ for $t=1,\dots, T$.
%
%\item[\textit{Step 2 (S)}:] 
%
%\item[(a)] Draw $\bfbeta_A^{**}$ from $p( \bfbeta_A | \bfbeta_N^*,  \boldsymbol{\eta}, \boldsymbol{\nu}, \tau^2, \sigma^2, {\bf Z} ) = p( \bfbeta_A | \boldsymbol{\nu}, \tau^2, \sigma^2 )$
%
%\item[(b)] Draw $\bfbeta_N^{**}$ from $p( \bfbeta_N | \bfbeta_A^{**},  \boldsymbol{\eta}, \boldsymbol{\nu}, \tau^2, \sigma^2, {\bf Z} ) = p( \bfbeta_N | \boldsymbol{\eta}, \tau^2 )$
%
%\item[\textbf{Update:}]  $\alpha_t^{**} = \eta_t - {\bf x}_t^\top\bfbeta_N^{**}$ and $\delta_t^{**} = \nu_t - {\bf w}_t^\top\bfbeta_A^{**} - \alpha_t^{**}$ for $t=1,\dots, T$.
%
%\item[\textbf{Calculate:}]  $\kappa_t = \alpha_t^{**}/\tau_*$ and $\xi_t = \delta_t^{**}/\sigma_*$ for $t=1,\dots, T$.
%
%\item[\textit{Step 3 (A)}:] Draw $( \tau^2_*, \sigma^2_*)$ from $p(  \tau^2, \sigma^2 | \bfbeta_A^{**} , \bfbeta_N^{**},  \boldsymbol{\kappa}, \boldsymbol{\xi}, {\bf Z} )$
%
%\item[\textbf{Update:}]  $\alpha_t^{***} = \tau_{*}\kappa_t$ and $\delta_t^{***} = \sigma_{*}\xi_t$ for $t=1,\dots, T$.
%
%\item[\textit{Step 3 (S)}:] 
%\item[(a)] Draw $\tau^2_{**}$ from $p( \tau^2 | \bfbeta_A^{**}, \bfbeta_N^{**}, \boldsymbol{\alpha}^{***}, \boldsymbol{\delta}^{***}, \sigma^2_*, {\bf Z} ) = p( \tau^2 | \boldsymbol{\alpha}^{***} )$
%
%\item[(b)] Draw $\sigma^2_{**}$ from $p( \sigma^2 | \bfbeta_A^{**}, \bfbeta_N^{**}, \boldsymbol{\alpha}^{***}, \boldsymbol{\delta}^{***}, \tau^2_{**}, {\bf Z} ) = p( \sigma^2 | \boldsymbol{\delta}^{***} )$
%
%
%
%\end{itemize}

\vskip-2ex

\noindent\hrulefill

\noindent At the end of each iteration, save the most recently sampled/calculated values of the parameters: $(\boldsymbol{\alpha}^{***}, \boldsymbol{\delta}^{***}, \boldsymbol{\gamma}^{*}, \bfbeta_A^{**}, \bfbeta_N^{**}, \tau^2_{**}, \sigma^2_{**}, \omega^2_*)$. Note: the proposal variance for each of the RWMH steps was tuned such that the acceptance probability falls between $0.3$ and $0.4$.


\section{Software and data}

\begin{table}[!t]
\begin{center}
\begin{tabular}{|l|c|} 
\hline
\textbf{Name} & \textbf{Version} \\ \hline \hline
{\tt R} 					& 3.2.3 \\ \hline \hline
{\tt colorspace} 		& 1.2-6 \\ \hline
{\tt RColorBrewer} 	& 1.1-2 \\ \hline
{\tt gridExtra} 			& 2.0.0 \\ \hline
{\tt ggplot2} 			& 1.0.1 \\ \hline
{\tt MASS} 			& 7.3-45 \\ \hline
\end{tabular}
\end{center}
\caption{Version of {\tt R} and other packages used to generate the results and plots in the main text.}
\label{versions}
\end{table}%


The componentwise interweaving MCMC sampler was coded up in {\tt R}, along with all other functions required to calculate the adjusted risk ratio and exceedance probabilities and make the plots in the paper. The source code, data files, and a script with replication code are publicly available in the GitHub repository [insert link].

As a note, the version of {\tt R} and various packages used to generate the results in the paper are listed in Supplemental Table \ref{versions}.

\subsection{Data files}

The following data files are included:

\begin{enumerate}
\item Binomial count data

A total of 58 files are provided, one for each region. Each text file (e.g., {\tt USA-C.txt}) contains the binomial count variables for each month ({\tt january}, {\tt february}, \dots, {\tt december}), as well as additional variables indicating {\tt event\char`_type} ({\tt hot}, {\tt cold}, or {\tt wet}), {\tt scenario} ({\tt ALL} or {\tt NAT}), {\tt year}, and {\tt n\char`_sims} (the ensemble size).

%This file is a list with 58 sub-lists, one for each of the WRAF regions. Each sub-list (e.g., {\tt WRAF\char`_regions[[3]]}) contains the name of the region (accessed by adding {\tt \$region}) and a list of data for the region (accessed by adding {\tt \$data}). The {\tt data} list contains seven items: {\tt n.sims}, a vector with the number of simulations available for each year (from 1982-2013); {\tt z.wet.ALL} and {\tt z.wet.NAT}, matrices of the count variables $Z_{ktj}$ for wet events (the matrices represent each year with a row and each month with a column);  {\tt z.hot.ALL} and {\tt z.hot.NAT}, corresponding matrices for hot events;  {\tt z.cold.ALL} and {\tt z.cold.NAT}, corresponding matrices for cold events.

\item {\tt gmt.txt}

This file contains four variables {\tt gmtA\char`_raw}, {\tt gmtA}, {\tt gmtN\char`_raw}, and {\tt gmtN}, each of which contains a temperature value from 1982-2013. The {\tt raw} vectors contain the raw temperature values shown in Figure 2 of the main text; the other vectors contain temperature values that are shifted to have mean zero and scaled to have variance one.

\item {\tt logit\char`_p\char`_LBs.txt}

This file contains the three variables {\tt hot\char`_limits},  {\tt cold\char`_limits}, and {\tt wet\char`_limits}, as well as a {\tt region} variable. Each of the {\tt limits} variables contain the lower bound for the logit probabilities used in the model fitting for the main text; these represent the $-L$ value introduced in Supplementary Section \ref{prior}.

\end{enumerate}


\subsection{Primary functions}

The replication code file contains sample implementations of each of the following functions. Default values of the inputs are indicated.

First, a wrapper function to run the componentwise interweaving MCMC sampler is:

{\singlespacing
\begin{verbatim}
timeRR_month( zA_mat, zN_mat, n_sims, gmt_A, gmt_N, 
  ITER = 10000, start_keep = 1, n_tune = 6, 
  n_iter_tune = c(rep(400,3),rep(800,3)),
  thin = 1, event_type, alpha_delta_corr_prop = -0.98,
  betaA_corr_prop = 0, betaN_corr_prop = 0, 
  logit_p_UB = 15, logit_p_LB = -15 )
\end{verbatim}
}

\noindent The variables {\tt zA\char`_mat} and {\tt zN\char`_mat} must be $M\times12$ matrices of monthly count variables (where $M$ is the total number of years) for the ALL and NAT scenarios, respectively. The variables {\tt n\char`_sims}, {\tt gmt\char`_A}, and {\tt gmt\char`_N} require vectors of length $M$, which contain the ensemble size, ALL scenario global temperature value, and NAT scenario global temperature value, respectively, for each year. The next five variables specify the run length of the MCMC: {\tt ITER} is the number of post-tuning MCMC iterations; {\tt start\char`_keep} (between 1 and {\tt ITER}) is the number of burn-in iterations that are discarded; {\tt thin} specifies if the saved MCMC output is thinned ({\tt thin} $> 1$) or not ({\tt thin} $= 1$); {\tt n\char`_tune} is the number of MCMC cycles used to tune the RWMH standard deviations, for {\tt n\char`_iter\char`_tune} iterations per tune cycle. The variable {\tt event\char`_type} must be one of {\tt "hot"}, {\tt "cold"}, or {\tt "wet"}; the remaining variables specify the proposal correlations and limits on the logit probabilities.

Next, two functions are provided to summarize the MCMC output. These are

{\singlespacing
\begin{verbatim}
calculate_probs_RR( fit_obj, years = NULL, lp = 0.025, 
  up = 0.975 )
\end{verbatim}
}

\noindent and

{\singlespacing
\begin{verbatim}
exceed_cutoff_RR( fit_obj, gmt_A_ref, gmt_N_ref, cutoff, 
  direction = ">", lp = 0.025, up = 0.975, years = NULL )
\end{verbatim}
}

\noindent The function {\tt calculate\char`_probs\char`_RR} takes the output of {\tt timeRR\char`_month} (using the {\tt fit\char`_obj} variable) and calculates the yearly probabilities ($p_{kt}$) and risk ratio ($RR_t$), as well as provides a point estimate (the posterior median) and a credible interval estimate for each probability and risk ratio. The {\tt lp} and {\tt up} variables allow for the lower and upper quantiles for the credible interval, respectively. The {\tt years} variable allows for an optional specification of what years are represented by the data.

The {\tt exceed\char`_cutoff\char`_RR} function calculates the adjusted risk ratio ($\widetilde{RR}_t$) as well as the exceedance probabilities $\pi$. Again, the function takes the output of {\tt timeRR\char`_month} (using the {\tt fit\char`_obj} variable) and adjusts the corresponding risk ratio to behave as if the arose from a stationary climate where the global temperature for ALL and NAT were {\tt gmt\char`_A\char`_ref} and {\tt gmt\char`_N\char`_ref}, respectively. The {\tt cutoff} variable is either a scalar or vector of length two, indicating a single cutoff for which to compare the adjusted risk ratio against or two values for which to determine the probability of the adjusted risk ratio lying between the two values. The {\tt direction} variable must be one of {\tt ">"}, {\tt "<"}, or {\tt "<>"}, to indicate whether the exceedance probability should be greater than, less than, or between the cutoff value(s) given. The {\tt lp}, {\tt up}, and {\tt years} variables are as in {\tt calculate\char`_probs\char`_RR}.

Finally, a function is provided to plot either the $p_{kt}$ or the $RR_t$:

{\singlespacing
\begin{verbatim}
plot_over_time( m, ub, lb, year, n, x_tic = 3, 
  grp_name = NULL, grp_pal = brewer.pal(3, "Dark2"), 
  title_txt = NULL, ylab = NULL, xlab = NULL,  
  y_lb = min(lb), y_ub = max(ub), 
  plot_p = TRUE, rr_bounds = c(-3,5,0.5,3) )
\end{verbatim}
}

\noindent The {\tt m}, {\tt ub}, and {\tt lb} variables take vectors of length $M$ of, respectively, the point estimate, credible interval upper bound, and credible interval lower bound for each year of the variable to be plotted. The variable {\tt year} contains a vector of which years are included in the analysis; {\tt n} is a vector of the ensemble sizes for each year (for the legend); and {\tt x\char`_tic} indicates the spacing between the years plotted on the $x$-axis. Next, {\tt grp\char`_name} is the name of the legend, and {\tt grp\char`_pal} indicates the color palette to use ({\tt brewer\char`_pal} is a function from the {\tt RColorBrewer} package). The options {\tt title\char`_text}, {\tt ylab}, {\tt xlab}, {\tt y\char`_lb}, and {\tt y\char`_ub} define standard plotting parameters. Finally, {\tt plot\char`_p} indicates whether the probabilities $p_{kt}$ are to be plotted ({\tt TRUE}), or if instead the risk ratio $RR_t$ are to be plotted ({\tt FALSE}), and {\tt rr\char`_bounds} is a vector of length four which contains, respectively, the exponent of 10 for the lower and upper bounds of the $y$-axis, the spacing between labels, and how many decimal places to round the labels.













\end{document}
