\documentclass[times,12pt]{article}

%\usepackage{macros}
\usepackage{mdwlist}
\usepackage[utf8]{inputenc}

% load packages
\usepackage{times,natbib}
\usepackage{balance}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{multirow}
\usepackage[margin=1in]{geometry}
\usepackage{subfigure}
\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{breakurl}
\usepackage{natbib,upgreek}
\usepackage{bbm}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{doi}

\DeclareMathOperator{\logit}{logit}
\DeclareMathOperator{\Var}{Var}
\DeclareMathOperator{\COV}{COV}

\newcommand{\bfbeta}{{\boldsymbol \beta}}
\newcommand{\bfs}{{\bf s}}
\newcommand{\bfx}{{\bf x}}
\def\bSig\mathbf{\Sigma}


%\newcommand{\todo}[1]{\textbf{[\color{green} #1]}}
%\newcommand{\pfc}[1]{\textbf{[\color{blue}Peter: #1]}}
%\newcommand{\rh}[1]{\textbf{[\color{red}Radu: #1]}}
%\newcommand{\vv}[1]{\mbox{\boldmath $#1$}}


\begin{document}

\title{Quantifying the effect of ocean variability on the attribution of extreme climate events to  human influence}

\author{ Mark D. Risser$^{\> a \>*}$, Christopher J. Paciorek$^{\> b}$, D\'aith\'i A. Stone$^{\>c}$, Michael F. Wehner$^{\>c}$}
\maketitle

{\scriptsize
\hskip4ex $^a$ Earth and Environmental Sciences Division, Lawrence Berkeley National Laboratory, Berkeley, CA, USA

\hskip4ex $^b$ Department of Statistics, University of California, Berkeley, Berkeley, CA, USA

\hskip4ex $^c$ Computational Research Division, Lawrence Berkeley National Laboratory, Berkeley, CA, USA

\hskip4ex $^*$ Correspondance email: \tt{mdrisser@lbl.gov}
}


\begin{abstract}
In recent years, the climate change research community has become highly interested in describing the influence of anthropogenic emissions on extreme weather events, commonly termed ``event attribution.'' Limitations in the observational record motivate the use of climate models to estimate anthropogenic influence, while computational limitations motivate the use of uncoupled, atmosphere-only climate models with prescribed ocean conditions. In this approach, large ensembles of high-resolution simulations can be obtained and used to estimate anthropogenic risk; however, fixing the ocean state does not account for the long-term internal variability of the climate system. This source of uncertainty is extremely important, as large internal variability can lead to qualitatively different conclusions about anthropogenic influence.

In this work, we develop a hierarchical Bayesian model to estimate the changing risk over time of extreme weather events due to anthropogenic influences. Unlike related approaches in event attribution, the model allows us to quantify the internal variability present in statements of risk, after adjusting for long-term trends. Furthermore, based on the magnitude of this variability, we develop a metric that allows climate change scientists to identify event types and regions of the globe for which single-year atmosphere-only climate simulations are sufficient for assessing the true risk. The methodology is illustrated by exploring extreme temperature and precipitation events for the northwest coast of South America and northern-central Siberia, as well as summary metrics for the entire globe.
\end{abstract}

\doublespacing

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} \label{introduction}

\subsubsection*{[Introduce event attribution]}

\subsubsection*{[Introduce the C20C+ experiment]}

Limitations in the observational record motivate the use of climate models to estimate anthropogenic influence and risk: first, extremes are (by definition) rare, meaning their occurrence in the observational record is highly sparse; second, no observations at all are available for the counterfactual world. Instead, climate models can be used to simulate the weather conditions that result in extreme events, for both the observed and counterfactual worlds. However, extreme weather is often highly localized, which requires these models to be run on scales or resolutions fine enough to resolve the physics in the model that generate extreme weather. And, for a probabilistic approach, large ensembles of simulations are needed for both scenarios.

This leads to a computational problem: fully-coupled, high resolution models that collectively model land, ocean, and atmospheric processes come with an extremely high computational cost. Fully-coupled models have long memory, meaning that it can take decades to centuries of model time to move away from the bias introduced by the initial conditions. If additional assumptions can be made about the behavior or state of the model, different years can be used as ``replicates''; however, such assumptions are often unreasonable.

To navigate these difficulties, the Climate of the 20th Century Plus (C20C+) Detection and Attribution project uses uncoupled atmosphere-only models (CAM5.1) with prescribed ocean conditions. These models have less spin-up time and can be run much more quickly; furthermore, models for the atmosphere are much more sophisticated than ocean models. % (because the atmosphere is much better understood). 
Additionally, fixing the ocean conditions actually provides some benefit in that it removes dependence on the ocean model's simulation of low frequency processes like El Ni\~no. Using this approach, large ensembles can be obtained, and statistical quantities can be estimated from these ensembles.

Of course, this approach comes with some difficulties. All model-based detection and attribution (list references) introduce various sources of uncertainty, for example, due to finite sampling (from limited ensemble sizes), model parameters and structure, and in forcings for the counterfactual world. However, additional uncertainty is introduced in uncoupled, atmosphere-only simulations. First and foremost is that the ocean state in the counterfactual world is unknown and must be estimated, often based on a scaling and restructuring of the ocean state in the observed world. Furthermore, the strategy of fixing the sea surface temperatures (SSTs) does not account for the internal variability of the ocean system.

Quantification of the uncertainty in statements of risk due to finite sampling have been addressed in the literature (Paciorek et al., Jeon et al., other references), but it is equally important to address the other source of this uncertainty. While each source of uncertainty listed above is important to address, perhaps the most easily accessible source to explore is the effect of the ocean's internal variability. 
%That is, it relatively straightforward to assess   whether variability over time in event probabilities (and therefore also in risk) is due to sampling variability, or instead due to interannual variability in the risk itself. Furthermore, if this interannual variability is present, it is important to quantify the extent of this variability.
To rephrase the question, identifying the effect of oceanic variability boils down to investigating how the experimental design of single-year, atmosphere-only attribution frameworks impact attributional statements. For example, attributional statements may (but should not) be highly sensitive to the specific temporal period used for the study. These sensitivities might differ for different events and different regions in the world.

In any case, assessing changes in risk is relevant anyway because, in the C20C+ experimental design, statements made about risk are actually conditional statements. That is, the statements are conditional on the fixed ocean states used for simulation of the counterfactual world. As described previously, the best strategy would be to obtain ensembles of fully-coupled climate models. However, given the computational challenges of such an approach, it would be helpful to identify events and regions of the world for which fully-coupled simulations are essentially unnecessary.


% In this strategy, the boundary conditions for the ocean state may change fairly drastically from year to year, and this may highly influence statements of risk. For example, how are the probabilities of extreme events for the observed world changing relative to the natural world probabilities? Is the change linear? Nonlinear? In other words: the risk ratio for successive years should be similar (plus or minus small changes), but there is no guarantee that this will happen for atmosphere-only models.

%One popular solution is to scale down the  based on an estimate of the warming attributable to anthropogenic effects, changing both the scale and pattern of ocean temperatures. However, the way in which this is done is not always clear, and this prescription most likely introduces the largest source of uncertainty into the results. The larger issue remains even for the observed world: what impact does fixing the ocean state have on simulations of the world-as-it-is?

%Thus, the primary question attached to using the C20C+ experiment is: how does the use of atmosphere-only models impact attributional statements? In this strategy, the boundary conditions for the ocean state may change fairly drastically from year to year, and this may highly influence statements of risk. For example, how are the probabilities of extreme events for the observed world changing relative to the natural world probabilities? Is the change linear? Nonlinear? In other words: the risk ratio for successive years should be similar (plus or minus small changes), but there is no guarantee that this will happen for atmosphere-only models.


\subsubsection*{[Methodological goals]}

\begin{itemize}
\item Quantify the effect of ocean variability on statements of risk, after adjusting for long-term trends
\item Identify event types and regions of the globe for which single-year, atmosphere-only climate simulations are sufficient for assessing true risk

%Research question: For which areas in the world are conditional attribution statements ``good enough'' to stand in for unconditional statements? (i.e., for which regions are the results not really sensitive to the pre-specified ocean state?)
%\begin{itemize}
%\item e.g.: in Europe, ocean conditions known to have low impact on extremes; in California, ocean conditions known to have huge impact on extremes
%\end{itemize}
%
%
%\item GOAL: summarize (with a map or lookup table) where the conditional statements are good enough to be interpreted as unconditional statements?
%
%\item Introduce Bayesian methods (how much needs to be explained?)
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Methods} \label{methods}

\subsection{Using the risk ratio for attributional statements}

In order to make attributional statements for the impact of anthropogenic influences on the occurrence of extreme events, we will follow a probabilistic extreme event attribution approach (list references) by considering occurrence probabilities $p_A$ and $p_N$. These quantities represent the probability that a predefined event will occur over a predefined spatial and temporal domain, with $p_A$ representing the observed world or ``world as it is'' (henceforth the so-called ALL forcings scenario), including both natural and anthropogenic influences on the climate, and $p_N$ representing the counterfactual scenario of the ``world as it may have been'' (henceforth the so-called NAT forcings scenario) without anthropogenic effects. Following [references], we will consider the ratio of these probabilities, or the risk ratio
\[
RR = \frac{p_A}{p_N},
\]
which mirrors the measurement of risk used in epidemiology and related fields (references) and has a number of desirable statistical properties (references). A risk ratio of larger than one indicates that anthropogenic influences have increased the likelihood of an event's occurrence, while a risk ratio of less than one indicates a decreased likelihood of occurrence.

In this work, we are interested in estimating the risk ratio over time, at a total of $T$ time increments. In what follows, the time increments represent years, but this framework could similarly be used for months, seasons, or decades, for example. Therefore, we must consider 
\[
RR_t = \frac{p_{At}}{p_{Nt}}  \hskip5ex t = 1, \dots, T,
\]
where $p_{At}$ and $p_{Nt}$ are the occurrence probabilities for a predefined time increment $t$. 

\subsection{Modeling probabilities and event definition}

In general, while there are a variety of approaches for estimating $p_A$ and $p_N$ (see, e.g., Paciorek et al., in prep), two popular methods are the nonparameteric (or Binomial) approach and extreme value analysis. The Binomial approach is desirable in that it makes essentially no assumptions about the underlying distribution of the variable of interest (e.g., precipitation or temperature), but becomes essentially useless when the event of interest is extremely rare or unobserved. Extreme value analysis, on the other hand, requires additional assumptions on the behavior of the variable of interest, but is able to estimate the probability of extremely rare events, even those that do not occur in an ensemble or the observational record. 

While the literature contains a variety of research on the sensitivity of a detection and attribution study to event definition [references], such investigations are beyond the scope of this work. In what follows, we will consider events of a prespecified magnitude, and will simply use an empirical quantile to define a threshold for determining whether or not a particular event is ``extreme.'' Furthermore, for simplicity, we will choose a quantile that is extreme but not too extreme: namely, the one in ten year event. In this case, the nonparametric Binomial approach will suffice for modeling probabilities. 

\subsection{Hierarchical statistical model for ALL and NAT probabilities}

Using a nonparametric approach for modeling the probabilities, a hierarchical model useful for quantifying interannual variability in risk can be set up as follows. First,
%Recall that in the C20C+ framework, we have a fixed number of $n_t$ simulations from each year for each of the forcings scenarios (ALL and NAT); furthermore, each simulation can be treated as an independent sample from the population distribution of all possible weather outcomes (discuss which sources of uncertainty are accounted for in this population?). D
define random variables 
\begin{equation} \label{zDefs}
\begin{array}{rcl} 
Z_{At} & = & \text{ the number of ALL simulations of year $t$ in which the event occurred} \\ 
Z_{Nt} & = & \text{ the number of NAT simulations of year $t$ in which the event occurred;} \\ 
\end{array}
\end{equation}
each of these random variables is modeled in a statistical framework as arising from a Binomial experiment with $n_t$ total trials and corresponding success probabilities $p_{kt}$, for $k \in \{A,N\}$ and $t=1, \dots, T$. Furthermore, conditional on the probabilities ${\bf p} = \{ p_{kt}: k \in \{A,N\}, t=1, \dots, T \}$, the random variables ${\bf Z} = \{ Z_{kt}: k \in \{A,N\}, t=1, \dots, T \}$ are independent. In other words, the probability mass function (pmf) of ${\bf Z}$ is
\begin{equation} \label{likelihood}
p({\bf Z} ; {\bf p}) = \prod_{k \in \{A, N\}} \prod_{t=1}^T p(Z_{kt} ; p_{kt}),
\end{equation}
where the individual pmfs are
\[
p(Z_{kt}; p_{kt}) \equiv \mathbbm{P}( Z_{kt} = x )= \frac{n_t!}{ x! (n_t - x)!} \big[ p_{kt} \big]^{x} \big[ 1 - p_{kt} \big]^{n_t - x}, \hskip3ex x = 0, 1, \dots, n_t,
\]
where $\mathbbm{P}(A)$ represents the probability of an event $A$.

Unlike other approaches which have explored estimation and uncertainty quantification for individual time points or years (list references), recall that our goal is to make systematic statements about the behavior of the risk ratio for multiple time points. In doing so, we can make statements about the effect of various explanatory variables on event probabilities and risk in addition to quantifying interannual variability. A hierarchical modeling framework allows us to address each of these questions by borrowing strength or pooling estimates over time.

The first level of the hierarchical model relates the data (${\bf Z}$) to the unknown parameters (${\bf p}$); this was given in (\ref{likelihood}). In the second level of the hierarchy, we specify a model which allows the unknown parameters to vary based on covariate information, while also tying together parameters which correspond to the same time period (i.e., $p_{At}$ and $p_{Nt}$). For this level, we will use
\begin{equation} \label{full_model}
\logit p_{kt} =  {\bf x}_t^\top\bfbeta_N\mathbbm{1}_{\{k=N\}} + \alpha_t + ({\bf w}_t^\top\bfbeta_A + \delta_t ) \mathbbm{1}_{\{k=A\}} ,
\end{equation}
for $k \in \{A,N\}$ and $t=1, \dots, T$. Here, ${\bf x}_t = (1, x_{t1}, \dots, x_{tp})$ is a vector of covariates for the NAT scenario, ${\bf w}_t = (1, w_{t1}, \dots, w_{tq})$ is a vector of covariates for the ALL scenario, $\bfbeta_N = (\beta_{N0}, \dots, \beta_{Np})$ and $\bfbeta_A = (\beta_{A0}, \dots, \beta_{Aq})$ are scenario-specific regression coefficients, and $\mathbbm{1}_{\{\cdot\}}$ is an indicator function. The $\logit$ function is defined as
\[
\logit x \equiv \log \left( \frac{x}{1-x} \right)
\]
and is used here because $\logit: (0,1) \rightarrow \mathbbm{R}$, and the probabilities must lie between 0 and 1. The strategy in (\ref{full_model}) is essentially logistic regression, a special case of the more general statistical technique % of modeling a transformed version of a parameter in a linear regression framework, 
called generalized linear modeling (list references?). A potentially more straightforward representation of the probabilities in (\ref{full_model}) is
\begin{equation*} \label{model1}
\logit p_{Nt} =  {\bf x}_t^\top\bfbeta_N + \alpha_t  \hskip3ex \text{and} \hskip3ex \logit p_{At} = {\bf w}_t^\top\bfbeta_A + \alpha_t + \delta_t .
\end{equation*}
Intuitively, including the scenario-specific covariates ${\bf x}_t$ and ${\bf w}_t$ accounts for any long-term trends present in $p_N$ and $p_A$, respectively.

The final level of the hierarchy ties together the year-specific effects $\{ \alpha_t, \delta_t : t= 1, \dots, T\}$ in order to allow for partial pooling or borrowing of strength across time. This level of the model specifies
%Finally, the $\boldsymbol{\alpha} = (\alpha_1, \dots, \alpha_T)$ and $\boldsymbol{\delta} = (\delta_1, \dots, \delta_T)$ are modeled as random effects, i.e., 
\begin{equation} \label{level3}
\alpha_t \stackrel{\text{iid}}{\sim} N(0, \tau^2) \hskip3ex \text{and} \hskip3ex \delta_{t} \stackrel{\text{iid}}{\sim} N(0, \sigma^2),
\end{equation}
where ``iid'' stands for ``independent and identically distributed'' and $N(a,b)$ denotes a univariate Gaussian (normal) distribution with mean $a$ and variance $b$. In terms of the probabilities, the $\alpha_t$ represent yearly deviations in event probabilities (above and beyond deviations due to the long term trend) that are common to both ALL and NAT scenarios (e.g., due to El Ni\~no or La Ni\~na events), while the $\delta_t$ represent deviations above and beyond the $\alpha_t$ that are specific to the ALL scenario. These $\delta_t$ effects have a specific importance for how risk is modeled, and will be explored further in Section \ref{rrm}.

The partial pooling of (\ref{level3}) is in contrast to \textit{complete pooling}, which would fix each $\alpha_t \equiv \alpha$ and $\delta_t \equiv \delta$, and \textit{no pooling}, which would allow each $\alpha_t$ and $\delta_t$ to be estimated independently of all others. Partial pooling, a popular strategy in random effects modeling (references?) and meta analyses (references?), offers a compromise between these two by allowing each year to have its own effect (i.e., unique $\alpha_t$ and $\delta_t$) while borrowing strength across years by requiring these effects to come from a common distribution. Intuitively, the partial pooling in this approach uses data from all years to estimate the year-specific effects; the actual degree of pooling that occurs is driven by the data itself. 

The implication of borrowing strength over time is that we can parse out two sources of variability in the data: namely, sampling variability and interannual variability. The variance parameters $\sigma^2$ and $\tau^2$ reflect the magnitude of interannual variability: when estimates of these parameters are small, the yearly effects will be tightly clustered (indicating small interannual variability); when the variance parameters are large, the yearly effects will display a wide spread (indicating large interannual variability). The sampling variability is captured by the Binomial framework of (\ref{likelihood}), which for each year/forcing combination is defined in terms of the event probabilities as
\[
\sqrt{\frac{p_{kt}(1-p_{kt})}{n_t}}.
\]

Up until this point, the model has been presented in general form; in this application, the model will be fit within a Bayesian paradigm. [say more]

\subsection{Risk ratio modeling} \label{rrm}

Now, consider what the three-level model specified by (\ref{likelihood}), (\ref{full_model}), and (\ref{level3}) imply about a model for the risk ratio. As a function of $p_A$ and $p_N$, the risk ratio for an individual year (from (\ref{full_model})) is  
\begin{equation} \label{riskratio_calc} 
RR_t = \frac{p_{At}}{p_{Nt}} = \frac{ \logit^{-1}({\bf w}_t^\top\bfbeta_A + \alpha_t + \delta_t)}{ \logit^{-1}({\bf x}_t^\top\bfbeta_N + \alpha_t)}.
\end{equation}
To understand the implications of (\ref{riskratio_calc}) more clearly, %consider a covariate-adjusted version of risk:
%\begin{equation} \label{riskratio_tilde} 
%\widetilde{RR}_t = \frac{ \logit^{-1}(\beta_{A0} + \alpha_t + \delta_t)}{ \logit^{-1}(\beta_{N0} + \alpha_t)},
%\end{equation}
%i.e., the risk ratio in (\ref{riskratio_calc}) with the effect of the covariate information removed so that only the intercept $\beta_{k0}$ remains. N
note that in dealing with extreme events, the probabilities are often small (i.e., near zero), in which case $\logit(p) = \log(p) - \log(1-p) \approx \log(p)$ and $\logit^{-1}(x) \approx \exp\{x\}$. So, we can approximate the risk ratio (\ref{riskratio_calc}) as
\begin{equation} \label{approxRR}
{RR}_t \approx \frac{ \exp \left\{ \beta_{A0} + \beta_{A1}w_{t1} + \alpha_t + \delta_t \right\} }{ \exp \left\{\beta_{N0} + \beta_{N1}x_{t1} + \alpha_t \right\} }
=  {RR}_0 \times \exp\{\beta_{A1}w_{t1} - \beta_{N1}x_{t1}\} \times \exp\{\delta_t\}
\end{equation}
where, for simplicity, $p=q=1$. So, from (\ref{approxRR}), the risk is approximated by three pieces: first, ${RR}_0 = \exp \left\{ \beta_{A0} - \beta_{N0} \right\}$, or the ``baseline'' risk ratio for the entire time interval; second, $\exp\{\beta_{A1}w_{t1} - \beta_{N1}x_{t1}\}$, a multiplicative scaling due to the covariates; and finally $\exp\{\delta_t\}$, a scaling for the risk ratio in a particular year. Note that the $\alpha_t$ term, which appears in both numerator and denominator, cancels out. Thus, we can interpret $\sigma^2 = \Var(\delta_t)$, $t=1, \dots, T$ as the interannual variability in the ``log'' risk ratio over time or, alternatively, variability in the $p_{At}$ above and beyond variability in the $p_{Nt}$. And, note that this parameter represents the year-to-year variability in risk after adjusting for relevant covariate information (i.e., the long-term trend).

Unfortunately, the fact that this variance parameter is on the $\logit$ scale makes it difficult to interpret with respect to risk on the raw scale. Furthermore, the variance by itself does not speak to the magnitude of the risk, represented by ${RR}_0$, which is highly relevant in statements of risk. For example, year-to-year variation of $\pm2$ in risk is important when the average risk is around $1$, but much less so if the average risk is $100$. To account for this important distinction, we will use the  coefficient of variation ($\COV$), which summarizes the year-to-year variability in risk on the raw scale and relative to the average risk for the time period of interest. Formally, 
\begin{equation} \label{caCOV}
\COV \equiv \frac{{S}}{\hskip1ex\overline{RR}\hskip1ex},
\end{equation}
where 
\[
\overline{RR} = \frac{1}{T} \sum_{t=1}^T {RR}_t
\]
represents the average risk over time and
\[
{S} = \sqrt{ \frac{1}{T-1} \sum_{t=1}^T \left( {RR}_t - \overline{RR} \right)^2 }
\]
represents the standard deviation in year-to-year risk (again, after adjusting for covariates). Scaling a measure of the interannual variability on the raw scale (${S}$) by the average risk ($\overline{RR}$) presents a measure of year-to-year variation in risk that is relative to the magnitude of the risk. Thus, $\COV$ can be used as an intuitive and appropriate measure for quantifying the interannual variability present in attribution statements.

Interestingly, there is an approximate equivalence between the COV as it is defined here and $\sigma = \sqrt{\Var(\delta_t)}$. Returning to (\ref{approxRR}), note that
\[
\Var( \log RR_t ) \approx \Var( \log RR_0 + \beta_{A1}w_{t1} - \beta_{N1}x_{t1} + \delta_t ) = \Var( \delta_t) = \sigma^2.
\]
(The next to last equality comes because $RR_0$ and $\beta_{A1}w_{t1} - \beta_{N1}x_{t1}$ are non-stochastic terms and do not impact the variance.) However, for a random variable $X$, the delta method gives the asymptotic variance of a function $g(X)$ as $\Var \big( g(X) \big) = \big[g'(E[X])\big]^2 \times \Var(X)$. In this case, with $g(\cdot) = \log(\cdot)$, we have
\[
\sigma \approx \sqrt{\Var( \log RR_t )} = \sqrt{\frac{\Var( RR_t )}{(E[RR_t])^2}} \approx \COV
\]
Thus, in some ways, it doesn't matter whether the metric is motivated in terms of the standard deviation of the $\delta_t$ or the coefficient or variation.

\subsection{A benchmark criterion for interpreting conditional statements of risk}

Recall from Section \ref{introduction} that attribution statements made from atmosphere-only climate model runs such as those in the C20C+ experiment are in fact conditional statements, being conditional on the prescribed ocean state used in the natural world simulations. When considering changes in risk over time, if the risk ratio changes very little (after correcting for long term trends using a covariate), this suggests that the pre-specified ocean conditions may not actually be having a large impact on the statements of risk, and the conditional statements of risk can be interpreted as ``unconditional.'' That is, in this case, regardless of which year was chosen for use in an attribution study, it is highly likely that the same conclusion would be made regarding the change in likelihood of an extreme climate event. However, on the other hand, if the risk ratio is highly volatile over time, this suggests that the risk is highly sensitive to the particular ocean state used for the natural world, and the overall conclusion of an attribution study may be very sensitive to the specific year chosen for analysis. Clearly, in the atmosphere-only climate model framework for attributing risk, we want to avoid cases that fall into the latter category. 

We now argue that the $\COV$ can be used as a measure to indicate when the results of a conditional attribution study (conditional on the ocean state) can and cannot be interpreted as an unconditional statement of risk. [Say more.]
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Case studies using the C20C+ ensembles: the southern Andean community and Krasnoyarsk regions} \label{results}

As indicated in Section \ref{introduction}, the C20C+ ensembles will be used to estimate risk for hot, cold, and wet extremes. [D\'aith\'i: say why we ignore dry for now.] Currently, C20C+ simulations of the ALL and NAT scenarios are available from 1960 to 2013, providing 50-member ensembles for the early years all the way up to 400-member ensembles for the most recent years (a summary of the ensemble sizes for each year is given in Table \ref{ensSize}). Temperature and precipitation is aggregated into 58 geopolitical land-regions, which correspond to the regions used for the Weather Risk Attribution Forecast (WRAF; references). All of the WRAF regions are shown in Figure \ref{WRAF}.

\begin{table}[!t]
\caption{Ensemble sizes for the C20C+ simulations from 1960-2013.}
\begin{center}
\begin{tabular}{|c|c|}
\hline
\textbf{Year range ($t$)} & \textbf{Ensemble size ($n_t$)} \\ \hline \hline
1960--1996 & 50 \\ \hline
1997--2010 & 100 \\ \hline
2011--2013 & 400 \\
\hline
\end{tabular}
\end{center}
\label{ensSize}
\end{table}%

The methods outlined in Section \ref{methods} will be illustrated using two case studies. The case studies will use years as the temporal resolution, and a WRAF region for the spatial domain: (a) the southern Andean community (comprising Peru and Bolivia, represented by the blue region on the west coast of South America in Figure \ref{WRAF}) and (b) Krasnoyarsk (in central Russia, represented by the yellow region in northern Asia). As will be illustrated, these two regions are chosen as a case study because they exhibit very different behavior in extremes.

\begin{figure}[!t]
\begin{center}
\vskip4ex
	\includegraphics[trim={0 304 0 0mm}, clip, width = \textwidth]{Daithi_Regions.pdf}
\vskip1ex
\caption{Weather Risk Attribution Forecast (WRAF) regions.}
\label{WRAF}
\end{center}
\end{figure}

In this application of the C20C+ experiment, temperature and precipitation measurements for each region are further aggregated by month. For year $t$, we then have $12\times t$ measurements from each of the $n_t$ simulations. The one-in-ten year event for monthly data corresponds to the $100(1 - 0.1/12)^{\text{th}} = 99.17^{\text{th}}$ percentile for large extremes (hot, wet) and the $100(0.1/12)^{\text{th}} = 0.83^{\text{th}}$ percentile for small extremes (cold). In order to avoid an inappropriate bias for more recent years with a larger ensemble size, the empirical percentiles will be calculated from the 50-member ensemble that covers the entire time period of the study, from 1960-2013. For cold and wet events, the empirical percentiles will be calculated from the ALL simulations; for hot events, the empirical percentile will be calculated from the NAT simulations. The count variables for year $t$ (previously defined by $Z_{At}$ and $Z_{Nt}$) will be calculated as the total number of monthly values per simulation of the $12\times n_t$ total values that exceed the given threshold.

A time series of the probabilities $p_{At}$ and $p_{Nt}$ can be constructed using the empirical probabilities
\[
\widehat{p}_{kt} = \frac{Z_{kt}}{12\times n_t}, \hskip3ex k \in \{A,N\}.
\]
The empirical probabilities of each event type for the Southern Andean Community and Krasnoyarsk are provided in the Appendix, in Figures \ref{sacProbs} and \ref{KrasProbs}. 

In order to account for the long-term trend present in the extreme event probabilities, a scenario-specific global mean temperature covariate will be used. Actually, the covariate used will be the average surface air (?) temperature (over land? land and ocean?) between 50$^\circ$S and 50$^\circ$N, calculated as an ensemble average from the 50 members that covered the entire time period from 1960 to 2013. The scenario-specific temperature measurements are plotted in Figure \ref{temps}.


\begin{itemize}
\item Predictive distributions of $p_A$, $p_N$, and $RR$ over time. Plots: Figure \ref{probsRR} for wet, hot, cold.
\item Discuss and interpret parameter estimates. Plots: Figure \ref{SDs}, Figure \ref{GMTcoefs}. 
\item Discuss the $\COV$
\end{itemize}

\begin{figure}[!t]
\begin{center}
\includegraphics[width=\textwidth]{temperatures.pdf}
\caption{Global mean temperature measurements between 50$^\circ$S and 50$^\circ$N, calculated as an ensemble average from the 50 members that covered the entire time period from 1960 to 2013.}
\label{temps}
\end{center}
\end{figure}

%\begin{figure}[!t]
%\begin{center}
%\includegraphics[width=\textwidth]{PB_SDs.pdf}
%\caption{Posterior distribution for $\sigma$, the standard deviation in the (log) risk ratios for each type of extreme, in the southern Andean region.}
%\label{SDs}
%\end{center}
%\end{figure}
%
%\begin{figure}[!t]
%\begin{center}
%\includegraphics[width=\textwidth]{GMTcoefs.pdf}
%\caption{Posterior distribution for the regression coefficient estimates for global temperature in the (logit) probabilities in each scenario for each type of extreme, in the southern Andean region. (Note: the temperature measurements were standardized.)}
%\label{GMTcoefs}
%\end{center}
%\end{figure}
%
%
%\begin{figure}[!t]
%\begin{center}
%\includegraphics[width=\textwidth]{PB_WET_pA_pN_RR.pdf}
%\caption{Estimated probabilities and risk ratios for wet extremes in the southern Andean region.}
%\label{probsRR}
%\end{center}
%\end{figure}
%


\section{Results for all regions}

Jump to global results: maps over the different regions illustrating each of the components from the case studies

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion} \label{discussion}

\clearpage
\begin{appendix}

\section{Supplemental Figures}

\begin{figure}[!h]
\begin{center}
\includegraphics[width=\textwidth]{southAndeanComm.pdf}
\caption{Empirical event probabilities for the Southern Andean Community.}
\label{sacProbs}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[width=\textwidth]{krasnoyarsk.pdf}
\caption{Empirical event probabilities for Krasnoyarsk.}
\label{KrasProbs}
\end{center}
\end{figure}

\end{appendix}



% ======================================================================

\singlespacing
%\bibliographystyle{apalike} \bibliography{MDRResearch}

\end{document}
