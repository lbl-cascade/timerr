"50% of years <1",
"75% of years <1",
"100% of years <1") ) +
theme( axis.text.x = element_text( angle = 45, hjust = 0.9, size = 10 ),
axis.text.y = element_text( size = 14),
legend.text = element_text(size = 15),
legend.title = element_text(size = 17),
title = element_text(size = 20),
legend.position = "bottom",
legend.box = "horizontal",
legend.direction = "vertical",
strip.text = element_text(size = 16)) +
guides(col = guide_legend(keywidth = 3, keyheight = 1,
override.aes = list(size=1.1))) +
geom_vline( xintercept = 2.5, color = "gray43") + annotate("text", x = 3.5, y = .2, label = "30S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 7.5, color = "gray43") + annotate("text", x = 8.5, y = .2, label = "15S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 15.5, color = "gray43") + annotate("text", x = 16, y = .2, label = "0", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 19.5, color = "gray43") + annotate("text", x = 20.5, y = .2, label = "15N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 32.5, color = "gray43") + annotate("text", x = 33.5, y = .2, label = "30N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 41.5, color = "gray43") + annotate("text", x = 42.5, y = .2, label = "45N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 49.5, color = "gray43") + annotate("text", x = 50.5, y = .2, label = "60N", color = "gray43" )
# Directories =============================================
setwd("~/CASCADE_Projects/timerr/final code/")
# Necessary libraries =====================================
library(MASS)
library(ggplot2)
library(gridExtra)
library(RColorBrewer)
library(colorspace)
# Functions ===============================================
# Calculate simulations for which a proportion of the risk exceeds a cutoff
exceed_cutoff_RR <- function( pooled.obj, gmt.A.ref, gmt.N.ref, cutoff, direction = ">",
lowerPerc = 0.025, upperPerc = 0.975, years = NULL,
years.use = NULL ){
betaN.save <- pooled.obj$betaN.save
betaA.save <- pooled.obj$betaA.save
alpha.save <- pooled.obj$alpha.save
delta.save <- pooled.obj$delta.save
M <- nrow(delta.save)
if( is.null(years.use) == FALSE ){
delta.save <- delta.save[(M-years.use+1):M,]
alpha.save <- alpha.save[(M-years.use+1):M,]
}
M <- nrow(delta.save)
J <- ncol(delta.save)
if( is.null(years) ){ years <- 1:M }
pA.samp <- pN.samp <- matrix(NA,J,M)
if( pooled.obj$event.type == "hot" || pooled.obj$event.type == "wet" ){
for(m in 1:M){
pA.samp[,m] <- expit(betaA.save[1,] + betaA.save[2,]*gmt.A.ref + alpha.save[m,] + delta.save[m,])
pN.samp[,m] <- expit(betaN.save[1,] + betaN.save[2,]*gmt.N.ref + alpha.save[m,])
}
}
if( pooled.obj$event.type == "cold" ){
for(m in 1:M){
pN.samp[,m] <- expit(betaA.save[1,] + betaA.save[2,]*gmt.N.ref + alpha.save[m,] + delta.save[m,])
pA.samp[,m] <- expit(betaN.save[1,] + betaN.save[2,]*gmt.A.ref + alpha.save[m,])
}
}
RR.samp <- pA.samp/pN.samp
if( direction == ">"){
exceedence.prob.per.samp <- rowMeans(RR.samp > cutoff)
}
if( direction == "<"){
exceedence.prob.per.samp <- rowMeans(RR.samp < cutoff)
}
if( direction == "<>"){
exceedence.prob.per.samp <- rowMeans(RR.samp > cutoff[1] & RR.samp < cutoff[2])
}
# Summarize shifted RR
results <- data.frame(
year = years,
RR = apply(RR.samp,2,median),
RRlb = apply(RR.samp,2,function(x){ quantile(x,lowerPerc) }),
RRub = apply(RR.samp,2,function(x){ quantile(x,upperPerc) })
)
output <- list(
shifted.RR.samp = RR.samp,
shifted.RR.results = results,
exceedence.prob.samp = exceedence.prob.per.samp,
exceedence.median = median(exceedence.prob.per.samp),
exceedence.lb = as.numeric(quantile(exceedence.prob.per.samp, lowerPerc)),
exceedence.ub = as.numeric(quantile(exceedence.prob.per.samp, upperPerc))
)
return(output)
}
source("f_CIS_MCMC.R")
source("f_summarize_plot.R")
source("~/CASCADE_Projects/timerr/regional summaries/plot_regions.R")
setwd("~/CASCADE_Projects/timerr/estimate prob exceedance/output/")
M
# Calculate exceedence probabilities ======================
# All WRAF Data
load("~/CASCADE_Projects/spatialMT/estimation for WRAF/WRAFdf.RData")
load("~/CASCADE_Projects/timerr/WRAF data prep/WRAF_regions_82_13.RData")
WRAF_regions <- WRAF_regions_82_13
M <- length(WRAF_regions)
# GMTs
load("~/CASCADE_Projects/timerr/global_mean_temps/ALLtemps1959_2013_50member.RData")
load("~/CASCADE_Projects/timerr/global_mean_temps/NATtemps1959_2013_50member.RData")
# Standardize covariates
gmt.A <- as.numeric(scale(ALLtemps1959_2013_50member[-c(1:23)]))
gmt.N <- as.numeric(scale(NATtemps1959_2013_50member[-c(1:23)]))
n.sims <- c(rep(50,15), rep(100,14), rep(400,3))
# Storage
region.names <- rep(NA,M)
HOT.exceed.mean <- HOT.exceed.LB <- HOT.exceed.UB <- matrix(NA, M, 3)
COLD.exceed.mean <- COLD.exceed.LB <- COLD.exceed.UB <- matrix(NA, M, 3)
WET.exceed.mean <- WET.exceed.LB <- WET.exceed.UB <- matrix(NA, M, 4)
HOT.exceed.mean2 <- HOT.exceed.LB2 <- HOT.exceed.UB2 <- matrix(NA, M, 3)
COLD.exceed.mean2 <- COLD.exceed.LB2 <- COLD.exceed.UB2 <- matrix(NA, M, 3)
WET.exceed.mean2 <- WET.exceed.LB2 <- WET.exceed.UB2 <- matrix(NA, M, 4)
# Columns correspond to different cutoffs
HOT.cutoffs <- data.frame(cutoff = c(1, 2, 10), direction = rep(">", 3) )
COLD.cutoffs <- data.frame(cutoff = c(1, 1/2, 1/10), direction = rep("<", 3) )
WET.cutoffs <- data.frame(cutoff1 = c(1, 1, 1/2), cutoff1 = c(NA, NA, 2),
direction = c("<", ">","<>") )
# Reference year for GMT
Q.l <- 0.025
Q.u <- 0.975
# Regions
for(i in 1:M){ region.names[i] <- WRAF_regions[[i]]$region }
# Load alternate names and central latitudes
load("~/CASCADE_Projects/timerr/plotWRAF/regionlatdf.RData")
# Define continents
continents <- rep(NA,M)
continents[c(1,2,6,11:15)] <- "South America" #8
continents[c(16:20,36,41,54:58)] <- "North America" #12
continents[c(38,39,42,7,29,21:24,43:46,49)] <- "Asia" #14
continents[c(8:10)] <- "Aus" #3
continents[c(30,34,35,47,48)] <- "Europe" #5
continents[c(25:28,40,50:53,3:5,31:33,37)] <- "Africa" #16
continents <- as.factor(continents)
#============================================
# COLD EVENTS
#============================================
cold.shifted.RR.results <- cold.shifted.RR <- list()
for(i in 1:M){ # Loop over regions
# Load MCMC posterior samples and p/RR samples
load(paste("~/CASCADE_Projects/timerr/final code/",WRAF_regions[[i]]$region,"_MCMCoutput.RData",sep=""))
for(j in 1:nrow(COLD.cutoffs)){ # Loop over cutoffs
temp <- exceed_cutoff_RR( pooled.obj = MCMCoutput[[5]], # 5 corresponds to COLD
gmt.A.ref = mean(gmt.A[28:32]),
gmt.N.ref = mean(gmt.N[28:32]),
cutoff = COLD.cutoffs$cutoff[j],
direction = COLD.cutoffs$direction[j] )
COLD.exceed.mean[i,j] <- temp$exceedence.median
COLD.exceed.UB[i,j] <- temp$exceedence.ub
COLD.exceed.LB[i,j] <- temp$exceedence.lb
}
cold.shifted.RR.results[[i]] <- temp$shifted.RR.results
cold.shifted.RR[[i]] <- temp$shifted.RR.samp
cat(i, " ")
}
# Version 2: use only 1997-2013
for(i in 1:M){ # Loop over regions
# Load MCMC posterior samples and p/RR samples
load(paste("~/CASCADE_Projects/timerr/final code/",WRAF_regions[[i]]$region,"_MCMCoutput.RData",sep=""))
for(j in 1:nrow(COLD.cutoffs)){ # Loop over cutoffs
temp <- exceed_cutoff_RR( pooled.obj = MCMCoutput[[5]], # 5 corresponds to COLD
gmt.A.ref = mean(gmt.A[28:32]),
gmt.N.ref = mean(gmt.N[28:32]),
cutoff = COLD.cutoffs$cutoff[j],
direction = COLD.cutoffs$direction[j],
years.use = length(1997:2013))
COLD.exceed.mean2[i,j] <- temp$exceedence.median
COLD.exceed.UB2[i,j] <- temp$exceedence.ub
COLD.exceed.LB2[i,j] <- temp$exceedence.lb
}
cat(i, " ")
}
p.COLD.df2 <- data.frame(
region = region.names,
region2 = region.lat.df$region,
continents = continents,
mean.lat = region.lat.df$mean.lat,
median.lat = region.lat.df$median.lat,
p.hat.exd1 = COLD.exceed.mean2[,1],
p.lb.exd1 = COLD.exceed.LB2[,1],
p.ub.exd1 = COLD.exceed.UB2[,1],
p.hat.exd2 = COLD.exceed.mean2[,2],
p.lb.exd2 = COLD.exceed.LB2[,2],
p.ub.exd2 = COLD.exceed.UB2[,2],
p.hat.exd10 = COLD.exceed.mean2[,3],
p.lb.exd10 = COLD.exceed.LB2[,3],
p.ub.exd10 = COLD.exceed.UB2[,3]
)
class.vec2 <- rep(NA,M)
for(i in 1:M){
if(p.COLD.df2$p.lb.exd1[i] <= 0.95){
class.vec2[i] <- "Potentially inconsistent, but inconclusive" } # "Statements are inconsistent" }
if(p.COLD.df2$p.lb.exd1[i] > 0.95 & p.COLD.df2$p.lb.exd2[i] <= 0.95){ class.vec2[i] <- "Consistent for cutoff = 1" }
if(p.COLD.df2$p.lb.exd2[i] > 0.95 & p.COLD.df2$p.lb.exd10[i] <= 0.95){ class.vec2[i] <- "Consistent for cutoff = 1/2" }
if(p.COLD.df2$p.lb.exd10[i] > 0.95){ class.vec2[i] <- "Consistent for cutoff = 1/10" }
}
class.vec2 <- as.factor(class.vec2)
class.vec2 <- ordered(class.vec2, levels = levels(class.vec2)[c(4, 1, 3, 2)])
class.vec2
p.COLD.df2
p.COLD.df2
class.vec2
class.vec2 <- rep(NA,M)
for(i in 1:M){
if(p.COLD.df2$p.ub.exd1[i] <= 0.95){
class.vec2[i] <- "Inconsistent or RR approx. 1" }
if(p.COLD.df2$p.lb.exd1[i] <= 0.95){
class.vec2[i] <- "Potentially inconsistent, but inconclusive" } # "Statements are inconsistent" }
if(p.COLD.df2$p.lb.exd1[i] > 0.95 & p.COLD.df2$p.lb.exd2[i] <= 0.95){ class.vec2[i] <- "Consistent for cutoff = 1" }
if(p.COLD.df2$p.lb.exd2[i] > 0.95 & p.COLD.df2$p.lb.exd10[i] <= 0.95){ class.vec2[i] <- "Consistent for cutoff = 1/2" }
if(p.COLD.df2$p.lb.exd10[i] > 0.95){ class.vec2[i] <- "Consistent for cutoff = 1/10" }
}
class.vec2 <- as.factor(class.vec2)
class.vec2
class.vec2 <- rep(NA,M)
for(i in 1:M){
if(p.COLD.df2$p.lb.exd1[i] <= 0.95){
class.vec2[i] <- "Potentially inconsistent, but inconclusive" } # "Statements are inconsistent" }
if(p.COLD.df2$p.ub.exd1[i] <= 0.95){
class.vec2[i] <- "Inconsistent or RR approx. 1" }
if(p.COLD.df2$p.lb.exd1[i] > 0.95 & p.COLD.df2$p.lb.exd2[i] <= 0.95){ class.vec2[i] <- "Consistent for cutoff = 1" }
if(p.COLD.df2$p.lb.exd2[i] > 0.95 & p.COLD.df2$p.lb.exd10[i] <= 0.95){ class.vec2[i] <- "Consistent for cutoff = 1/2" }
if(p.COLD.df2$p.lb.exd10[i] > 0.95){ class.vec2[i] <- "Consistent for cutoff = 1/10" }
}
class.vec2 <- as.factor(class.vec2)
class.vec2
class.vec2 <- ordered(class.vec2, levels = levels(class.vec2)[c(5,4, 1, 3, 2)])
p.COLD.df2$classification <- class.vec2
save(p.COLD.df2, file = "pCOLDdf2.RData")
plot.df <- data.frame( med = p.COLD.df2$p.hat.exd1, ub = p.COLD.df2$p.ub.exd1,
lb = p.COLD.df2$p.lb.exd1,
regions = as.factor(p.COLD.df2$region2),
color.var = as.factor(p.COLD.df2$continents),
sort.var = p.COLD.df2$mean.lat,
shade.var = p.COLD.df2$classification )
plot.df <- plot.df[ order(plot.df$sort.var), ]
temp.reg <- as.character(plot.df$regions)
plot.df$regions <- ordered(temp.reg, levels = temp.reg)
plot.df$newVar <- plot.df$shade.var
ggplot(plot.df, aes(x=regions, y=med)) +
ylab(NULL) + xlab(NULL) + ggtitle(NULL) +
geom_pointrange( aes(x = regions, y = med, ymin = lb, ymax = ub, col = newVar ), size = 1.1) +
scale_color_manual( name = "Does oceanic variability affect consistency\nof positive attribution statements?",
values = c( "gray55", "black", brewer.pal(9, "Blues")[c(4,6,8)]) ) +
geom_hline(aes(yintercept=0.95)) +
geom_hline(aes(yintercept=0.05)) +
scale_y_continuous( limits = c(0.15,1),
breaks = c(0, 0.25, 0.5, 0.75, 1),
labels = c("0% of years <1",
"25% of years <1",
"50% of years <1",
"75% of years <1",
"100% of years <1") ) +
theme( axis.text.x = element_text( angle = 45, hjust = 0.9, size = 10 ),
axis.text.y = element_text( size = 14),
legend.text = element_text(size = 15),
legend.title = element_text(size = 17),
title = element_text(size = 20),
legend.position = "bottom",
legend.box = "horizontal",
legend.direction = "vertical",
strip.text = element_text(size = 16)) +
guides(col = guide_legend(keywidth = 3, keyheight = 1,
override.aes = list(size=1.1))) +
geom_vline( xintercept = 2.5, color = "gray43") + annotate("text", x = 3.5, y = .2, label = "30S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 7.5, color = "gray43") + annotate("text", x = 8.5, y = .2, label = "15S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 15.5, color = "gray43") + annotate("text", x = 16, y = .2, label = "0", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 19.5, color = "gray43") + annotate("text", x = 20.5, y = .2, label = "15N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 32.5, color = "gray43") + annotate("text", x = 33.5, y = .2, label = "30N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 41.5, color = "gray43") + annotate("text", x = 42.5, y = .2, label = "45N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 49.5, color = "gray43") + annotate("text", x = 50.5, y = .2, label = "60N", color = "gray43" )
class.vec2 <- rep(NA,M)
for(i in 1:M){
if(p.COLD.df2$p.lb.exd1[i] <= 0.95){
class.vec2[i] <- "Potentially inconsistent, but inconclusive" } # "Statements are inconsistent" }
if(p.COLD.df2$p.ub.exd1[i] <= 0.95){
class.vec2[i] <- "Inconsistent or RR approx. 1" }
if(p.COLD.df2$p.lb.exd1[i] > 0.95 & p.COLD.df2$p.lb.exd2[i] <= 0.95){ class.vec2[i] <- "Consistent for cutoff = 1" }
if(p.COLD.df2$p.lb.exd2[i] > 0.95 & p.COLD.df2$p.lb.exd10[i] <= 0.95){ class.vec2[i] <- "Consistent for cutoff = 1/2" }
if(p.COLD.df2$p.lb.exd10[i] > 0.95){ class.vec2[i] <- "Consistent for cutoff = 1/10" }
}
class.vec2 <- as.factor(class.vec2)
levels(class.vec2)
class.vec2 <- ordered(class.vec2, levels = levels(class.vec2)[c(4, 5, 1, 3, 2)])
p.COLD.df2$classification <- class.vec2
save(p.COLD.df2, file = "pCOLDdf2.RData")
plot.df <- data.frame( med = p.COLD.df2$p.hat.exd1, ub = p.COLD.df2$p.ub.exd1,
lb = p.COLD.df2$p.lb.exd1,
regions = as.factor(p.COLD.df2$region2),
color.var = as.factor(p.COLD.df2$continents),
sort.var = p.COLD.df2$mean.lat,
shade.var = p.COLD.df2$classification )
plot.df <- plot.df[ order(plot.df$sort.var), ]
temp.reg <- as.character(plot.df$regions)
plot.df$regions <- ordered(temp.reg, levels = temp.reg)
plot.df$newVar <- plot.df$shade.var
ggplot(plot.df, aes(x=regions, y=med)) +
ylab(NULL) + xlab(NULL) + ggtitle(NULL) +
geom_pointrange( aes(x = regions, y = med, ymin = lb, ymax = ub, col = newVar ), size = 1.1) +
scale_color_manual( name = "Does oceanic variability affect consistency\nof positive attribution statements?",
values = c( "gray55", "black", brewer.pal(9, "Blues")[c(4,6,8)]) ) +
geom_hline(aes(yintercept=0.95)) +
geom_hline(aes(yintercept=0.05)) +
scale_y_continuous( limits = c(0.15,1),
breaks = c(0, 0.25, 0.5, 0.75, 1),
labels = c("0% of years <1",
"25% of years <1",
"50% of years <1",
"75% of years <1",
"100% of years <1") ) +
theme( axis.text.x = element_text( angle = 45, hjust = 0.9, size = 10 ),
axis.text.y = element_text( size = 14),
legend.text = element_text(size = 15),
legend.title = element_text(size = 17),
title = element_text(size = 20),
legend.position = "bottom",
legend.box = "horizontal",
legend.direction = "vertical",
strip.text = element_text(size = 16)) +
guides(col = guide_legend(keywidth = 3, keyheight = 1,
override.aes = list(size=1.1))) +
geom_vline( xintercept = 2.5, color = "gray43") + annotate("text", x = 3.5, y = .2, label = "30S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 7.5, color = "gray43") + annotate("text", x = 8.5, y = .2, label = "15S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 15.5, color = "gray43") + annotate("text", x = 16, y = .2, label = "0", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 19.5, color = "gray43") + annotate("text", x = 20.5, y = .2, label = "15N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 32.5, color = "gray43") + annotate("text", x = 33.5, y = .2, label = "30N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 41.5, color = "gray43") + annotate("text", x = 42.5, y = .2, label = "45N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 49.5, color = "gray43") + annotate("text", x = 50.5, y = .2, label = "60N", color = "gray43" )
pdf("cold_shaded_exceed_prob2.pdf",
height = 8, width = 16)
ggplot(plot.df, aes(x=regions, y=med)) +
ylab(NULL) + xlab(NULL) + ggtitle(NULL) +
geom_pointrange( aes(x = regions, y = med, ymin = lb, ymax = ub, col = newVar ), size = 1.1) +
scale_color_manual( name = "Does oceanic variability affect consistency\nof positive attribution statements?",
values = c( "gray55", "black", brewer.pal(9, "Blues")[c(4,6,8)]) ) +
geom_hline(aes(yintercept=0.95)) +
geom_hline(aes(yintercept=0.05)) +
scale_y_continuous( limits = c(0.15,1),
breaks = c(0, 0.25, 0.5, 0.75, 1),
labels = c("0% of years <1",
"25% of years <1",
"50% of years <1",
"75% of years <1",
"100% of years <1") ) +
theme( axis.text.x = element_text( angle = 45, hjust = 0.9, size = 10 ),
axis.text.y = element_text( size = 14),
legend.text = element_text(size = 15),
legend.title = element_text(size = 17),
title = element_text(size = 20),
legend.position = "bottom",
legend.box = "horizontal",
legend.direction = "vertical",
strip.text = element_text(size = 16)) +
guides(col = guide_legend(keywidth = 3, keyheight = 1,
override.aes = list(size=1.1))) +
geom_vline( xintercept = 2.5, color = "gray43") + annotate("text", x = 3.5, y = .2, label = "30S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 7.5, color = "gray43") + annotate("text", x = 8.5, y = .2, label = "15S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 15.5, color = "gray43") + annotate("text", x = 16, y = .2, label = "0", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 19.5, color = "gray43") + annotate("text", x = 20.5, y = .2, label = "15N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 32.5, color = "gray43") + annotate("text", x = 33.5, y = .2, label = "30N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 41.5, color = "gray43") + annotate("text", x = 42.5, y = .2, label = "45N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 49.5, color = "gray43") + annotate("text", x = 50.5, y = .2, label = "60N", color = "gray43" )
dev.off()
plot_sortCentLat(
m = COLD.df$sigma.med, ub = COLD.df$sigma.UB, sort.var = p.COLD.df$mean.lat,
lb = COLD.df$sigma.LB, regions = p.COLD.df$region2, ylab="",
grp.pal = c( "gray55", "black", brewer.pal(9, "Blues")[c(4,6,8)]),
grp.name = "Does oceanic variability affect consistency\nof negative attribution statements?",
title.txt = NULL,
xlab="", continents = p.COLD.df$continents, shade = p.COLD.df2$classification ) +
theme( legend.margin = unit(-1, "cm")) + ylab(expression(sigma)) +
geom_vline( xintercept = 2.5, color = "gray43") + annotate("text", x = 3.5, y = 1.8, label = "30S", color = "gray43" ) +
geom_vline( xintercept = 7.5, color = "gray43") + annotate("text", x = 8.5, y = 1.8, label = "15S", color = "gray43" ) +
geom_vline( xintercept = 15.5, color = "gray43") + annotate("text", x = 15, y = 1.8, label = "0", color = "gray43" ) +
geom_vline( xintercept = 19.5, color = "gray43") + annotate("text", x = 20.5, y = 1.8, label = "15N", color = "gray43" ) +
geom_vline( xintercept = 32.5, color = "gray43") + annotate("text", x = 33.5, y = 1.8, label = "30N", color = "gray43" ) +
geom_vline( xintercept = 41.5, color = "gray43") + annotate("text", x = 42.5, y = 1.8, label = "45N", color = "gray43" ) +
geom_vline( xintercept = 49.5, color = "gray43") + annotate("text", x = 50.5, y = 1.8, label = "60N", color = "gray43" )
pdf("cold_internalVar.pdf",
height = 8.5, width = 12)
plot_sortCentLat(
m = COLD.df$sigma.med, ub = COLD.df$sigma.UB, sort.var = p.COLD.df$mean.lat,
lb = COLD.df$sigma.LB, regions = p.COLD.df$region2, ylab="",
grp.pal = c( "gray55", "black", brewer.pal(9, "Blues")[c(4,6,8)]),
grp.name = "Does oceanic variability affect consistency\nof negative attribution statements?",
title.txt = NULL,
xlab="", continents = p.COLD.df$continents, shade = p.COLD.df2$classification ) +
theme( legend.margin = unit(-1, "cm")) + ylab(expression(sigma)) +
geom_vline( xintercept = 2.5, color = "gray43") + annotate("text", x = 3.5, y = 1.8, label = "30S", color = "gray43" ) +
geom_vline( xintercept = 7.5, color = "gray43") + annotate("text", x = 8.5, y = 1.8, label = "15S", color = "gray43" ) +
geom_vline( xintercept = 15.5, color = "gray43") + annotate("text", x = 15, y = 1.8, label = "0", color = "gray43" ) +
geom_vline( xintercept = 19.5, color = "gray43") + annotate("text", x = 20.5, y = 1.8, label = "15N", color = "gray43" ) +
geom_vline( xintercept = 32.5, color = "gray43") + annotate("text", x = 33.5, y = 1.8, label = "30N", color = "gray43" ) +
geom_vline( xintercept = 41.5, color = "gray43") + annotate("text", x = 42.5, y = 1.8, label = "45N", color = "gray43" ) +
geom_vline( xintercept = 49.5, color = "gray43") + annotate("text", x = 50.5, y = 1.8, label = "60N", color = "gray43" )
dev.off()
pdf("cold_internalVar.pdf",
height = 9.25, width = 12)
plot_sortCentLat(
m = COLD.df$sigma.med, ub = COLD.df$sigma.UB, sort.var = p.COLD.df$mean.lat,
lb = COLD.df$sigma.LB, regions = p.COLD.df$region2, ylab="",
grp.pal = c( "gray55", "black", brewer.pal(9, "Blues")[c(4,6,8)]),
grp.name = "Does oceanic variability affect consistency\nof negative attribution statements?",
title.txt = NULL,
xlab="", continents = p.COLD.df$continents, shade = p.COLD.df2$classification ) +
theme( legend.margin = unit(-1, "cm")) + ylab(expression(sigma)) +
geom_vline( xintercept = 2.5, color = "gray43") + annotate("text", x = 3.5, y = 1.8, label = "30S", color = "gray43" ) +
geom_vline( xintercept = 7.5, color = "gray43") + annotate("text", x = 8.5, y = 1.8, label = "15S", color = "gray43" ) +
geom_vline( xintercept = 15.5, color = "gray43") + annotate("text", x = 15, y = 1.8, label = "0", color = "gray43" ) +
geom_vline( xintercept = 19.5, color = "gray43") + annotate("text", x = 20.5, y = 1.8, label = "15N", color = "gray43" ) +
geom_vline( xintercept = 32.5, color = "gray43") + annotate("text", x = 33.5, y = 1.8, label = "30N", color = "gray43" ) +
geom_vline( xintercept = 41.5, color = "gray43") + annotate("text", x = 42.5, y = 1.8, label = "45N", color = "gray43" ) +
geom_vline( xintercept = 49.5, color = "gray43") + annotate("text", x = 50.5, y = 1.8, label = "60N", color = "gray43" )
dev.off()
setwd("~/CASCADE_Projects/timerr/paper_draft/final draft/figures/")
plot.df <- data.frame( med = p.COLD.df2$p.hat.exd1, ub = p.COLD.df2$p.ub.exd1,
lb = p.COLD.df2$p.lb.exd1,
regions = as.factor(p.COLD.df2$region2),
color.var = as.factor(p.COLD.df2$continents),
sort.var = p.COLD.df2$mean.lat,
shade.var = p.COLD.df2$classification )
plot.df <- plot.df[ order(plot.df$sort.var), ]
temp.reg <- as.character(plot.df$regions)
plot.df$regions <- ordered(temp.reg, levels = temp.reg)
plot.df$newVar <- plot.df$shade.var
pdf("cold_shaded_exceed_prob2.pdf",
height = 8, width = 16)
ggplot(plot.df, aes(x=regions, y=med)) +
ylab(NULL) + xlab(NULL) + ggtitle(NULL) +
geom_pointrange( aes(x = regions, y = med, ymin = lb, ymax = ub, col = newVar ), size = 1.1) +
scale_color_manual( name = "Does oceanic variability affect consistency\nof positive attribution statements?",
values = c( "gray55", "black", brewer.pal(9, "Blues")[c(4,6,8)]) ) +
geom_hline(aes(yintercept=0.95)) +
geom_hline(aes(yintercept=0.05)) +
scale_y_continuous( limits = c(0.15,1),
breaks = c(0, 0.25, 0.5, 0.75, 1),
labels = c("0% of years <1",
"25% of years <1",
"50% of years <1",
"75% of years <1",
"100% of years <1") ) +
theme( axis.text.x = element_text( angle = 45, hjust = 0.9, size = 10 ),
axis.text.y = element_text( size = 14),
legend.text = element_text(size = 15),
legend.title = element_text(size = 17),
title = element_text(size = 20),
legend.position = "bottom",
legend.box = "horizontal",
legend.direction = "vertical",
strip.text = element_text(size = 16)) +
guides(col = guide_legend(keywidth = 3, keyheight = 1,
override.aes = list(size=1.1))) +
geom_vline( xintercept = 2.5, color = "gray43") + annotate("text", x = 3.5, y = .2, label = "30S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 7.5, color = "gray43") + annotate("text", x = 8.5, y = .2, label = "15S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 15.5, color = "gray43") + annotate("text", x = 16, y = .2, label = "0", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 19.5, color = "gray43") + annotate("text", x = 20.5, y = .2, label = "15N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 32.5, color = "gray43") + annotate("text", x = 33.5, y = .2, label = "30N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 41.5, color = "gray43") + annotate("text", x = 42.5, y = .2, label = "45N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 49.5, color = "gray43") + annotate("text", x = 50.5, y = .2, label = "60N", color = "gray43" )
dev.off()
pdf("cold_shaded_exceed_prob2.pdf",
height = 8.5, width = 16)
ggplot(plot.df, aes(x=regions, y=med)) +
ylab(NULL) + xlab(NULL) + ggtitle(NULL) +
geom_pointrange( aes(x = regions, y = med, ymin = lb, ymax = ub, col = newVar ), size = 1.1) +
scale_color_manual( name = "Does oceanic variability affect consistency\nof positive attribution statements?",
values = c( "gray55", "black", brewer.pal(9, "Blues")[c(4,6,8)]) ) +
geom_hline(aes(yintercept=0.95)) +
geom_hline(aes(yintercept=0.05)) +
scale_y_continuous( limits = c(0.15,1),
breaks = c(0, 0.25, 0.5, 0.75, 1),
labels = c("0% of years <1",
"25% of years <1",
"50% of years <1",
"75% of years <1",
"100% of years <1") ) +
theme( axis.text.x = element_text( angle = 45, hjust = 0.9, size = 10 ),
axis.text.y = element_text( size = 14),
legend.text = element_text(size = 15),
legend.title = element_text(size = 17),
title = element_text(size = 20),
legend.position = "bottom",
legend.box = "horizontal",
legend.direction = "vertical",
strip.text = element_text(size = 16)) +
guides(col = guide_legend(keywidth = 3, keyheight = 1,
override.aes = list(size=1.1))) +
geom_vline( xintercept = 2.5, color = "gray43") + annotate("text", x = 3.5, y = .2, label = "30S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 7.5, color = "gray43") + annotate("text", x = 8.5, y = .2, label = "15S", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 15.5, color = "gray43") + annotate("text", x = 16, y = .2, label = "0", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 19.5, color = "gray43") + annotate("text", x = 20.5, y = .2, label = "15N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 32.5, color = "gray43") + annotate("text", x = 33.5, y = .2, label = "30N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 41.5, color = "gray43") + annotate("text", x = 42.5, y = .2, label = "45N", color = "gray43" ) + # 30S = 2.125, 15S = 7.9, 0 = 15.33, 15N = 19.8, 30N = 32.1, 45N = 41.5, 60N = 49.8, 75N = 58.1
geom_vline( xintercept = 49.5, color = "gray43") + annotate("text", x = 50.5, y = .2, label = "60N", color = "gray43" )
dev.off()
pdf("cold_internalVar.pdf",
height = 9.25, width = 12)
plot_sortCentLat(
m = COLD.df$sigma.med, ub = COLD.df$sigma.UB, sort.var = p.COLD.df$mean.lat,
lb = COLD.df$sigma.LB, regions = p.COLD.df$region2, ylab="",
grp.pal = c( "gray55", "black", brewer.pal(9, "Blues")[c(4,6,8)]),
grp.name = "Does oceanic variability affect consistency\nof negative attribution statements?",
title.txt = NULL,
xlab="", continents = p.COLD.df$continents, shade = p.COLD.df2$classification ) +
theme( legend.margin = unit(-1, "cm")) + ylab(expression(sigma)) +
geom_vline( xintercept = 2.5, color = "gray43") + annotate("text", x = 3.5, y = 1.8, label = "30S", color = "gray43" ) +
geom_vline( xintercept = 7.5, color = "gray43") + annotate("text", x = 8.5, y = 1.8, label = "15S", color = "gray43" ) +
geom_vline( xintercept = 15.5, color = "gray43") + annotate("text", x = 15, y = 1.8, label = "0", color = "gray43" ) +
geom_vline( xintercept = 19.5, color = "gray43") + annotate("text", x = 20.5, y = 1.8, label = "15N", color = "gray43" ) +
geom_vline( xintercept = 32.5, color = "gray43") + annotate("text", x = 33.5, y = 1.8, label = "30N", color = "gray43" ) +
geom_vline( xintercept = 41.5, color = "gray43") + annotate("text", x = 42.5, y = 1.8, label = "45N", color = "gray43" ) +
geom_vline( xintercept = 49.5, color = "gray43") + annotate("text", x = 50.5, y = 1.8, label = "60N", color = "gray43" )
dev.off()
