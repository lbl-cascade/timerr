# Plotting the WRAF regions
setwd("~/CASCADE_Projects/timerr/plotWRAF/shapefiles")

library(maptools)
library(stringr)
library(ggplot2)
library(gpclib)
library(rgeos)
library(sp)
library(plyr)
library(colorspace)
library(RColorBrewer)

wraf.region.list <- readLines("wraf_list_region.txt")
wraf.region.list <- wraf.region.list[-c(1:10)]
reg.list <- list()
reg.nms <- list()
reg.lab <- reg.nam <- reg.code <- NULL
for(i in 1:71){
  
  if(str_sub(wraf.region.list[5*(i-1) + 5], 12, 17) == "Global"){
    reg.lab[i] <- str_sub(wraf.region.list[5*(i-1) + 2], 11, -1) 
    reg.nam[i] <- str_sub(wraf.region.list[5*(i-1) + 3], 10, -1) 
    reg.code[i] <- str_sub(wraf.region.list[5*(i-1) + 4], 10, -1) 
    
    temp <- str_split(reg.code[i], ", ")
    reg.list[[i]] <- data.frame( raw = temp[[1]] )
    grp <- grp.nm <- NULL
    for(j in 1:length(temp[[1]])){
      grp[j] <- str_split(temp[[1]][j], "-")[[1]][1]
      grp.nm[j] <- str_split(temp[[1]][j], "-")[[1]][2]
    }
    reg.list[[i]]$grp <- grp
    reg.list[[i]]$grp.nm <- grp.nm
    
    reg.nms[[i]] <- grp.nm
  }
}

# Import the shape files; small adjustments
country.shp <- readShapeSpatial( "Admin0_v3.1.0_10km/ne_10m_admin_0_countries.shp" )
country.shp$ISO_A3 <- as.character(country.shp$ISO_A3)
country.shp$ISO_A3[78] <- "FRA"
country.shp$ISO_A3[170] <- "NOR"
reg.nms[[26]][2] <- "SSD"

provState.shp <- readShapeSpatial( "Admin1_v3.0.0_10km/ne_10m_admin_1_states_provinces.shp" )
provState.shp$fips <- as.character(provState.shp$fips)
provState.shp$fips[628] <- "Qinghai"


# First 29 involve ISO files only.
map.df <- NULL
for( i in 1:29){
  
  reg.idx <- rep(NA,255)
  reg.idx[ country.shp$ISO_A3 %in% reg.nms[[i]] ] <- reg.lab[i]
  
  temp.shape <- unionSpatialPolygons(country.shp, IDs = reg.idx)
  temp.points <- fortify(temp.shape)
  temp.points$label <- reg.lab[i]

  map.df <- rbind(map.df, temp.points)
  
}

# Next group involves the provinces/states shp file only
for(i in 30:58){

  reg.idx <- rep(NA,nrow(provState.shp@data))
  reg.idx[ provState.shp$code_hasc %in% reg.nms[[i]] & is.na(provState.shp$code_hasc) == FALSE ] <- reg.lab[i]
  reg.idx[ provState.shp$fips %in% reg.nms[[i]] & is.na(provState.shp$fips) == FALSE ] <- reg.lab[i]
  
  temp.shape <- unionSpatialPolygons(provState.shp, IDs = reg.idx)
  temp.points <- fortify(temp.shape)
  temp.points$label <- reg.lab[i]
  
  map.df <- rbind(map.df, temp.points)
  
}

# Adjust the edge of eastern Russia (limits: < -168.9946, > 64.24098)
map.df$long[map.df$long < -168.9946 & map.df$lat > 64.24098 ] <- -map.df$long[map.df$long < -168.9946 & map.df$lat > 64.24098] + 2*(180 + map.df$long[map.df$long < -168.9946 & map.df$lat > 64.24098])

# # Test plot
# pdf("map.pdf", height = 15, width = 20)
# ggplot( aes(x = long, y = lat, group = group), data = map.df ) +
#   geom_polygon( data = map_data("world", interior = FALSE), aes(x = long, y = lat, group = group),  
#                 color = "black", fill = NA) +
#   geom_polygon( aes(fill = label) ) +
#   geom_path(color="black") + 
#   scale_y_continuous(breaks = seq(-80,80,20)) +
#   scale_x_continuous(breaks = seq(-180,180,20)) +
#   coord_equal() 
# dev.off()

# Final map data frame (thin the outlines to reduce the image size)
final.WRAF.df <- map.df[map.df$order %% 2 == 0,]
save(final.WRAF.df, file = "finalWRAFdf.RData")

getPalette <- colorRampPalette(brewer.pal(10, "Spectral"))
set.seed(979)
pal.use <- getPalette(58)[sample(58)]

# set.seed(4)
# pal.use <- rep(brewer.pal(10, "Spectral"),6)[sample(58)]

pdf("finalWRAFmap.pdf", height = 9, width = 20)
ggplot( aes(x = long, y = lat, group = group), data = final.WRAF.df ) +
  geom_polygon( data = map_data("world", interior = FALSE), aes(x = long, y = lat, group = group),  
                color = "black", fill = NA) +
  geom_polygon( aes(fill = label) ) +
  scale_fill_manual( values = pal.use ) +
  geom_path(color="black") +
  coord_equal() + xlab("\nLongitude") + ylab("Latitude\n") +
  scale_y_continuous(breaks = c(-60, -30, 0, 30, 60, 90), limits = c(-60,85)) +
  scale_x_continuous(breaks = c(-180, -120, -60, 0, 60, 120, 180)) +
  theme(panel.grid.major = element_line( colour = "gray85"),
        panel.grid.minor = element_line( colour = "gray85"),
        panel.background = element_blank(),
        legend.position = "none" ) 
dev.off()


# # HASC only
# # for(i in c(38:42,48:49,54)){
# #   
# #   reg.idx <- rep(NA,nrow(provState.shp@data))
# #   reg.idx[ provState.shp$code_hasc %in% reg.nms[[i]] & is.na(provState.shp$code_hasc) == FALSE ] <- reg.lab[i]
# #   
# for(i in 30:58){
#   
#   reg.idx <- rep(NA,nrow(provState.shp@data))
#   reg.idx[ provState.shp$code_hasc %in% reg.nms[[i]] & is.na(provState.shp$code_hasc) == FALSE ] <- reg.lab[i]
#   reg.idx[ provState.shp$fips %in% reg.nms[[i]] & is.na(provState.shp$fips) == FALSE ] <- reg.lab[i]
#   temp.shape <- unionSpatialPolygons(provState.shp, IDs = reg.idx)
#   temp.points <- fortify(temp.shape)
#   temp.points$label <- reg.lab[i]
#   
#   ggplot( aes(x = long, y = lat, group = group), data = temp.points ) +
#     geom_polygon( data = map_data("world", interior = FALSE), aes(x = long, y = lat, group = group),  
#                   color = "black", fill = NA) +
#     geom_polygon( aes(fill = label) ) +
#     geom_path(color="black") + ggtitle(reg.lab[i]) +
#     coord_equal()
#   i <- i + 1
#   
#   map.df <- rbind(map.df, temp.points)
#   
# }
# 
# # All from prov/state data
# for(i in c(50, 51, 52, 53, 57)){
#   
#   reg.idx <- rep(NA,nrow(provState.shp@data))
#   reg.idx[ provState.shp$code_hasc %in% reg.nms[[i]] & is.na(provState.shp$code_hasc) == FALSE ] <- reg.lab[i]
#   reg.idx[ provState.shp$fips %in% reg.nms[[i]] & is.na(provState.shp$fips) == FALSE ] <- reg.lab[i]
#   
#   temp.shape <- unionSpatialPolygons(provState.shp, IDs = reg.idx)
#   temp.points <- fortify(temp.shape)
#   temp.points$label <- reg.lab[i]
#   
#   ggplot( aes(x = long, y = lat, group = group), data = temp.points ) +
#     geom_polygon( data = map_data("world", interior = FALSE), aes(x = long, y = lat, group = group),  
#                   color = "black", fill = NA) +
#     geom_polygon( aes(fill = label) ) +
#     geom_path(color="black") + ggtitle(reg.lab[i]) +
#     coord_equal()
#   
#   map.df <- rbind(map.df, temp.points)
#   
# }
# # OW: 45 
# 
# 
# 
# 
