model
{

	for(i in 1:n.yrs){
	  
	  zA[i] ~ dbinom( pA[i], n.sim[i] )
	  zN[i] ~ dbinom( pA[i]/RR[i], n.sim[i] )
	
	  log(RR[i]) <- RR.randEff[i]

	  logit(pA[i]) <- pA.randEff[i]

	}

	for(j in 1:n.yrs){

		RR.randEff[j] ~ dnorm( mu.RR.randEff, sigmasq.RR.randEff )
		pA.randEff[j] ~ dnorm( mu.pA.randEff, sigmasq.pA.randEff )

	}

	mu.RR.randEff ~ dnorm(0.0, mean.coef.precision)
  mu.pA.randEff ~ dnorm(0.0, mean.coef.precision)

	sigmasq.RR.randEff <- 1/tausq.RR.randEff
	sigmasq.pA.randEff <- 1/tausq.pA.randEff

	tausq.RR.randEff ~ dunif(0, tausq.RR.UB)
	tausq.pA.randEff ~ dunif(0, tausq.pA.UB)

}