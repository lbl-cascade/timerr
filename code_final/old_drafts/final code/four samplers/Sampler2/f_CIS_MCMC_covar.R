#==========================================================
# Models for time-varying risk ratio
# Logistic regression with random effects for pA and pN
# Componentwise Interweaving Strategy
# Mark Risser / LBL / CASCADE postdoctoral fellowship
# November, 2015
#==========================================================

calculate_probs_RR <- function( betaN.save, betaA.save, alpha.save, delta.save, gmt.A, gmt.N,
                                years, n.sims, lowerPerc = 0.025, upperPerc = 0.975 ){
  
  M <- nrow(delta.save)
  J <- ncol(delta.save)
  
  pA.samp <- pN.samp <- matrix(NA,J,M)
  for(m in 1:M){
    pA.samp[,m] <- expit(betaA.save[1,] + betaA.save[2,]*gmt.A[m] + alpha.save[m,] + delta.save[m,])
    pN.samp[,m] <- expit(betaN.save[1,] + betaN.save[2,]*gmt.N[m] + alpha.save[m,])
  }
  RR.samp <- pA.samp/pN.samp
  
  results <- data.frame(
    year = years,
    n = n.sims,
    pA = round(apply(pA.samp,2,median),3),
    pAlb = round(apply(pA.samp,2,function(x){ quantile(x,lowerPerc) }),4),
    pAub = round(apply(pA.samp,2,function(x){ quantile(x,upperPerc) }),4),
    pN = round(apply(pN.samp,2,median),3),
    pNlb = round(apply(pN.samp,2,function(x){ quantile(x,lowerPerc) }),4),
    pNub = round(apply(pN.samp,2,function(x){ quantile(x,upperPerc) }),4),
    RR = round(apply(RR.samp,2,median),3),
    RRlb = round(apply(RR.samp,2,function(x){ quantile(x,lowerPerc) }),4),
    RRub = round(apply(RR.samp,2,function(x){ quantile(x,upperPerc) }),4)
  )
  
  output <- list(
    results = results,
    pA.samp = pA.samp,
    pN.samp = pN.samp,
    RR.samp = RR.samp
  )
  
}

# Plot results
plot_over_time <- function( 
  m, ub, lb, year, n, x.tic = 3, 
  grp.name = "Sample Size", grp.pal = brewer.pal(3, "Dark2"),
  title.txt = "Year-specific 95% credible intervals", ylab = NULL, xlab = "\nYear",
  y.lb = min(lb), y.ub = max(ub), plot.p = TRUE, 
  rr.bounds = c(-3, 5, 0.5, 3), print.plot = FALSE
){
  
  rr.breaks = round( 10^seq(from = rr.bounds[1],
                            to = rr.bounds[2],
                            by = rr.bounds[3] ), rr.bounds[4] )
  
  plot.df <- data.frame( med = m, ub = ub, lb = lb,
                         year = year, color.var = as.factor(n) )
  
  if( plot.p ){
    plt <-  ggplot(plot.df, aes(x=year, y=med)) +
      ylab(ylab) + xlab(xlab) + ggtitle(title.txt) +
      geom_pointrange( aes(x = year, y = med, ymin = lb, ymax = ub, col = color.var )) +
      scale_color_manual( name = grp.name, values = grp.pal ) + ylim( y.lb, y.ub ) +
      scale_x_continuous( breaks = seq(from=plot.df$year[1],to=plot.df$year[nrow(plot.df)],by=x.tic) ) +
      theme( axis.text.x = element_text( angle = 45, hjust = 0.9 ))
  }
  else{
    plt <-  ggplot(plot.df, aes(x=year, y=med)) +
      ylab(ylab) + xlab(xlab) + ggtitle(title.txt) +
      geom_pointrange(aes(x = year, y = med, ymin = lb, ymax = ub, col = color.var)) +
      scale_color_manual( name = grp.name, values = grp.pal ) + 
      scale_x_continuous( breaks = seq(from=plot.df$year[1],to=plot.df$year[nrow(plot.df)],by=x.tic) ) +
      theme( axis.text.x = element_text( angle = 45, hjust = 0.9 )) +
      geom_hline(aes(yintercept=1), color = 'darkgrey') +
      scale_y_log10(limits = c(y.lb, y.ub ), breaks = rr.breaks ) 
  }
  
  if(print.plot){
    print(plt)
  }
  else{
    return(plt)
  }
  
}

# CIS MCMC functions =========================================================

# Inverse logit function
expit <- function(x){ return( exp(x)/(1 + exp(x)) ) }

# Loglikelihood for alpha, step 1
loglik.alpha.delta.t <- function( n.t, z.At, z.Nt, alpha.curr.t,
                                  betaA.curr, betaN.curr, delta.curr.t,
                                  gmt.At, gmt.Nt ){
  
  p.At <- expit( betaA.curr[1] + betaA.curr[2]*gmt.At + alpha.curr.t + delta.curr.t )
  p.Nt <- expit( betaN.curr[1] + betaN.curr[2]*gmt.Nt + alpha.curr.t )
  
  loglik <- sum( dbinom( x = c(z.At, z.Nt), size = c(n.t, n.t),  
                         prob = c(p.At, p.Nt), log = TRUE ) )
  return(loglik)
}

# Loglikelihood for beta, step 2A
loglik.beta <- function( n, z.A, z.N, delta.curr, betaA.curr, betaN.curr, alpha.curr,
                         gmt.A, gmt.N ){
  
  p.A <- expit( betaA.curr[1] + betaA.curr[2]*gmt.A + alpha.curr + delta.curr )
  p.N <- expit( betaN.curr[1] + betaN.curr[2]*gmt.N + alpha.curr )

  loglik <- sum(dbinom( x = c(z.A,z.N), size = c(n,n), prob = c(p.A,p.N), log = TRUE ))
  return(loglik)
}

# Sample betaA, step 2S
sample.betaA.2S <- function( nu.curr, tausq.curr, sigmasq.curr, c2.beta, gmt.A ){
  
  M <- length(nu.curr)
  p <- 2 # number of coefficients
  
  Wmat <- unname(cbind(rep(1,M),gmt.A))
  
  cov <- solve( diag(rep(1/c2.beta,p)) + (1/(tausq.curr+sigmasq.curr))*t(Wmat)%*%Wmat )
  mean <- (1/(tausq.curr+sigmasq.curr))* cov %*% t(Wmat) %*% nu.curr

  betaA.sample <- mvrnorm( n = 1, mu = mean, Sigma = cov )
  return(betaA.sample)

}

# Sample betaN, step 2S
sample.betaN.2S <- function( eta.curr, tausq.curr, c2.beta, gmt.N ){
  
  M <- length(eta.curr)
  p <- 2 # number of coefficients
  
  Xmat <- unname(cbind(rep(1,M),gmt.N))
  
  cov <- solve( diag(rep(1/c2.beta,p)) + (1/tausq.curr)*t(Xmat)%*%Xmat )
  mean <- (1/tausq.curr)* cov %*% t(Xmat) %*% eta.curr
  
  betaN.sample <- mvrnorm( n = 1, mu = mean, Sigma = cov )
  return(betaN.sample)
  
}

# Loglikelihood for tausq and sigmasq, step 3A
loglik.sig.tau <- function( n, z.A, z.N, betaA.curr, betaN.curr, tausq.curr,
                            sigmasq.curr, kappa.curr, xi.curr,
                            gmt.A, gmt.N ){
  
  p.A <- expit( betaA.curr[1] + betaA.curr[2]*gmt.A + tausq.curr*kappa.curr + sigmasq.curr*xi.curr )
  p.N <- expit( betaN.curr[1] + betaN.curr[2]*gmt.N + tausq.curr*kappa.curr )

  loglik <- sum(dbinom( x = c(z.A,z.N), size = c(n,n), prob = c(p.A,p.N), log = TRUE ))
  return(loglik)
}

# Sample tausq, step 3S
sample.tausq.3S <- function( alpha.curr, a.tau, b.tau ){
  
  M <- length(alpha.curr)
  tausq.sample <- 1/rgamma( n = 1, shape = a.tau + 0.5*M, 
                            rate = b.tau + 0.5*sum( alpha.curr^2 ) )
  return(tausq.sample)
  
}

# Sample sigmasq, step 3S
sample.sigmasq.3S <- function( delta.curr, a.sigma, b.sigma ){
  
  M <- length(delta.curr)
  sigmasq.sample <- 1/rgamma( n = 1, shape = a.sigma + 0.5*M, 
                            rate = b.sigma + 0.5*sum( delta.curr^2 ) )
  return(sigmasq.sample)
  
}

