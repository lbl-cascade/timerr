#==========================================================
# Models for time-varying risk ratio
# Logistic regression with random effects for pA and pN
# Componentwise Interweaving Strategy
# Mark Risser / LBL / CASCADE postdoctoral fellowship
# November, 2015
#==========================================================


library(MASS)
library(ggplot2)
library(gridExtra)
library(RColorBrewer)
library(colorspace)

setwd("~/CASCADE_Projects/timerr/final code/four samplers/Sampler1")
source("f_CIS_MCMC_covar.R")

# Load data
load("peru_bolivia.RData")
z.A <- peru_bolivia$z.hot.ALL
z.N <- peru_bolivia$z.hot.NAT
n.sims <- peru_bolivia$N.sims.months
M <- length(z.A)  # Number of years

# GMTs
load("ALLtemps1959_2013_50member.RData")
load("NATtemps1959_2013_50member.RData")
# Standardize covariates
gmt.A <- as.numeric(scale(ALLtemps1959_2013_50member[-1]))
gmt.N <- as.numeric(scale(NATtemps1959_2013_50member[-1]))

# MCMC constants
ITER <- 20000
n.tune <- 6                                     # times to recalculate propSDs
n.iter.tune <- c(rep(2000,n.tune))#,rep(5000,n.tune/2))  # iterations per tune

# Initialize
alpha.init <- rep(0,M)
delta.init <- rep(0,M)
betaA.init <- rep(0,2)
betaN.init <- rep(0,2)
tausq.init <- 1
sigmasq.init <- 1

# Set current to initial
alpha.curr <- alpha.init
delta.curr <- delta.init
betaA.curr <- betaA.init
betaN.curr <- betaN.init
tausq.curr <- tausq.init
sigmasq.curr <- sigmasq.init

# Prior quantities
c2.beta <- 100^2
a.tau <- 2
b.tau <- 1
a.sigma <- 2
b.sigma <- 1

# MH quantities
alpha.propSD <- rep(1,M)
delta.propSD <- rep(1,M)
betaA.propSD <- rep(1,2)
betaN.propSD <- rep(1,2)
tausq.propSD <- 0.5
sigmasq.propSD <- 0.5
rho.prop <- -0.98

# Target accept prob limits
tuneLimLOW <- 0.2
tuneLimLow <- 0.3
tuneLimHigh <- 0.4
tuneLinHIGH <- 0.5

# Scalings
tuneAdjLOW <- 0.5
tuneAdjLow <- 0.8
tuneAdjHigh <- 1.2
tuneAdjHIGH <- 1.5

# Initial loglikelihoods
alpha.loglik <- delta.loglik <- rep(NA,M)

#==========================================================
# Componentwise Interweaving Strategy
#==========================================================
{
#===================
# Tuning
#===================

cat("Tuning the MCMC ========================\n")

for(tune in 1:n.tune){
  cat( paste("Tune cycle ",tune," ... \n"), sep="")
  
  # Storage
  alpha.save <- delta.save <- matrix(NA, M, n.iter.tune[tune])
  betaA.save <- betaN.save <- matrix(NA, 2, n.iter.tune[tune]) 
  tausq.save <- sigmasq.save <- rep(NA, n.iter.tune[tune])
  alpha.accept <- delta.accept <- matrix(NA, M, n.iter.tune[tune])
  betaA.accept <- betaN.accept <- matrix(NA, 2, n.iter.tune[tune])
  tausq.accept <- sigmasq.accept <- rep(NA, n.iter.tune[tune])
  
#   # Set current to initial
#   alpha.curr <- alpha.init
#   delta.curr <- delta.init
#   betaA.curr <- betaA.init
#   betaN.curr <- betaN.init
#   tausq.curr <- tausq.init
#   sigmasq.curr <- sigmasq.init
  
  for(i in 1:n.iter.tune[tune]){
    
    #===================================================
    # Step 1: sample alpha, delta (Metropolis-Hastings)
    #===================================================
    for(t in 1:M){
      alpha.loglik[t] <- loglik.alpha.t( n.t = n.sims[t], z.At = z.A[t], z.Nt = z.N[t], alpha.curr.t = alpha.curr[t],
        betaA.curr = betaA.curr, betaN.curr = betaN.curr, delta.curr.t = delta.curr[t],
        gmt.At = gmt.A[t], gmt.Nt = gmt.N[t])
      delta.loglik[t] <- loglik.delta.t( 
        n.t = n.sims[t], z.At = z.A[t], delta.curr.t = delta.curr[t],
        betaA.curr, alpha.curr.t = alpha.curr[t], gmt.At = gmt.A[t], gmt.Nt = gmt.N[t] )
    }

    for(t in 1:M){
      
      # MH for alpha.t =========
      alpha.t.prop <- alpha.curr[t] + alpha.propSD[t]*rnorm(1)
      loglik.alpha.t.prop <- loglik.alpha.t( n.t = n.sims[t], z.At = z.A[t], 
                                             z.Nt = z.N[t], alpha.curr.t = alpha.t.prop,
                                             betaA.curr = betaA.curr, betaN.curr = betaN.curr, delta.curr.t = delta.curr[t],
                                             gmt.At = gmt.A[t], gmt.Nt = gmt.N[t])
        
      logtarget.prop <- loglik.alpha.t.prop + dnorm(alpha.t.prop, 0, sqrt(tausq.curr), log = TRUE)
      logtarget.curr <- alpha.loglik[t] + dnorm(alpha.curr[t], 0, sqrt(tausq.curr), log = TRUE) 
      
      # MH accept/reject
      if ( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
        alpha.curr[t] <- alpha.t.prop
        alpha.accept[t,i] <- 1
      }
      else{ # reject        
        alpha.accept[t,i] <- 0
      }
      
      # MH for delta.t =========
      delta.t.prop <- delta.curr[t] + delta.propSD[t]*rnorm(1)
      loglik.delta.t.prop <- loglik.delta.t( 
        n.t = n.sims[t], z.At = z.A[t], delta.curr.t = delta.t.prop,
        betaA.curr, alpha.curr.t = alpha.curr[t], gmt.At = gmt.A[t], gmt.Nt = gmt.N[t] )  
      
      logtarget.prop <- loglik.delta.t.prop + dnorm(delta.t.prop, 0, sqrt(sigmasq.curr), log = TRUE)
      logtarget.curr <- delta.loglik[t] + dnorm(delta.curr[t], 0, sqrt(sigmasq.curr), log = TRUE) 
      
      # MH accept/reject
      if ( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
        delta.curr[t] <- delta.t.prop
        delta.accept[t,i] <- 1
      }
      else{ # reject        
        delta.accept[t,i] <- 0
      }
    }
    
    #===================================================
    # Step 2A: sample beta (Metropolis-Hastings)
    #===================================================
    
    for(j in 1:2){
      # betaA ======
      betaA.prop <- betaA.curr
      betaA.prop[j] <- betaA.curr[j] + betaA.propSD[j]*rnorm(1)
      
      beta.loglik <- loglik.beta( n = n.sims, z.A = z.A, z.N = z.N, 
                                  delta.curr = delta.curr, betaA.curr = betaA.curr, 
                                  betaN.curr = betaN.curr, alpha.curr = alpha.curr,
                                  gmt.A = gmt.A, gmt.N = gmt.N )
 
      loglik.beta.prop <-  loglik.beta( n = n.sims, z.A = z.A, z.N = z.N, 
                                        delta.curr = delta.curr, betaA.curr = betaA.prop, 
                                        betaN.curr = betaN.curr, alpha.curr = alpha.curr,
                                        gmt.A = gmt.A, gmt.N = gmt.N )
        
      logtarget.prop <- loglik.beta.prop + dnorm(betaA.prop[j], 0, sqrt(c2.beta), log = TRUE)
      logtarget.curr <- beta.loglik + dnorm(betaA.curr[j], 0, sqrt(c2.beta), log = TRUE) 
      
      # MH accept/reject
      if( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
        betaA.curr <- betaA.prop
        betaA.accept[j,i] <- 1
      }
      else{ # reject        
        betaA.accept[j,i] <- 0
      }
      
      # betaN ======
      betaN.prop <- betaN.curr
      betaN.prop[j] <- betaN.curr[j] + betaN.propSD[j]*rnorm(1)

      beta.loglik <- loglik.beta( n = n.sims, z.A = z.A, z.N = z.N, 
                                  delta.curr = delta.curr, betaA.curr = betaA.curr, 
                                  betaN.curr = betaN.curr, alpha.curr = alpha.curr,
                                  gmt.A = gmt.A, gmt.N = gmt.N )
      
      loglik.beta.prop <-  loglik.beta( n = n.sims, z.A = z.A, z.N = z.N, 
                                        delta.curr = delta.curr, betaA.curr = betaA.curr, 
                                        betaN.curr = betaN.prop, alpha.curr = alpha.curr,
                                        gmt.A = gmt.A, gmt.N = gmt.N )
      
      logtarget.prop <- loglik.beta.prop + dnorm(betaN.prop[j], 0, sqrt(c2.beta), log = TRUE)
      logtarget.curr <- beta.loglik + dnorm(betaN.curr[j], 0, sqrt(c2.beta), log = TRUE) 
      
      # MH accept/reject
      if ( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
        betaN.curr <- betaN.prop
        betaN.accept[j,i] <- 1
      }
      else{ # reject        
        betaN.accept[j,i] <- 0
      } 
    }
    
#     # Calculate eta and nu
#     eta.curr <- cbind(rep(1,M),gmt.N) %*% betaN.curr + alpha.curr
#     nu.curr <- cbind(rep(1,M),gmt.A) %*% betaA.curr + alpha.curr + delta.curr
#     
#     #===================================================
#     # Step 2S: sample beta (Gibbs)
#     #===================================================
#     betaA.curr <- sample.betaA.2S( nu.curr, tausq.curr, sigmasq.curr, c2.beta, gmt.A )
#     betaN.curr <- sample.betaN.2S( eta.curr, tausq.curr, c2.beta, gmt.N )
#     
#     # Update alpha and delta
#     alpha.curr <- as.numeric(eta.curr - cbind(rep(1,M),gmt.N) %*% betaN.curr)
#     delta.curr <- as.numeric(nu.curr - cbind(rep(1,M),gmt.A) %*% betaA.curr) - alpha.curr
# 
#     # Calculate kappa and xi
#     kappa.curr <- alpha.curr/tausq.curr
#     xi.curr <- delta.curr/sigmasq.curr
#     
#     #===================================================
#     # Step 3A: sample tausq, sigmasq (Metropolis-Hastings)
#     #===================================================
#     
#     # tausq ======
#     tausq.prop <- tausq.curr + tausq.propSD*rnorm(1)
#     
#     if( tausq.prop > 0 ){ # Do the MH step
#       tausq.loglik <- loglik.sig.tau( n = n.sims, z.A, z.N, betaA.curr, 
#                                       betaN.curr, tausq.curr, sigmasq.curr, kappa.curr, xi.curr,
#                                       gmt.A, gmt.N )
#       tausq.loglik.prop <- loglik.sig.tau( n = n.sims, z.A, z.N, betaA.curr, 
#                                            betaN.curr, tausq.prop, sigmasq.curr, kappa.curr, xi.curr,
#                                            gmt.A, gmt.N )
#       
#       logtarget.prop <- tausq.loglik.prop - (a.tau + 1)*log(tausq.prop) - b.tau/tausq.prop
#       logtarget.curr <- tausq.loglik - (a.tau + 1)*log(tausq.curr) - b.tau/tausq.curr
#       
#       # MH accept/reject
#       if( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
#         tausq.curr <- tausq.prop
#         tausq.accept[i] <- 1
#       }
#       else{ # reject        
#         tausq.accept[i] <- 0
#       } 
#     }
#     else{ # Automatically reject
#       tausq.accept[i] <- 0
#     }
#     
#     # sigmasq ======
#     sigmasq.prop <- sigmasq.curr + sigmasq.propSD*rnorm(1)
#     
#     if( sigmasq.prop > 0 ){ # Do the MH step
#       sigmasq.loglik <- loglik.sig.tau( n = n.sims, z.A, z.N, betaA.curr, 
#                                         betaN.curr, tausq.curr, sigmasq.curr, kappa.curr, xi.curr,
#                                         gmt.A, gmt.N )
#       sigmasq.loglik.prop <- loglik.sig.tau( n = n.sims, z.A, z.N, betaA.curr, 
#                                              betaN.curr, tausq.curr, sigmasq.prop, kappa.curr, xi.curr,
#                                              gmt.A, gmt.N )
#       
#       logtarget.prop <- sigmasq.loglik.prop - (a.sigma + 1)*log(sigmasq.prop) - b.sigma/sigmasq.prop
#       logtarget.curr <- sigmasq.loglik - (a.sigma + 1)*log(sigmasq.curr) - b.sigma/sigmasq.curr
#       
#       # MH accept/reject
#       if( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
#         sigmasq.curr <- sigmasq.prop
#         sigmasq.accept[i] <- 1
#       }
#       else{ # reject        
#         sigmasq.accept[i] <- 0
#       } 
#     }
#     else{ # Automatically reject
#       sigmasq.accept[i] <- 0
#     }
#     
#     # Update alpha and delta
#     alpha.curr <- tausq.curr*kappa.curr
#     delta.curr <- sigmasq.curr*xi.curr
    
    #===================================================
    # Step 3S: sample tausq, sigmasq (Gibbs)
    #===================================================
    tausq.curr <- sample.tausq.3S( alpha.curr, a.tau, b.tau )
    sigmasq.curr <- sample.sigmasq.3S( delta.curr, a.sigma, b.sigma )
    
    # SAVE
    alpha.save[,i] <- alpha.curr
    delta.save[,i] <- delta.curr
    betaA.save[,i] <- betaA.curr
    betaN.save[,i] <- betaN.curr
    tausq.save[i] <- tausq.curr
    sigmasq.save[i] <- sigmasq.curr
    
    if(i %% (n.iter.tune[tune]/10) == 0){ cat(i, " ") }
  }
  
  # Progress
  cat("\nalpha accept probs = \n")
  print(round(rowMeans(alpha.accept, na.rm = T),3))
  cat("\ndelta accept probs = \n")
  print(round(rowMeans(delta.accept, na.rm = T),3))
  cat("\nbetaA accept prob = ")
  cat(round(rowMeans(betaA.accept, na.rm = T),3))
  cat("\nbetaN accept prob = ")
  cat(round(rowMeans(betaN.accept, na.rm = T),3))
  cat("\ntausq accept prob = ")
  cat(round(mean(tausq.accept, na.rm = T),3))
  cat("\nsigmasq accept prob = ")
  cat(round(mean(sigmasq.accept, na.rm = T),3))
  cat("\n")
  
  # Tune
  for(t in 1:M){
    # ALPHA
    if( mean(alpha.accept[t,]) < tuneLimLOW ){ 
      # Scale propSD down big
      alpha.propSD[t] <- tuneAdjLOW*alpha.propSD[t]
    }
    if( mean(mean(alpha.accept[t,])) >= tuneLimLOW & mean(mean(alpha.accept[t,])) < tuneLimLow ){ 
      # Scale propSD down little
      alpha.propSD[t] <- tuneAdjLow*alpha.propSD[t]
    }
    if( mean(mean(alpha.accept[t,])) > tuneLinHIGH ){ 
      # Scale propSD up big
      alpha.propSD[t] <- tuneAdjHIGH*alpha.propSD[t]
    }
    if( mean(mean(alpha.accept[t,])) > tuneLimHigh & mean(mean(alpha.accept[t,])) <= tuneLinHIGH ){ 
      # Scale propSD up little
      alpha.propSD[t] <- tuneAdjHigh*alpha.propSD[t]
    }
    
    # DELTA
    if( mean(delta.accept[t,]) < tuneLimLOW ){ 
      # Scale propSD down big
      delta.propSD[t] <- tuneAdjLOW*delta.propSD[t]
    }
    if( mean(mean(delta.accept[t,])) >= tuneLimLOW & mean(mean(delta.accept[t,])) < tuneLimLow ){ 
      # Scale propSD down little
      delta.propSD[t] <- tuneAdjLow*delta.propSD[t]
    }
    if( mean(mean(delta.accept[t,])) > tuneLinHIGH ){ 
      # Scale propSD up big
      delta.propSD[t] <- tuneAdjHIGH*delta.propSD[t]
    }
    if( mean(mean(delta.accept[t,])) > tuneLimHigh & mean(mean(delta.accept[t,])) <= tuneLinHIGH ){ 
      # Scale propSD up little
      delta.propSD[t] <- tuneAdjHigh*delta.propSD[t]
    }
  }
  
  for(j in 1:2){
    # betaA
    if( mean(betaA.accept[j,]) < tuneLimLOW ){ 
      # Scale propSD down big
      betaA.propSD[j] <- tuneAdjLOW*betaA.propSD[j]
    }
    if( mean(betaA.accept[j,]) >= tuneLimLOW & mean(betaA.accept[j,]) < tuneLimLow ){ 
      # Scale propSD down little
      betaA.propSD[j] <- tuneAdjLow*betaA.propSD[j]
    }
    if( mean(betaA.accept[j,]) > tuneLinHIGH ){ 
      # Scale propSD up big
      betaA.propSD[j] <- tuneAdjHIGH*betaA.propSD[j]
    }
    if( mean(betaA.accept[j,]) > tuneLimHigh & mean(betaA.accept[j,]) <= tuneLinHIGH ){ 
      # Scale propSD up little
      betaA.propSD[j] <- tuneAdjHigh*betaA.propSD[j]
    }
    
    # betaN
    if( mean(betaN.accept[j,]) < tuneLimLOW ){ 
      # Scale propSD down big
      betaN.propSD[j] <- tuneAdjLOW*betaN.propSD[j]
    }
    if( mean(betaN.accept[j,]) >= tuneLimLOW & mean(betaN.accept[j,]) < tuneLimLow ){ 
      # Scale propSD down little
      betaN.propSD[j] <- tuneAdjLow*betaN.propSD[j]
    }
    if( mean(betaN.accept[j,]) > tuneLinHIGH ){ 
      # Scale propSD up big
      betaN.propSD[j] <- tuneAdjHIGH*betaN.propSD[j]
    }
    if( mean(betaN.accept[j,]) > tuneLimHigh & mean(betaN.accept[j,]) <= tuneLinHIGH ){ 
      # Scale propSD up little
      betaN.propSD[j] <- tuneAdjHigh*betaN.propSD[j]
    }
  }
  
#   # TAUSQ
#   if( mean(tausq.accept) < tuneLimLOW ){ 
#     # Scale propSD down big
#     tausq.propSD <- tuneAdjLOW*tausq.propSD
#   }
#   if( mean(tausq.accept) >= tuneLimLOW & mean(tausq.accept) < tuneLimLow ){ 
#     # Scale propSD down little
#     tausq.propSD <- tuneAdjLow*tausq.propSD
#   }
#   if( mean(tausq.accept) > tuneLinHIGH ){ 
#     # Scale propSD up big
#     tausq.propSD <- tuneAdjHIGH*tausq.propSD
#   }
#   if( mean(tausq.accept) > tuneLimHigh & mean(tausq.accept) <= tuneLinHIGH ){ 
#     # Scale propSD up little
#     tausq.propSD <- tuneAdjHigh*tausq.propSD
#   }
#   
#   # SIGMASQ
#   if( mean(sigmasq.accept) < tuneLimLOW ){ 
#     # Scale propSD down big
#     sigmasq.propSD <- tuneAdjLOW*sigmasq.propSD
#   }
#   if( mean(sigmasq.accept) >= tuneLimLOW & mean(sigmasq.accept) < tuneLimLow ){ 
#     # Scale propSD down little
#     sigmasq.propSD <- tuneAdjLow*sigmasq.propSD
#   }
#   if( mean(sigmasq.accept) > tuneLinHIGH ){ 
#     # Scale propSD up big
#     sigmasq.propSD <- tuneAdjHIGH*sigmasq.propSD
#   }
#   if( mean(sigmasq.accept) > tuneLimHigh & mean(sigmasq.accept) <= tuneLinHIGH ){ 
#     # Scale propSD up little
#     sigmasq.propSD <- tuneAdjHigh*sigmasq.propSD
#   }
  
} # End tuning

#===================
# Final MCMC
#===================

cat("Final MCMC ========================\n")

# Storage
alpha.save <- delta.save <- matrix(NA, M, ITER)
betaA.save <- betaN.save <- matrix(NA, 2, ITER)
tausq.save <- sigmasq.save <- rep(NA, ITER)
alpha.accept <- delta.accept <- matrix(NA, M, ITER)
betaA.accept <- betaN.accept <- matrix(NA, 2, ITER) 
tausq.accept <- sigmasq.accept <- rep(NA, ITER)

# Set current to initial
alpha.curr <- alpha.init
delta.curr <- delta.init
betaA.curr <- betaA.init
betaN.curr <- betaN.init
tausq.curr <- tausq.init
sigmasq.curr <- sigmasq.init

prt <- proc.time()
for(i in 1:ITER){
  
  #===================================================
  # Step 1: sample alpha, delta (Metropolis-Hastings)
  #===================================================
  for(t in 1:M){
    alpha.loglik[t] <- loglik.alpha.t( n.t = n.sims[t], z.At = z.A[t], z.Nt = z.N[t], alpha.curr.t = alpha.curr[t],
                                       betaA.curr = betaA.curr, betaN.curr = betaN.curr, delta.curr.t = delta.curr[t],
                                       gmt.At = gmt.A[t], gmt.Nt = gmt.N[t])
    delta.loglik[t] <- loglik.delta.t( 
      n.t = n.sims[t], z.At = z.A[t], delta.curr.t = delta.curr[t],
      betaA.curr, alpha.curr.t = alpha.curr[t], gmt.At = gmt.A[t], gmt.Nt = gmt.N[t] )
  }
  
  for(t in 1:M){
    
    # MH for alpha.t =========
    alpha.t.prop <- alpha.curr[t] + alpha.propSD[t]*rnorm(1)
    loglik.alpha.t.prop <- loglik.alpha.t( n.t = n.sims[t], z.At = z.A[t], 
                                           z.Nt = z.N[t], alpha.curr.t = alpha.t.prop,
                                           betaA.curr = betaA.curr, betaN.curr = betaN.curr, delta.curr.t = delta.curr[t],
                                           gmt.At = gmt.A[t], gmt.Nt = gmt.N[t])
    
    logtarget.prop <- loglik.alpha.t.prop + dnorm(alpha.t.prop, 0, sqrt(tausq.curr), log = TRUE)
    logtarget.curr <- alpha.loglik[t] + dnorm(alpha.curr[t], 0, sqrt(tausq.curr), log = TRUE) 
    
    # MH accept/reject
    if ( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
      alpha.curr[t] <- alpha.t.prop
      alpha.accept[t,i] <- 1
    }
    else{ # reject        
      alpha.accept[t,i] <- 0
    }
    
    # MH for delta.t =========
    delta.t.prop <- delta.curr[t] + delta.propSD[t]*rnorm(1)
    loglik.delta.t.prop <- loglik.delta.t( 
      n.t = n.sims[t], z.At = z.A[t], delta.curr.t = delta.t.prop,
      betaA.curr, alpha.curr.t = alpha.curr[t], gmt.At = gmt.A[t], gmt.Nt = gmt.N[t] )  
    
    logtarget.prop <- loglik.delta.t.prop + dnorm(delta.t.prop, 0, sqrt(sigmasq.curr), log = TRUE)
    logtarget.curr <- delta.loglik[t] + dnorm(delta.curr[t], 0, sqrt(sigmasq.curr), log = TRUE) 
    
    # MH accept/reject
    if ( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
      delta.curr[t] <- delta.t.prop
      delta.accept[t,i] <- 1
    }
    else{ # reject        
      delta.accept[t,i] <- 0
    }
  }
  
  #===================================================
  # Step 2A: sample beta (Metropolis-Hastings)
  #===================================================
  
  for(j in 1:2){
    # betaA ======
    betaA.prop <- betaA.curr
    betaA.prop[j] <- betaA.curr[j] + betaA.propSD[j]*rnorm(1)
    
    beta.loglik <- loglik.beta( n = n.sims, z.A = z.A, z.N = z.N, 
                                delta.curr = delta.curr, betaA.curr = betaA.curr, 
                                betaN.curr = betaN.curr, alpha.curr = alpha.curr,
                                gmt.A = gmt.A, gmt.N = gmt.N )
    
    loglik.beta.prop <-  loglik.beta( n = n.sims, z.A = z.A, z.N = z.N, 
                                      delta.curr = delta.curr, betaA.curr = betaA.prop, 
                                      betaN.curr = betaN.curr, alpha.curr = alpha.curr,
                                      gmt.A = gmt.A, gmt.N = gmt.N )
    
    logtarget.prop <- loglik.beta.prop + dnorm(betaA.prop[j], 0, sqrt(c2.beta), log = TRUE)
    logtarget.curr <- beta.loglik + dnorm(betaA.curr[j], 0, sqrt(c2.beta), log = TRUE) 
    
    # MH accept/reject
    if( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
      betaA.curr <- betaA.prop
      betaA.accept[j,i] <- 1
    }
    else{ # reject        
      betaA.accept[j,i] <- 0
    }
    
    # betaN ======
    betaN.prop <- betaN.curr
    betaN.prop[j] <- betaN.curr[j] + betaN.propSD[j]*rnorm(1)
    
    beta.loglik <- loglik.beta( n = n.sims, z.A = z.A, z.N = z.N, 
                                delta.curr = delta.curr, betaA.curr = betaA.curr, 
                                betaN.curr = betaN.curr, alpha.curr = alpha.curr,
                                gmt.A = gmt.A, gmt.N = gmt.N )
    
    loglik.beta.prop <-  loglik.beta( n = n.sims, z.A = z.A, z.N = z.N, 
                                      delta.curr = delta.curr, betaA.curr = betaA.curr, 
                                      betaN.curr = betaN.prop, alpha.curr = alpha.curr,
                                      gmt.A = gmt.A, gmt.N = gmt.N )
    
    logtarget.prop <- loglik.beta.prop + dnorm(betaN.prop[j], 0, sqrt(c2.beta), log = TRUE)
    logtarget.curr <- beta.loglik + dnorm(betaN.curr[j], 0, sqrt(c2.beta), log = TRUE) 
    
    # MH accept/reject
    if ( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
      betaN.curr <- betaN.prop
      betaN.accept[j,i] <- 1
    }
    else{ # reject        
      betaN.accept[j,i] <- 0
    } 
  }
  
#   # Calculate eta and nu
#   eta.curr <- cbind(rep(1,M),gmt.N) %*% betaN.curr + alpha.curr
#   nu.curr <- cbind(rep(1,M),gmt.A) %*% betaA.curr + alpha.curr + delta.curr
#   
#   #===================================================
#   # Step 2S: sample beta (Gibbs)
#   #===================================================
#   betaA.curr <- sample.betaA.2S( nu.curr, tausq.curr, sigmasq.curr, c2.beta, gmt.A )
#   betaN.curr <- sample.betaN.2S( eta.curr, tausq.curr, c2.beta, gmt.N )
#   
#   # Update alpha and delta
#   alpha.curr <- as.numeric(eta.curr - cbind(rep(1,M),gmt.N) %*% betaN.curr)
#   delta.curr <- as.numeric(nu.curr - cbind(rep(1,M),gmt.A) %*% betaA.curr) - alpha.curr
#   
#   # Calculate kappa and xi
#   kappa.curr <- alpha.curr/tausq.curr
#   xi.curr <- delta.curr/sigmasq.curr
#   
#   #===================================================
#   # Step 3A: sample tausq, sigmasq (Metropolis-Hastings)
#   #===================================================
#   
#   # tausq ======
#   tausq.prop <- tausq.curr + tausq.propSD*rnorm(1)
#   
#   if( tausq.prop > 0 ){ # Do the MH step
#     tausq.loglik <- loglik.sig.tau( n = n.sims, z.A, z.N, betaA.curr, 
#                                     betaN.curr, tausq.curr, sigmasq.curr, kappa.curr, xi.curr,
#                                     gmt.A, gmt.N )
#     tausq.loglik.prop <- loglik.sig.tau( n = n.sims, z.A, z.N, betaA.curr, 
#                                          betaN.curr, tausq.prop, sigmasq.curr, kappa.curr, xi.curr,
#                                          gmt.A, gmt.N )
#     
#     logtarget.prop <- tausq.loglik.prop - (a.tau + 1)*log(tausq.prop) - b.tau/tausq.prop
#     logtarget.curr <- tausq.loglik - (a.tau + 1)*log(tausq.curr) - b.tau/tausq.curr
#     
#     # MH accept/reject
#     if( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
#       tausq.curr <- tausq.prop
#       tausq.accept[i] <- 1
#     }
#     else{ # reject        
#       tausq.accept[i] <- 0
#     } 
#   }
#   else{ # Automatically reject
#     tausq.accept[i] <- 0
#   }
#   
#   # sigmasq ======
#   sigmasq.prop <- sigmasq.curr + sigmasq.propSD*rnorm(1)
#   
#   if( sigmasq.prop > 0 ){ # Do the MH step
#     sigmasq.loglik <- loglik.sig.tau( n = n.sims, z.A, z.N, betaA.curr, 
#                                       betaN.curr, tausq.curr, sigmasq.curr, kappa.curr, xi.curr,
#                                       gmt.A, gmt.N )
#     sigmasq.loglik.prop <- loglik.sig.tau( n = n.sims, z.A, z.N, betaA.curr, 
#                                            betaN.curr, tausq.curr, sigmasq.prop, kappa.curr, xi.curr,
#                                            gmt.A, gmt.N )
#     
#     logtarget.prop <- sigmasq.loglik.prop - (a.sigma + 1)*log(sigmasq.prop) - b.sigma/sigmasq.prop
#     logtarget.curr <- sigmasq.loglik - (a.sigma + 1)*log(sigmasq.curr) - b.sigma/sigmasq.curr
#     
#     # MH accept/reject
#     if( log(runif(1)) < (logtarget.prop - logtarget.curr) ){ # accept      
#       sigmasq.curr <- sigmasq.prop
#       sigmasq.accept[i] <- 1
#     }
#     else{ # reject        
#       sigmasq.accept[i] <- 0
#     } 
#   }
#   else{ # Automatically reject
#     sigmasq.accept[i] <- 0
#   }
#   
#   # Update alpha and delta
#   alpha.curr <- tausq.curr*kappa.curr
#   delta.curr <- sigmasq.curr*xi.curr
  
  #===================================================
  # Step 3S: sample tausq, sigmasq (Gibbs)
  #===================================================
  tausq.curr <- sample.tausq.3S( alpha.curr, a.tau, b.tau )
  sigmasq.curr <- sample.sigmasq.3S( delta.curr, a.sigma, b.sigma )
  
  # SAVE
  alpha.save[,i] <- alpha.curr
  delta.save[,i] <- delta.curr
  betaA.save[,i] <- betaA.curr
  betaN.save[,i] <- betaN.curr
  tausq.save[i] <- tausq.curr
  sigmasq.save[i] <- sigmasq.curr
  
  if(i %% (ITER/10) == 0){ cat(i, " ") }
}
tot.time <- proc.time() - prt
save(tot.time, file = "tottime.RData")

# Progress
cat("\nalpha/delta accept probs = \n")
print(round(rowMeans(alpha.accept, na.rm = T),3))
cat("\nbetaA accept prob = ")
cat(round(rowMeans(betaA.accept, na.rm = T),3))
cat("\nbetaN accept prob = ")
cat(round(rowMeans(betaN.accept, na.rm = T),3))
cat("\ntausq accept prob = ")
cat(round(mean(tausq.accept, na.rm = T),3))
cat("\nsigmasq accept prob = ")
cat(round(mean(sigmasq.accept, na.rm = T),3))
cat("\n")

}


# sampler1.output <- list(
#   betaA.save = betaA.save,
#   betaN.save = betaN.save,
#   tausq.save = tausq.save,
#   sigmasq.save = sigmasq.save,
#   alpha.save = alpha.save,
#   delta.save = delta.save
# )
# 
# save(sampler1.output, file="sampler1output.RData")
# 
# # Thinned results
# samples.keep <- seq( from = (ITER/2 + 1), to = ITER, by = 1)
# 
# par(ask=FALSE, mfrow=c(2,3))
# plot(betaA.save[1,samples.keep], type="l", xlab="Iteration", ylab="", main="muA")
# plot(betaN.save[1,samples.keep], type="l", xlab="Iteration", ylab="", main="muN")
# plot(tausq.save[samples.keep], type="l", xlab="Iteration", ylab="", main="tausq")
# plot(betaA.save[2,samples.keep], type="l", xlab="Iteration", ylab="", main="ALL temp coefficient")
# plot(betaN.save[2,samples.keep], type="l", xlab="Iteration", ylab="", main="NAT temp coefficient")
# plot(sigmasq.save[samples.keep], type="l", xlab="Iteration", ylab="", main="sigmasq")
# 
# par(ask=TRUE, mfrow=c(1,2))
# for(t in 1:M){
#   plot(alpha.save[t,samples.keep], type="l", xlab="Iteration", ylab="", 
#        main=paste("alpha_",t,sep=""))
#   plot(delta.save[t,samples.keep], type="l", xlab="Iteration", ylab="", 
#        main=paste("delta_",t,sep=""))
# }
