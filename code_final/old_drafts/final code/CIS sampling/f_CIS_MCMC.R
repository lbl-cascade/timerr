#==========================================================
# Models for time-varying risk ratio
# Logistic regression with random effects for pA and pN
# Componentwise Interweaving Strategy
# Mark Risser / LBL / CASCADE postdoctoral fellowship
# November, 2015
#==========================================================

# CIS MCMC functions

# Inverse logit function
expit <- function(x){ return( exp(x)/(1 + exp(x)) ) }

# Loglikelihood for alpha, step 1
loglik.alpha.t <- function( n.t, z.At, z.Nt, alpha.curr.t,
                            beta0.curr, beta1.curr, delta.curr.t ){
  
  p.At <- expit( beta0.curr + beta1.curr + alpha.curr.t + delta.curr.t )
  p.Nt <- expit( beta0.curr + alpha.curr.t )
  
  loglik <- sum( dbinom( x = c(z.At, z.Nt), size = c(n.t, n.t),  
                         prob = c(p.At, p.Nt), log = TRUE ) )
  return(loglik)
}

# Loglikelihood for delta, step 1
loglik.delta.t <- function( n.t, z.At, delta.curr.t,
                            beta0.curr, beta1.curr, alpha.curr.t ){
  
  p.At <- expit( beta0.curr + beta1.curr + alpha.curr.t + delta.curr.t )

  loglik <- dbinom( x = z.At, size = n.t, prob = p.At, log = TRUE )
  return(loglik)
}

# Loglikelihood for beta, step 2A
loglik.beta <- function( n, z.A, z.N, delta.curr, beta0.curr, beta1.curr, alpha.curr ){
  
  p.A <- expit( beta0.curr + beta1.curr + alpha.curr + delta.curr )
  p.N <- expit( beta0.curr + alpha.curr )
  
  loglik <- sum(dbinom( x = c(z.A,z.N), size = c(n,n), prob = c(p.A,p.N), log = TRUE ))
  return(loglik)
}

# Sample beta0, step 2S
sample.beta0.2S <- function( eta.curr, tausq.curr, c2.beta ){
  
  M <- length(eta.curr)
  
  variance <- (c2.beta*tausq.curr)/(tausq.curr + M*c2.beta)
  mean <- variance*sum(eta.curr)/tausq.curr
  
  beta0.sample <- mean + sqrt(variance)*rnorm(1)
  return(beta0.sample)

}

# Sample beta1, step 2S
sample.beta1.2S <- function( nu.curr, sigmasq.curr, c2.beta ){
  
  M <- length(nu.curr)
  
  variance <- (c2.beta*sigmasq.curr)/(sigmasq.curr + M*c2.beta)
  mean <- variance*sum(nu.curr)/sigmasq.curr
  
  beta0.sample <- mean + sqrt(variance)*rnorm(1)
  return(beta0.sample)
  
}

# Loglikelihood for tausq and sigmasq, step 3A
loglik.sig.tau <- function( n, z.A, z.N, beta0.curr, beta1.curr, tausq.curr,
                            sigmasq.curr, kappa.curr, xi.curr ){
  
  p.A <- expit( beta0.curr + beta1.curr + tausq.curr*kappa.curr + sigmasq.curr*xi.curr )
  p.N <- expit( beta0.curr + tausq.curr*kappa.curr )
  
  loglik <- sum(dbinom( x = c(z.A,z.N), size = c(n,n), prob = c(p.A,p.N), log = TRUE ))
  return(loglik)
}

# Sample tausq, step 3S
sample.tausq.3S <- function( alpha.curr, a.tau, b.tau ){
  
  M <- length(alpha.curr)
  tausq.sample <- 1/rgamma( n = 1, shape = a.tau + 0.5*M, 
                            rate = b.tau + 0.5*sum( alpha.curr^2 ) )
  return(tausq.sample)
  
}

# Sample sigmasq, step 3S
sample.sigmasq.3S <- function( delta.curr, a.sigma, b.sigma ){
  
  M <- length(delta.curr)
  sigmasq.sample <- 1/rgamma( n = 1, shape = a.sigma + 0.5*M, 
                            rate = b.sigma + 0.5*sum( delta.curr^2 ) )
  return(sigmasq.sample)
  
}

