\documentclass[times,12pt]{article}

%\usepackage{macros}
\usepackage{mdwlist}
\usepackage[utf8]{inputenc}

% load packages
\usepackage{times,natbib}
\usepackage{balance}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{multirow}
\usepackage[margin=1in]{geometry}
\usepackage{subfigure}
\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{breakurl}
\usepackage{natbib,upgreek}
\usepackage{bbm}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{doi}
\DeclareMathOperator{\logit}{logit}

\newcommand{\bfbeta}{{\boldsymbol \beta}}
\newcommand{\bftheta}{{\boldsymbol \theta}}

\newcommand{\bfs}{{\bf s}}
\newcommand{\bfx}{{\bf x}}
\def\bSig\mathbf{\Sigma}
\DeclareMathOperator{\CH}{CH_4}


%\newcommand{\todo}[1]{\textbf{[\color{green} #1]}}
%\newcommand{\pfc}[1]{\textbf{[\color{blue}Peter: #1]}}
%\newcommand{\rh}[1]{\textbf{[\color{red}Radu: #1]}}
%\newcommand{\vv}[1]{\mbox{\boldmath $#1$}}


\begin{document}
\doublespacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{A metric for assessing the impact of internal variability on the qualitative results of a D\&A}

Once the effect of ocean internal variability on the risk ratio has been quantified, a natural question to ask is: ``Do the qualitative results of a D\&A analysis change based on the effect of oceanic internal variability?'' Put another way,
\begin{itemize}
\item[] ``For the years considered in our study (1982-2013), for what proportion of years would the qualitative inference for a D\&A analysis have changed based on the effect of ocean internal variability, supposing each year had a global mean temperature equal to the 2009-2013 average?''
\end{itemize}
In this framework, a ``change'' in a D\&A analysis is relative to the event type and how extreme the evidence must be to establish anthropogenic influence. Furthermore, fixing each year to have the same global mean temperature value in a sense removes the dependence of the risk ratio on global temperature and also makes each of the years comparable.
Formally, the question above involves estimating 
\[
p = \text{the proportion of years for which the qualitative result of a D\&A study \textbf{does not change}},
\]
i.e., the proportion of years for which the risk ratio significantly exceeds (greater than or less than) a particular cutoff. Intuitively, large $p$ indicates that the interval estimate of the risk ratio exceeds (or does not exceed) a threshold for most years; small $p$ indicates that the interval estimate for the risk ratio exceeds (or does not exceed) the threshold for only a few years.

Thus, our criterion might involve something like
\begin{itemize}
\item[] ``If the interval estimate of $p$ (for a region/event type, and relative to a specified cutoff) is entirely included in the interval $(0.95, 1]$ or $[0, 0.05)$, then the risk ratios are homogeneous (relative to the threshold) and the qualitative results of a D\&A analysis are insensitive to the specific year chosen for analysis. On the other hand, if any part of the interval estimate for $p$ falls inside $[0.05, 0.95]$, then there is evidence of heterogeneity in both the risk ratio (relative to the threshold) \textbf{and} the corresponding qualitative statement about the risk.'' 
\end{itemize}
This might be operationalized as follows: first, for hot and cold events, the interpretation is somewhat straightforward, since anthropogenic influence clearly increases the risk of hot events and clearly decreases the risk of cold events. Consider the interval estimates of $p$ for hot events and three different cutoffs (1, 2, and 10), shown in Figure \ref{p_hot}. Here's a suggestion for how to interpret these plots:

\begin{itemize}
\item If your cutoff for making a definitive statement about anthropogenic influence is 1, then you don't need to worry about the ocean's internal variability for [regions with CrI's entirely in (0.95, 1] in the top panel, e.g., all South American regions]; in other words, your conclusions are insensitive to the year chosen.
\item On the other hand, if your cutoff for making a definitive statement about anthropogenic influence is 2, then you don't need to worry about the ocean's internal variability for [regions with CrI's entirely in (0.95, 1] in the middle panel, e.g., all African regions].
\end{itemize}
...and so on. On a map, the hashing could correspond to different categories of the cutoff value, with the regions shaded according to the largest value for which this is true. This might be interpreted as:
\begin{itemize}
\item[] \textbf{Even if your threshold for establishing a significant anthropogenic influence is as extreme as [1, 2, or 10], then the effect of the ocean's internal variability does not change the outcome of a D\&A analysis for regions with the corresponding shading.}
\end{itemize}
Again for hot, the shading would be:

\begin{itemize}
\item \textbf{No shading:} CHN-NandNE, MNG, RUS-E, RUS-Krasnoyarsk, etc. (all those that cross the 0.95 boundary in the top panel)

\item \textbf{Shaded for cutoff of 1:}  ASEAN-W, CHN-NW, CHN-SCandE, etc. (all those that do not cross the 0.95 boundary in top panel but do cross the 0.95 boundary in the middle panel)

\item \textbf{Shaded for cutoff of 2:} ArabL-E, ArabL-N, CHN-SW, IND, AUS-NE, AUS-W, etc. (all those that do not cross the 0.95 boundary in middle panel but do cross the 0.95 boundary in the bottom panel)

\item \textbf{Shaded for cutoff of 10:} COD, EAC, SADC-NW, Andean-S (all those that do not cross the 0.95 boundary in the bottom panel)
\end{itemize}
I've implemented the above shading strategy for all of the regions, and have applied the shading to our estimate of the effect of the ocean's internal variability. See Figure \ref{shadings}.


\begin{figure}[!b]
\begin{center}
	\includegraphics[width = \textwidth]{IV_wshading.pdf}
\caption{Interval estimates of the magnitude of the effect of oceanic internal variability on the risk ratio. The estimates are shaded according to whether (solid) or not (dotted/dashed) the effect of this internal variability impacts qualitative statements about attributing risk.}
\label{shadings}
\end{center}
\end{figure}

A similar approach works for cold events (Figure \ref{p_cold}). Wet events are more difficult (Figure \ref{p_wet}).



\begin{figure}[!h]
\begin{center}
	\includegraphics[width = 1.075\textwidth]{exceedance_probabilities_HOT.pdf}
\caption{Interval estimates of $p$ for hot events in each region and three different cutoffs.}
\label{p_hot}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
	\includegraphics[width = 1.075\textwidth]{exceedance_probabilities_COLD.pdf}
\caption{Interval estimates of $p$ for cold events in each region and three different cutoffs.}
\label{p_cold}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
	\includegraphics[width = 0.8\textwidth]{exceedance_probabilities_WET.pdf}
\caption{Interval estimates of $p$ for wet events in each region and four different cutoffs.}
\label{p_wet}
\end{center}
\end{figure}






















\end{document}
