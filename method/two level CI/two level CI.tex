\documentclass[times,12pt]{article}

%\usepackage{macros}
\usepackage{mdwlist}
\usepackage[utf8]{inputenc}

% load packages
\usepackage{times,natbib}
\usepackage{balance}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{multirow}
\usepackage[margin=1in]{geometry}
\usepackage{subfigure}
\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{breakurl}
\usepackage{natbib,upgreek}
\usepackage{bbm}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{doi}
\DeclareMathOperator{\logit}{logit}

\newcommand{\bfbeta}{{\boldsymbol \beta}}
\newcommand{\bftheta}{{\boldsymbol \theta}}

\newcommand{\bfs}{{\bf s}}
\newcommand{\bfx}{{\bf x}}
\def\bSig\mathbf{\Sigma}
\DeclareMathOperator{\CH}{CH_4}


%\newcommand{\todo}[1]{\textbf{[\color{green} #1]}}
%\newcommand{\pfc}[1]{\textbf{[\color{blue}Peter: #1]}}
%\newcommand{\rh}[1]{\textbf{[\color{red}Radu: #1]}}
%\newcommand{\vv}[1]{\mbox{\boldmath $#1$}}


\begin{document}
\doublespacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Contrasting sampling and internal variability}

\textbf{Scenario:} Sometime after our paper is released, which quantifies the effect of the ocean's internal variability, suppose another research team comes along and conducts a one-year, atmosphere-only D\&A analysis by estimating the risk ratio for a certain event type in a certain region (such that the region can be associated with one of the WRAF regions in our analysis). The new research team might want to know: given the sampling variability in the new study, how much concern needs to be taken over the fact that the variability introduced by the ocean was ignored? In other words, how would an interval estimate have changed if the effect of the ocean's variability had been taken into consideration? Would the lower bound of the interval cross a threshold such that the results of the study would change?

In order to answer this question, consider the following framework. Define the new study's estimate of the (log) risk ratio to be
$
\widehat{\theta} \equiv \widehat{\theta}(X_1, \dots, X_n),
$
where the estimator $\widehat{\theta}$ is a function of random variates $X_1, \dots, X_n$. More specifically, to denote that the estimator is for the risk in a specific year or time period $t$, rewrite
\[
\widehat{\theta}_t \equiv \widehat{\theta}(X_{t1}, \dots, X_{tn}),
\]
Whatever the functional form of the estimator, we might assume its sampling distribution to look something like
\[
\widehat{\theta}_t \sim N(\theta_t, \tau^2/n),
\]
where $\theta_t$ is the true log risk for year $t$ and $\tau^2/n$ represents the sampling variability in the estimator. 

As the notation implies, a confidence interval for $\theta_t$ using only $\tau^2/n$ is in fact only an interval estimate for the (log) risk in a single year. In fact, the true log risk ratio for year $t$ itself comes from a population distribution which involves variability, in this case arising from the unaccounted-for effect of the ocean's internal variability. That is, we might have something like
\[
\theta_t \sim N(\mu, \sigma^2),
\]
where now $\mu$ represents a ``population'' mean (the population being all possible years) and $\sigma^2$ represents the magnitude of oceanic internal variability (for events of a certain type in the specified region). 

To answer our original question, what we actually need to estimate is a lower (or upper) quantile of the population distribution, most likely the 2.5$^{\text{th}}$ percentile (to correspond to a 95\% confidence interval). 
%Formally, with the population distribution as given and $\sigma^2$ considered ``fixed,'' 
%
%
%So, to answer our question, we require an estimate of a lower quantile of the population distribution, i.e., $N(\mu, \sigma^2)$.
%
\textbf{One very rough approximation to obtain this} involves the distribution of
$
p(\widehat{\theta}_t | \mu ) = \int_{\theta_t} p(\widehat{\theta}_t | \theta_t) p(\theta_t | \mu) d\theta_t,
$
where the implicit conditioning on $\tau^2$ and $\sigma^2$ is suppressed in the notation. Given the specifications for $p(\widehat{\theta}_t | \theta_t)$ and $p(\theta_t | \mu)$ above, the integral follows from the classic derivation for the marginal distribution of the data in a Normal-Normal Bayesian posterior calculation. That is,
\[
p(\widehat{\theta}_t | \mu ) = N(\mu, \tau^2/n + \sigma^2).
\]
Again, what we are really interested in is a confidence interval for the lower bound of the 95\% confidence interval; i.e., for $\phi =$ the 2.5$^{\text{th}}$ percentile of the above distribution, or $\phi =  \mu - 1.96\sqrt{\tau^2/n + \sigma^2}$. Because $\phi$ is simply a shift of $\mu$, we can use a 95\% confidence interval for $\mu$, which is
\[
\left( \widehat{\theta}_t - 1.96\sqrt{\tau^2/n + \sigma^2}, \widehat{\theta}_t + 1.96\sqrt{\tau^2/n + \sigma^2} \right),
\]
and transform the interval to obtain a 95\% confidence interval for $\phi = f(\mu) = \mu - 1.96\sqrt{\tau^2/n + \sigma^2}$, as follows: 
\[
\left( f(\widehat{\theta}_t) - 1.96\sqrt{\text{Var}[f(\widehat{\theta}_t)]},  f(\widehat{\theta}_t) + 1.96\sqrt{\text{Var}[f(\widehat{\theta}_t)]} \right).
\]
Since $\text{Var}[f(\widehat{\theta}_t)] = \text{Var}\hskip0.5ex \widehat{\theta}_t = \tau^2/n + \sigma^2$, this becomes
\[
\left( \left[\widehat{\theta}_t - 1.96\sqrt{\tau^2/n + \sigma^2}\right] - 1.96\sqrt{\tau^2/n + \sigma^2},  \left[\widehat{\theta}_t - 1.96\sqrt{\tau^2/n + \sigma^2}\right] + 1.96\sqrt{\tau^2/n + \sigma^2} \right)
\]
or $\left( \widehat{\theta}_t - 3.92\sqrt{\tau^2/n + \sigma^2}, \widehat{\theta}_t \right)$. If this interval crosses some threshold (i.e., $\log 1 = 0$), then the research team should beware that their results might be sensitive to the specific year chosen.

%This framework allows us to get at a confidence interval for a percentile of the population distribution. One method for this is given in Chakraborti and Li (2007), and is as follows: using $s = \sqrt{\widehat{\tau}^2/n + \widehat{\sigma}^2}$, a $100(1-\alpha)\%$ equal-tailed interval for the $100p^{\text{th}}$ percentile is
%\[
%\left( \widehat{\theta}_t + CZ_ps - b\frac{s}{\sqrt{n}} \sqrt{ 1 + nZ_p^2(C^2 - 1)}, \hskip2ex\widehat{\theta}_t + CZ_ps - a\frac{s}{\sqrt{n}} \sqrt{ 1 + nZ_p^2(C^2 - 1)} \right),
%\]
%where
%\begin{itemize}
%\item $C = C(n) = \sqrt{\frac{n-1}{2}} \Gamma\left( \frac{n-1}{2}\right)/ \Gamma\left( \frac{n}{2}\right)$ is a small-sample bias correction,
%\item $Z_p$ is the $100p^{\text{th}}$ percentile of a $N(0,1)$ distribution, and 
%\item for moderately large $n$,  $a$ and $b$ can be approximated as 
%\[
%b \approx t_{1-\alpha/2, n-1} \hskip3ex \text{and} \hskip3ex a \approx t_{\alpha/2, n-1}.
%\]
%(For small $n$, the exact values of $a$ and $b$ can be obtained numerically.)
%\end{itemize}
%
%
%Therefore, we can derive a confidence interval in the usual way: the statistic
%\[
%Z = \frac{\widehat{\theta}_t - \mu}{\sqrt{\tau^2/n + \sigma^2}} \sim N(0,1),
%\]
%which means that $P( -1.96 \leq Z \leq 1.96 ) = 0.95$ and therefore
%\[
%P( \widehat{\theta}_t - 1.96 \sqrt{\tau^2/n + \sigma^2} \leq \mu \leq \widehat{\theta}_t + 1.96 \sqrt{\tau^2/n + \sigma^2}) = 0.95.
%\]
%This implies that a confidence interval for $\mu$, the population log risk ratio, based on $\widehat{\theta}_t$ is
%\[
%\widehat{\theta}_t \pm 1.96\sqrt{\tau^2/n + \sigma^2}.
%\]
%In practice, we might plug in estimates $\widehat{\tau}^2$ (from the new analysis) and $\widehat{\sigma}^2$ (from our analysis) to arrive at an approximate confidence interval; additionally, a critical value from the correct $t$-distribution might be more appropriate.




























\end{document}
