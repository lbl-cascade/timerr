\documentclass[times,12pt]{article}

%\usepackage{macros}
\usepackage{mdwlist}
\usepackage[utf8]{inputenc}

% load packages
\usepackage{times,natbib}
\usepackage{balance}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{multirow}
\usepackage[margin=1in]{geometry}
\usepackage{subfigure}
\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{breakurl}
\usepackage{natbib,upgreek}
\usepackage{bbm}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{doi}
\DeclareMathOperator{\logit}{logit}

\newcommand{\bfbeta}{{\boldsymbol \beta}}
\newcommand{\bfs}{{\bf s}}
\newcommand{\bfx}{{\bf x}}
\def\bSig\mathbf{\Sigma}


%\newcommand{\todo}[1]{\textbf{[\color{green} #1]}}
%\newcommand{\pfc}[1]{\textbf{[\color{blue}Peter: #1]}}
%\newcommand{\rh}[1]{\textbf{[\color{red}Radu: #1]}}
%\newcommand{\vv}[1]{\mbox{\boldmath $#1$}}


\begin{document}
\doublespacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{New model: logistic regression with random effects} 

The likelihood for this model is defined in terms of the parameters
\begin{equation*} \label{probDefs}
\begin{array}{rcl} 
p_{At} & = & \text{ the probability of the prescribed event under ALL forcings for year $t$} \\ 
p_{Nt} & = & \text{ the probability of the prescribed event under NAT forcings for year $t$} \\ 
\end{array}
\end{equation*}
and corresponding random variables
\begin{equation*} \label{zDefs}
\begin{array}{rcl} 
Z_{At} & = & \text{ the number of ALL simulations of year $t$ in which the event occurred} \\ 
Z_{Nt} & = & \text{ the number of NAT simulations of year $t$ in which the event occurred} \\ 
\end{array}
\end{equation*}
for $t = 1, \dots, T$.
Conditional on each $p_{kt}$, we model $Z_{kt} \sim \text{Bin}(n_t, p_{kt})$. Conditional on all parameters, each year is modeled as being independent of all other years, and the likelihood for the full data is
\begin{equation*} \label{likelihood}
p({\bf Z} ; {\bf p}) = \prod_{k \in \{A, N\}} \prod_{t=1}^T p(Z_{kt} ; p_{kt}),
\end{equation*}
where 
$
{\bf Z} = (Z_{A1}, \dots, Z_{AT}, Z_{N1}, \dots, Z_{NT})
$ 
and 
$
{\bf p}  = (p_{A1}, \dots, p_{AT}, p_{N1}, \dots, p_{NT}).
$
We're interested in modeling the year-specific risk ratios, $RR_t = p_{At}/p_{Nt}$, and would like to borrow strength across years and include forcings-specific covariate information.


One way to do this is to model the probabilities on the logit scale, tying together the years and scenarios using random effects. That is, model
\[
\logit p_{kt} = \mu_k + \alpha_t + \delta_{t}\cdot \mathbbm{1}(k = A) + {\bf x}_{kt}^\top \bfbeta,
\]
where $\mu_k$, $k \in \{A,N\}$, is a scenario-specific (fixed) effect, $\alpha_t$, $t=1, \dots, T$, is a year-specific random effect (common across ALL and NAT), $\delta_{t}$ is a year-specific random effect for the ALL forcings scenario, ${\bf x}_{kt}$ is a vector of covariates, and $\bfbeta$ is a vector of regression coefficients. The random effects will be modeled as
\[
\alpha_t \stackrel{\text{iid}}{\sim} N(0, \tau^2), \hskip3ex \delta_{t} \stackrel{\text{iid}}{\sim} N(0, \sigma^2).
\]
%Integrating over the random effects, this model induces correlation between the scenarios for a specific year. That is, it can be shown that the model specifies
%\[
%\text{Corr}( \logit p_{At}, \logit p_{Nt} ) = \frac{\tau^2}{\tau^2 + \sigma^2},
%\]
%for $t=1, \dots, T$.

\section{Comparison to previous approach}

Previously, the model specified
\[
\log RR_t = \mu_R + \delta_t, \hskip3ex
\logit p_{At} = \mu_A + \alpha_t.
\]
In this case, approximating $\logit^{-1}(x) \approx \exp(x)$ for small $x$, we have
\[
p_{Nt} = \frac{p_{At}}{RR_t} \approx \frac{\exp \{ \mu_A + \alpha_t\} }{ \exp \{ \mu_R + \delta_t \} },
\]
so that the risk ratio can be written as
\[
\frac{p_{At}}{p_{Nt}} \approx  \frac{\exp \{ \mu_A + \alpha_t\} }{ \exp \{ \mu_A - \mu_R + \alpha_t - \delta_t \} } = \frac{\exp \{ \mu_A + \alpha_t\} }{ \exp \{ \mu_N + \alpha_t - \delta_t \} },
\]
where $\mu_N \equiv \mu_A - \mu_R$. Using the new approach, the risk ratio is approximately
\[
RR_t \approx \frac{\exp \{ \mu_A + \alpha_t + \delta_t \} }{ \exp \{ \mu_N + \alpha_t \} },
\]
which is equivalent to the previous representation. Note that we can write $RR_t \approx RR_0 \cdot \exp\{ \delta_t \}$,
where $RR_0 = \exp\{ \mu_A - \mu_N \}$ represents a baseline (or overall) risk.

So: why use the new parameterization?
\begin{itemize}
\item The probabilities are modeled on the logit scale -- which ensures the correct prior support for both $p_A$ and $p_N$.
\item More straightforward to enter scenario-specific covariates
\end{itemize}


%\subsection{Parameterizing the model in terms of $p_N$ (or $p_A$) and $RR$} \label{pNRR}
%
%In this case, we have
%\[
%\logit (p_N^{(i)}) = \delta_i, \hskip4ex \log(RR^{(i)}) = \alpha_i,
%\] 
%where
%\[
%\alpha_i  \stackrel{\text{iid}}{\sim} N(\mu_R, \tau^2_R) \hskip2ex \text{and} \hskip2ex \delta_i  \stackrel{\text{iid}}{\sim} N(\mu_N, \tau^2_N), \hskip3ex i = 1, \dots, T.
%\]
%To complete the prior specification, set
%\[
%\mu_R \sim N(0,c_\mu^2), \hskip3ex \mu_N \sim N(0,c_\mu^2), \hskip3ex \tau^2_R \sim U(0,C_\tau), \hskip3ex \tau^2_N\sim U(0,C_\tau),
%\]
%where the hyperparameters of these distributions ($c_\mu^2$ and $C_\tau$) are fixed such that the priors are diffuse but proper. The logit of the ALL forcings probabilities is then
%\[
%\logit( p_A^{(i)}) = \logit \big( p_N^{(i)} \cdot RR^{(i)} \big) = \logit \big( \logit^{-1}(\delta_i) \cdot \exp \{\alpha_i\} \big). 
%\]
%
%Alternatively, the priors could be put explicitly on $p_A$ and $RR$.
%
%However, we've found this approach to be problematic.

%
%
%\subsection{Beta regression with (Gaussian) random effects} \label{betaReg1}
%
%{\footnotesize [Summary at \url{http://www.de.ufpe.br/~raydonal/Talks/ShortCourseBetaRegression.pdf}.]}
%
%\noindent One new way to parameterize the model is to use the idea of beta regression (Ferrari and Cribari-Neto, 2004; Simas et al., 2010). In this approach, the probabilities are assigned beta distributions, parameterized in terms of the mean and ``precision.'' More specifically, the prior for a general probability $p$ is
%\[
%\pi( p; \mu, \phi ) = \frac{\Gamma(\phi)}{\Gamma(\mu\phi)\Gamma\big((1-\mu)\phi\big)} \> p^{\mu\phi-1} \> (1-p)^{(1-\mu)\phi - 1}, \hskip4ex 0<p<1.
%\]
%Under this parameterization, we have 
%\[
%E[p] = \mu, \hskip4ex Var(p) = \frac{\mu(1-\mu)}{1+\phi},
%\]
%where $0<\mu<1$ and $\phi>0$. While $\phi$ is not precisely the precision, it is termed as such because $\phi$ is inversely related to $Var(p)$.
%
%The regression component is introduced as follows. For a random sample $(p_k^{(1)}, \dots, p_k^{(T)})$ ($k\in\{A,N\}$), assume each $p_k^{(i)} \sim B(\mu_{ki}, \phi_{ki})$, where
%\[
%g(\mu_{ki}) = \delta_{ki} + {\bf x}_{ki}^\top \bfbeta_k.
%\]
%Here, $g(\cdot)$ is a link function such that $g:(0,1)\rightarrow \mathbbm{R}$. In order to tie together the parameters for a particular year, the random effects $(\delta_{Ai}, \delta_{Ni})^\top$ are modeled as 
%\begin{equation} \label{REdist}
%\left[ \begin{array}{c} \delta_{Ai} \\ \delta_{Ni} \end{array} \right] \stackrel{\text{iid}}{\sim} N \left( \left[ \begin{array}{c}  \theta_A \\ \theta_N  \end{array} \right], \left[ \begin{array}{cc} \sigma^2_A & \rho \> \sigma_A \sigma_N \\  \rho \> \sigma_A \sigma_N  & \sigma_N^2 \end{array} \right] \right), \hskip2ex i = 1, \dots, T.
%\end{equation}
%A similar regression model could be specified for the $\phi_{ki}$, or we might just fix $\phi_{ki} \equiv \phi_k$.

%Under this approach, it remains to specify a joint prior for $\theta_A$ and $\theta_N$, which are the logit mean event probabilities under ALL and NAT forcings, respectively, for all years (after adjusting for covariate information). Several approaches are as follows:
%
%\begin{enumerate}
%\item Common distribution: 
%$
%\theta_A, \theta_N \stackrel{\text{iid}}{\sim} N(\theta, \tau^2)
%$
%
%\item Bivariate Gaussian (copula):
%\[
%\left[ \begin{array}{c} \theta_A \\ \theta_N \end{array} \right] \sim N \left( \left[ \begin{array}{c} 0 \\ 0 \end{array} \right], \tau^2  \left[ \begin{array}{cc} 1 & \rho \\ \rho & 1 \end{array} \right] \right)
%\]
%
%\item Scale model: specify $\theta_A = \xi \theta_N$, and then set out to estimate $\xi$. In this case, note
%\[
%\xi = \frac{\theta_A}{\theta_N},
%\]
%so since the $\theta$s represent logit mean event probabilities, this is in a way the ``logit risk ratio.''
%
%\item Other options?
%
%\end{enumerate}
%
%\subsection{Beta regression with (beta) random effects}
%
%First, suppose that there's no covariate information to be included. Then, as in Section \ref{betaReg1}, model $p_k^{(i)} \sim B(\mu_{ki}, \phi_{k})$, but specify
%\[
%\mu_{ki} \sim B(\theta_k, \sigma_k).
%\]
%That is, the random effect distribution is on the Beta scale. Again, jointly model $\theta_A$ and $\theta_N$.
%
%However, not sure how to include covariates?











\end{document}
