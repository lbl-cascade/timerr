\documentclass[times,12pt]{article}

%\usepackage{macros}
\usepackage{mdwlist}
\usepackage[utf8]{inputenc}

% load packages
\usepackage{times,natbib}
\usepackage{balance}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{multirow}
\usepackage[margin=1in]{geometry}
\usepackage{subfigure}
\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{breakurl}
\usepackage{natbib,upgreek}
\usepackage{bbm}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{doi}
\DeclareMathOperator{\logit}{logit}

\newcommand{\bfbeta}{{\boldsymbol \beta}}
\newcommand{\bfs}{{\bf s}}
\newcommand{\bfx}{{\bf x}}
\def\bSig\mathbf{\Sigma}


%\newcommand{\todo}[1]{\textbf{[\color{green} #1]}}
%\newcommand{\pfc}[1]{\textbf{[\color{blue}Peter: #1]}}
%\newcommand{\rh}[1]{\textbf{[\color{red}Radu: #1]}}
%\newcommand{\vv}[1]{\mbox{\boldmath $#1$}}


\begin{document}
\doublespacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Alternative prior choices for $p_A$ and $p_N$} 

Regardless of the prior choice, the likelihood is defined in terms of the parameters
\begin{equation*} \label{probDefs}
\begin{array}{rcl} 
p_A^{(i)} & = & \text{ the probability of the prescribed event under ALL forcings for year $i$} \\ 
p_N^{(i)} & = & \text{ the probability of the prescribed event under NAT forcings for year $i$} \\ 
\end{array}
\end{equation*}
and corresponding random variables
\begin{equation*} \label{zDefs}
\begin{array}{rcl} 
Z_A^{(i)} & = & \text{ the number of ALL simulations of year $i$ in which the event occurred} \\ 
Z_N^{(i)} & = & \text{ the number of NAT simulations of year $i$ in which the event occurred} \\ 
\end{array}
\end{equation*}
for $i = 1, \dots, T$.
Conditional on each $p_k^{(i)}$, we model the random variables $Z_k^{(i)}$ as arising from a binomial distribution with success probability $p_k^{(i)}$ and $n_i$ total trials. 

Conditional on all of the parameters, each year is modeled as being independent of all other years, and the likelihood for all of the data is then
\begin{equation*} \label{likelihood}
p({\bf Z} ; {\bf p}) = \prod_{k \in \{A, N\}} \prod_{i=1}^T p(Z_k^{(i)} ; p_k^{(i)}),
\end{equation*}
where 
$
{\bf Z} = (Z_A^{(1)}, \dots, Z_A^{(T)}, Z_N^{(1)}, \dots, Z_N^{(T)})
$ 
and 
$
{\bf p}  = (p_A^{(1)}, \dots, p_A^{(T)}, p_N^{(1)}, \dots, p_N^{(T)}).
$
We're interested in modeling the year-specific risk ratios, $RR^{(i)} = p_A^{(i)}/p_N^{(i)}$, and would like to borrow strength across years and include forcings-specific covariate information.

%\subsection{Parameterizing the model in terms of $p_N$ (or $p_A$) and $RR$} \label{pNRR}
%
%In this case, we have
%\[
%\logit (p_N^{(i)}) = \delta_i, \hskip4ex \log(RR^{(i)}) = \alpha_i,
%\] 
%where
%\[
%\alpha_i  \stackrel{\text{iid}}{\sim} N(\mu_R, \tau^2_R) \hskip2ex \text{and} \hskip2ex \delta_i  \stackrel{\text{iid}}{\sim} N(\mu_N, \tau^2_N), \hskip3ex i = 1, \dots, T.
%\]
%To complete the prior specification, set
%\[
%\mu_R \sim N(0,c_\mu^2), \hskip3ex \mu_N \sim N(0,c_\mu^2), \hskip3ex \tau^2_R \sim U(0,C_\tau), \hskip3ex \tau^2_N\sim U(0,C_\tau),
%\]
%where the hyperparameters of these distributions ($c_\mu^2$ and $C_\tau$) are fixed such that the priors are diffuse but proper. The logit of the ALL forcings probabilities is then
%\[
%\logit( p_A^{(i)}) = \logit \big( p_N^{(i)} \cdot RR^{(i)} \big) = \logit \big( \logit^{-1}(\delta_i) \cdot \exp \{\alpha_i\} \big). 
%\]
%
%Alternatively, the priors could be put explicitly on $p_A$ and $RR$.
%
%However, we've found this approach to be problematic.



\subsection{Beta regression with (Gaussian) random effects} \label{betaReg1}

{\footnotesize [Summary at \url{http://www.de.ufpe.br/~raydonal/Talks/ShortCourseBetaRegression.pdf}.]}

\noindent One new way to parameterize the model is to use the idea of beta regression (Ferrari and Cribari-Neto, 2004; Simas et al., 2010). In this approach, the probabilities are assigned beta distributions, parameterized in terms of the mean and ``precision.'' More specifically, the prior for a general probability $p$ is
\[
\pi( p; \mu, \phi ) = \frac{\Gamma(\phi)}{\Gamma(\mu\phi)\Gamma\big((1-\mu)\phi\big)} \> p^{\mu\phi-1} \> (1-p)^{(1-\mu)\phi - 1}, \hskip4ex 0<p<1.
\]
Under this parameterization, we have 
\[
E[p] = \mu, \hskip4ex Var(p) = \frac{\mu(1-\mu)}{1+\phi},
\]
where $0<\mu<1$ and $\phi>0$. While $\phi$ is not precisely the precision, it is termed as such because $\phi$ is inversely related to $Var(p)$.

The regression component is introduced as follows. For a random sample $(p_k^{(1)}, \dots, p_k^{(T)})$ ($k\in\{A,N\}$), assume each $p_k^{(i)} \sim B(\mu_{ki}, \phi_{ki})$, where
\[
g(\mu_{ki}) = \delta_{ki} + {\bf x}_{ki}^\top \bfbeta_k.
\]
Here, $g(\cdot)$ is a link function such that $g:(0,1)\rightarrow \mathbbm{R}$. In order to tie together the parameters for a particular year, the random effects $(\delta_{Ai}, \delta_{Ni})^\top$ are modeled as 
\begin{equation} \label{REdist}
\left[ \begin{array}{c} \delta_{Ai} \\ \delta_{Ni} \end{array} \right] \stackrel{\text{iid}}{\sim} N \left( \left[ \begin{array}{c}  \theta_A \\ \theta_N  \end{array} \right], \left[ \begin{array}{cc} \sigma^2_A & \rho \> \sigma_A \sigma_N \\  \rho \> \sigma_A \sigma_N  & \sigma_N^2 \end{array} \right] \right), \hskip2ex i = 1, \dots, T.
\end{equation}
A similar regression model could be specified for the $\phi_{ki}$, or we might just fix $\phi_{ki} \equiv \phi_k$.

%Under this approach, it remains to specify a joint prior for $\theta_A$ and $\theta_N$, which are the logit mean event probabilities under ALL and NAT forcings, respectively, for all years (after adjusting for covariate information). Several approaches are as follows:
%
%\begin{enumerate}
%\item Common distribution: 
%$
%\theta_A, \theta_N \stackrel{\text{iid}}{\sim} N(\theta, \tau^2)
%$
%
%\item Bivariate Gaussian (copula):
%\[
%\left[ \begin{array}{c} \theta_A \\ \theta_N \end{array} \right] \sim N \left( \left[ \begin{array}{c} 0 \\ 0 \end{array} \right], \tau^2  \left[ \begin{array}{cc} 1 & \rho \\ \rho & 1 \end{array} \right] \right)
%\]
%
%\item Scale model: specify $\theta_A = \xi \theta_N$, and then set out to estimate $\xi$. In this case, note
%\[
%\xi = \frac{\theta_A}{\theta_N},
%\]
%so since the $\theta$s represent logit mean event probabilities, this is in a way the ``logit risk ratio.''
%
%\item Other options?
%
%\end{enumerate}

\subsection{Logistic regression with (Gaussian) random effects}

This approach combines our previous approach and Section \ref{betaReg1}: model both $p_A$ and $p_N$ on the logit scale (related to our previous approach), and then tie together the random effect means (as in Section \ref{betaReg1}). That is, for $k \in \{ A,N \}$, model
\[
\logit p_k^{(i)} = \delta_{ki} + {\bf x}_{ki}^\top \beta_k,
\]
where the random effects are modeled as in Eq. (\ref{REdist}).

How does this approach differ from Section \ref{betaReg1}? One way: in this approach, we can't explicitly model $Var(p_k)$, which can be done in \ref{betaReg1} (through modeling of the $\phi$s).


\subsection{Beta regression with (beta) random effects}

First, suppose that there's no covariate information to be included. Then, as in Section \ref{betaReg1}, model $p_k^{(i)} \sim B(\mu_{ki}, \phi_{k})$, but specify
\[
\mu_{ki} \sim B(\theta_k, \sigma_k).
\]
That is, the random effect distribution is on the Beta scale. Again, jointly model $\theta_A$ and $\theta_N$.

However, not sure how to include covariates?











\end{document}
