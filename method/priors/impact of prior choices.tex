\documentclass[times,12pt]{article}

%\usepackage{macros}
\usepackage{mdwlist}
\usepackage[utf8]{inputenc}

% load packages
\usepackage{times,natbib}
\usepackage{balance}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{multirow}
\usepackage[margin=1in]{geometry}
\usepackage{subfigure}
\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{breakurl}
\usepackage{natbib,upgreek}
\usepackage{bbm}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{doi}
\DeclareMathOperator{\logit}{logit}

\newcommand{\bfbeta}{{\boldsymbol \beta}}
\newcommand{\bfs}{{\bf s}}
\newcommand{\bfx}{{\bf x}}
\def\bSig\mathbf{\Sigma}


%\newcommand{\todo}[1]{\textbf{[\color{green} #1]}}
%\newcommand{\pfc}[1]{\textbf{[\color{blue}Peter: #1]}}
%\newcommand{\rh}[1]{\textbf{[\color{red}Radu: #1]}}
%\newcommand{\vv}[1]{\mbox{\boldmath $#1$}}


\begin{document}
\doublespacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Impact of priors chosen for the risk ratio example} 

Regardless of the prior choice, the likelihood is defined in terms of the parameters
\begin{equation*} \label{probDefs}
\begin{array}{rcl} 
p_A^{(i)} & = & \text{ the probability of the prescribed event under ALL forcings for year $i$} \\ 
p_N^{(i)} & = & \text{ the probability of the prescribed event under NAT forcings for year $i$} \\ 
\end{array}
\end{equation*}
and corresponding random variables
\begin{equation*} \label{zDefs}
\begin{array}{rcl} 
Z_A^{(i)} & = & \text{ the number of ALL simulations of year $i$ in which the event occurred} \\ 
Z_N^{(i)} & = & \text{ the number of NAT simulations of year $i$ in which the event occurred} \\ 
\end{array}
\end{equation*}
for $i = 1, \dots, T$.
Conditional on each $p_k^{(i)}$, we model the random variables $Z_k^{(i)}$ as arising from a binomial distribution with success probability $p_k^{(i)}$ and $n_i$ total trials. 

Conditional on all of the parameters, each year is modeled as being independent of all other years, and the likelihood for all of the data is then
\begin{equation*} \label{likelihood}
p({\bf Z} ; {\bf p}) = \prod_{k \in \{A, N\}} \prod_{i=1}^T p(Z_k^{(i)} ; p_k^{(i)}),
\end{equation*}
where 
$
{\bf Z} = (Z_A^{(1)}, \dots, Z_A^{(T)}, Z_N^{(1)}, \dots, Z_N^{(T)})
$ 
and 
$
{\bf p}  = (p_A^{(1)}, \dots, p_A^{(T)}, p_N^{(1)}, \dots, p_N^{(T)}).
$
We're interested in modeling the year-specific risk ratios, $RR^{(i)} = p_A^{(i)}/p_N^{(i)}$. For now, we assume no covariate information is to be used.

\subsection{Parameterizing the model in terms of $p_N$ and $RR$}

In this case, we have
\[
\logit (p_N^{(i)}) = \delta_i, \hskip4ex \log(RR^{(i)}) = \alpha_i,
\] 
where
\[
\alpha_i  \stackrel{\text{iid}}{\sim} N(\mu_R, \tau^2_R) \hskip2ex \text{and} \hskip2ex \delta_i  \stackrel{\text{iid}}{\sim} N(\mu_N, \tau^2_N), \hskip3ex i = 1, \dots, T.
\]
To complete the prior specification, set
\[
\mu_R \sim N(0,c_\mu^2), \hskip3ex \mu_N \sim N(0,c_\mu^2), \hskip3ex \tau^2_R \sim U(0,C_\tau), \hskip3ex \tau^2_N\sim U(0,C_\tau),
\]
where the hyperparameters of these distributions ($c_\mu^2$ and $C_\tau$) are fixed such that the priors are diffuse but proper. The logit of the ALL forcings probabilities is then
\[
\logit( p_A^{(i)}) = \logit \big( p_N^{(i)} \cdot RR^{(i)} \big) = \logit \big( \logit^{-1}(\delta_i) \cdot \exp \{\alpha_i\} \big). 
\]

\subsubsection{Exploring the prior on $p_A$}

To understand the prior on the ALL forcings probabilities, we can first draw a sample of hyperparameters $\mu_R, \tau^2_R, \mu_N$, and $\tau^2_N$ from their respective priors. Then, conditional on these values, we can draw values of $\delta_i$ and $\alpha_i$, and use these values to calculate a value of $p_A^{(i)}$; repeating this many times will allow us to get a sense of what the prior over $p_A^{(i)}$ looks like. However, in the case where the priors are diffuse, we may set $C_\tau = 100$ and $c_\mu^2 = 100^2$. For some of the draws, we might, for example, draw $\tau^2_R \approx 100$ and $\mu_R \approx 100$, in which case the draw of $\alpha_i \sim N(100, 100)$ could yield $\alpha_i >> 100$. Then, as long as the corresponding draw of $\delta_i$ is positive (say $\delta_i = 1$), even for $\alpha_i = 100$ the result is
\[
\logit^{-1}(\delta_i) \cdot \exp\{\alpha_i\} = \logit^{-1}(1) \cdot \exp\{100\} \approx \frac{1}{4} \cdot \exp\{100\} >> 1,
\]
so that $\logit( p_A^{(i)}) = \logit \big( \logit^{-1}(\delta_i) \cdot \exp \{\alpha_i\} \big)$ is undefined.

I'm not sure what this means for the prior on $p_A$. If we calculated
\[
\logit( p_A^{(i)} ) = \logit \big( \min \big\{ \logit^{-1}(\delta_i) \cdot \exp \{\alpha_i\}, 1-\epsilon \big\} \big),
\]
(for small $\epsilon$) then this essentially creates a ``spike and slab'' prior for $\logit( p_A^{(i)} )$ -- see Figure \ref{priors}.

\subsection{Parameterizing the model in terms of $p_A$ and $RR$}

Essentially the same results occur when the parameterizations are switched -- again see Figure \ref{priors}.

\subsection{Comparison}

Clearly, with the truncation, the priors look very different under the different parameterizations. However, note that even if the truncation is ignored, there appears to be more variability in the prior for $\logit p_A$ when the model is parameterized in terms of $p_N$ (and vice versa): the lower left histogram has larger negative values than the upper left. Not sure if or how this is affecting the analysis.

\begin{figure}[!t]
\begin{center}
\includegraphics[width = \textwidth]{priors.pdf}
\caption{Simulated priors for $\logit p_A$ and $\logit p_N$, under each parameterization.}
\label{priors}
\end{center}
\end{figure}

The priors on the raw scale are given in Figure \ref{rawscale} -- these also look a little unnatural.

\begin{figure}[!t]
\begin{center}
\includegraphics[width = \textwidth]{priors_rawscale.pdf}
\caption{Simulated priors for $p_A$ and $p_N$, under each parameterization.}
\label{rawscale}
\end{center}
\end{figure}


\section{Alternative parameterization}

A completely different way to parameterize the model is as follows: let
\[
p_A^{(i)} \stackrel{\text{iid}}{\sim} B(\theta_A, \sigma^2_A), \hskip4ex p_N^{(i)} \stackrel{\text{iid}}{\sim} B(\theta_N, \sigma^2_N),
\]
where $B(m,v)$ the Beta distribution with mean $m$ and variance $v$. Then, for example, we might define diffuse priors for $\sigma^2_A$ and $\sigma^2_N$ (say $U(0,C)$, $C$ fixed at a reasonable value for the scale of the distribution) and then model
\[
\logit( \theta_A ) = \overline{\theta}_A + \bfbeta_A \big( \text{covariates corresponding to ALL forcings} \big)
\]
and
\[
\logit( \theta_N ) = \overline{\theta}_N + \bfbeta_N \big( \text{covariates corresponding to NAT forcings} \big).
\]
We could then tie together the two models by having $\overline{\theta}_A$ and $\overline{\theta}_N$ come from a common prior (or maybe another strategy?).

\textbf{One other question:} should the variance for $p_A^{(i)}$ and $p_N^{(i)}$ depend on the ensemble (sample) size for year $i$? That is, something like:
\[
p_A^{(i)} \stackrel{\text{ind}}{\sim} B(\theta_A, \sigma^2_A/n_i)
\]
Maybe this is already accounted for in the likelihood?







\end{document}
