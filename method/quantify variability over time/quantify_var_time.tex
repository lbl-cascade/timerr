\documentclass[times,12pt]{article}

%=================================================================================
% load packages
\usepackage{times,natbib}
\usepackage{balance}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{multirow}
\usepackage[margin=1in]{geometry}
\usepackage{subfigure}
\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{breakurl}
\usepackage{natbib,upgreek}
\usepackage{bbm}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{doi}

%=================================================================================
% define some new commands
\def\argmax{\mathop{\rm argmax}}
\def\argmin{\mathop{\rm argmin}}
\def\arginf{\mathop{\rm arginf}}
\newcommand{\bfsigma}{{\boldsymbol \sigma}}
\newcommand{\bftheta}{{\boldsymbol \theta}}
\newcommand{\bfbeta}{{\boldsymbol \beta}}
\def\bSig\mathbf{\Sigma}
\newcommand{\VS}{V\&S}
\newcommand{\tr}{\mbox{tr}}

%=================================================================================
% formatting
\topmargin=-0.5in \textheight=9in
\sloppy \hyphenpenalty=10000


%=================================================================================
\begin{document}

\doublespacing

%=================================================================================

\section{Quantifying the variability in risk ratio over time} 

Using the Bayesian model, we can obtain posterior MCMC samples of the yearly risk ratio for each of $T$ years, denoted $RR_{t}$, for $t = 1, \dots, T$, where suppose we have also removed the effect of any covariates. Our goal is to summarize the variability over time in the risk ratio. 

For each of the following, we are taking the ``finite population'' approach, such that our summary will speak to the specific years for which we have data, rather than trying to make statements about a larger ``super population'' of all possible years.

\subsection{Overall estimate of the time-averaged risk ratio}

First, we can consider the posterior distribution of the overall estimate of the risk ratio, averaged over time:
\[
\overline{RR} = \frac{1}{T} \sum_{t=1}^T RR_{t}
\]
This quantity speaks to the range of values that the risk ratios cover over all $T$ years but, since it is largely influenced by the sampling variability within each year, does not provide a good quantification of the year-to-year variability. Consider the toy example in Figure \ref{figure1}, which contains risk ratios for ten years as well as a red line which gives the overall mean risk ratio (red line) and an interval for the overal risk ratio (blue line). Both scenarios contain similar levels of year-to-year variability, but the interval estimates for the overall risk ratio are very different. 

\begin{figure}[!h]
\begin{center}
\includegraphics[width = \textwidth]{toyExample3.pdf}
\caption{Two scenarios with very different overall risk ratio interval estimates (blue line) but similar levels of year-to-year variability.}
\label{figure1}
\end{center}
\end{figure}

Conversely, the overall risk ratio estimates could be the same even when there are drastically different levels of year-to-year variability. Consider the toy example in Figure \ref{figure2}: clearly, the scenario on the right contains a great deal of year-to-year variability while the scenario on the left has essentially no year-to-year variability, yet the blue intervals are the same.

Hence, while interesting, an overall estimate of the risk ratio seems insufficient for quantifying the year-to-year variability.

\begin{figure}[!h]
\begin{center}
\includegraphics[width = \textwidth]{toyExample2.pdf}
\caption{Two scenarios with similar overall risk ratio estimates (blue line) but very different levels of year-to-year variability.}
\label{figure2}
\end{center}
\end{figure}

\subsection{Measures of variability}
We are more interested in how the yearly estimates $RR_t$ deviate about the overall mean $\overline{RR}$. Several possibilities for how to measure this are as follows:

\begin{itemize}
\item Variance, $V = \frac{1}{T} \sum_{t=1}^T \left(RR_{t} - \overline{RR} \right)^2$: a point estimate $\widehat{V}$ would indicate the average squared deviation of yearly values from the overall mean.

\item Standard deviation, $S = \sqrt{ \frac{1}{T} \sum_{t=1}^T \left(RR_{t} - \overline{RR} \right)^2 }$: similar to $V$, a point estimate $\widehat{S}$ would indicate the average deviation of yearly values from the overall mean.

\item Mean absolute deviation, $MAD = \frac{1}{T} \sum_{t=1}^T \left| RR_{t} - \overline{RR} \right|$: a point estimate $\widehat{MAD}$ would indicate the average absolute difference from the overall mean.

\item Mean relative absolute deviation, $MRAD = \frac{1}{T} \sum_{t=1}^T \left| \frac{RR_{t}}{\overline{RR}} - 1 \right|$: a point estimate $\widehat{MAD}$ would indicate the average percent difference from the overall mean.

\item Minimum ratio, $MinR = \min_{t=1, \dots, T} \frac{RR_{t}}{\overline{RR}}$: this quantity is slightly less straightforward, and would be influenced by outliers. Nonetheless, a point estimate $\widehat{MinR}$ would indicate the minimum ratio over time, or the largest negative departure from the overall mean risk ratio.

\item Maximum ratio, $MaxR = \max_{t=1, \dots, T} \frac{RR_{t}}{\overline{RR}}$: similar to above,  a point estimate $\widehat{MaxR}$ would indicate the maximum ratio over time, or the largest positive departure from the overall mean risk ratio.
\end{itemize}
For each of these measures, we can provide a point estimate along with a measure of uncertainty. 

\subsection{Texas application}
For the Texas extreme precipitation example, consider the posterior summaries (Table \ref{tab1}) and distributions (Figure \ref{fig3}) for each measure of variability.

\begin{table}[h]
\caption{Posterior summaries for each variance measure.}
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
Quantity			& Posterior mean 	& Posterior median & SD	& 95\% CI lower 	& 95\% CI upper  \\ \hline \hline
$\overline{RR}$		& 2.07 	& 2.05	& 0.32	& 1.53	& 2.78		\\ \hline
$V$				& 0.44 	& 0.28	& 0.57	& 0.02	& 1.82		\\ \hline
$S$				& 0.58 	& 0.53	& 0.32	& 0.14	& 1.35		\\ \hline
$MAD$			& 0.46 	& 0.42	& 0.24	& 0.11	& 1.03		\\ \hline
$MRAD$			& 0.22 	& 0.21	& 0.10	& 0.06	& 0.43		\\ \hline
$MinR$			& 0.60 	& 0.60	& 0.16	& 0.29	& 0.88		\\ \hline
$MaxR$			& 1.63 	& 1.55	& 0.39	& 1.13	& 2.61		\\ \hline
\end{tabular}
\end{center}
\label{tab1}
\end{table}%3      MAD 0.4604835 0.4232473 0.24281350 0.11067099 1.0285100


\begin{figure}[!h]
\begin{center}
\includegraphics[width = \textwidth]{varPost.pdf}
\caption{Posterior distributions and 95\% credible intervals for each measure of variability.}
\label{fig3}
\end{center}
\end{figure}

Here's an attempt at interpreting each of the measures above:

\begin{enumerate}
\item We estimate that the overall, time-averaged risk ratio from 1997-2013 is approximately $\widehat{\overline{RR}} = 2.07$, with a 95\% probability that the true value lies between $1.53$ and $2.78$.
\item We estimate that the average squared deviation between the yearly risk ratios and the overall average risk ratio is approximately $\widehat{V} = 0.41$, with a 95\% probability that the true average squared deviation is between $0.02$ and $1.29$.
\item Similarly, we estimate that the average deviation between the yearly risk ratios and the overall average is approximately $\widehat{S}=0.55$, with a 95\% probability that the true average deviation is between $0.15$ and $1.13$.
\item We estimate that the average absolute difference between the yearly values and overall mean is approximately $\widehat{MAD} = 0.46$, with a 95\% probability that the percent difference is between $0.11$ and $1.03$.
\item We estimate that the average percent difference between the yearly values and overall mean is approximately $\widehat{MRAD} = 0.21$, with a 95\% probability that the percent difference is between $0.06$ and $0.39$.
\end{enumerate}
%[MDR note: I really don't think the MinR and MaxR are intuitive summaries here; forgoing their interpretations.]

\end{document}






