\documentclass[times,12pt]{article}

%\usepackage{macros}
\usepackage{mdwlist}
\usepackage[utf8]{inputenc}

% load packages
\usepackage{enumitem}
\usepackage{times,natbib}
\usepackage{balance}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{multirow}
\usepackage[margin=1in]{geometry}
\usepackage{subfigure}
\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{breakurl}
\usepackage{natbib,upgreek}
\usepackage{bbm}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{doi}
\DeclareMathOperator{\logit}{logit}

\newcommand{\bfbeta}{{\boldsymbol \beta}}
\newcommand{\bfs}{{\bf s}}
\newcommand{\bfx}{{\bf x}}
\def\bSig\mathbf{\Sigma}

\setitemize[0]{leftmargin=60pt}

%\newcommand{\todo}[1]{\textbf{[\color{green} #1]}}
%\newcommand{\pfc}[1]{\textbf{[\color{blue}Peter: #1]}}
%\newcommand{\rh}[1]{\textbf{[\color{red}Radu: #1]}}
%\newcommand{\vv}[1]{\mbox{\boldmath $#1$}}


\begin{document}
\doublespacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{MCMC strategies for logistic regression with random effects} 

The data for this model involves binomial counts of events occurring for what we take to be independent simulations of weather from two climate scenarios: first, a climate scenario representing the ``world as it is,'' which includes both natural and anthropogenic effects (the ALL scenario), and second, a climate scenario representing the ``world as it might have been,'' which includes natural effects only (the NAT scenario). The counts are aggregated by year; define random variables
\begin{equation*} \label{zDefs}
\begin{array}{rcl} 
Z_{At} & = & \text{ the number of ALL simulations of year $t$ in which the event occurred} \\ 
Z_{Nt} & = & \text{ the number of NAT simulations of year $t$ in which the event occurred} \\ 
\end{array}
\end{equation*}
for years $t = 1, \dots, T$; define ${\bf Z} = (Z_{A1}, \dots, Z_{AT}, Z_{N1}, \dots, Z_{NT})$. The likelihood for these data is
\begin{equation} \label{full_model}
\begin{array} {c}
Z_{kt} | \boldsymbol{\alpha}, \boldsymbol{\delta}, \bfbeta_A, \bfbeta_N  \stackrel{\text{ind}}{\sim} \text{Binomial}\big( n_t, p_{kt} \big), \\[2ex]
p_{kt} = \logit^{-1} \Big[ {\bf x}_t^\top\bfbeta_N\mathbbm{1}_{\{k=N\}} + \alpha_t + ({\bf w}_t^\top\bfbeta_A + \delta_t ) \mathbbm{1}_{\{k=A\}} \Big],
\end{array}
\end{equation}
for $k \in \{A,N\}$ and $t=1, \dots, T$. Here, ${\bf x}_t = (1, x_{t1}, \dots, x_{tp-1})$ is a vector of covariates for the NAT scenario, ${\bf w}_t = (1, w_{t1}, \dots, w_{tq-1})$ is a vector of covariates for the ALL scenario, $\bfbeta_A$ and $\bfbeta_N$ are scenario-specific regression coefficients, and $\mathbbm{1}_{\{\cdot\}}$ is an indicator function. A potentially more straightforward representation of the probabilities in (\ref{full_model}) is
\begin{equation*} \label{model1}
p_{Nt} = \logit^{-1} [ {\bf x}_t^\top\bfbeta_N + \alpha_t ] \hskip3ex \text{and} \hskip3ex p_{At} = \logit^{-1} [ {\bf w}_t^\top\bfbeta_A + \alpha_t + \delta_t ].
\end{equation*}
Finally, the $\boldsymbol{\alpha} = (\alpha_1, \dots, \alpha_T)$ and $\boldsymbol{\delta} = (\delta_1, \dots, \delta_T)$ are modeled as random effects, i.e., 
\[
\alpha_t \stackrel{\text{iid}}{\sim} N(0, \tau^2) \hskip3ex \text{and} \hskip3ex \delta_{t} \stackrel{\text{iid}}{\sim} N(0, \sigma^2).
\]
A Bayesian approach can be used by specifying a prior distribution $p(\bfbeta_A, \bfbeta_N, \tau^2, \sigma^2)$; we will use 
\[
p(\bfbeta_A, \bfbeta_N, \tau^2, \sigma^2) = p(\bfbeta_A) p(\bfbeta_N) p( \tau^2) p( \sigma^2),
\] specifying each component to be noninformative but proper and conjugate, with fixed hyperparameters. The posterior distribution of interest is then
\begin{equation} \label{post}
\begin{array} {rcl}
p(\boldsymbol{\alpha}, \boldsymbol{\delta}, \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2 | {\bf Z}) & \propto & \prod_{k \in \{A,N\}} \prod_{t=1}^T  p(Z_{kt} | \boldsymbol{\alpha}, \boldsymbol{\delta}, \bfbeta_A, \bfbeta_N) \\
 &  & \times \> p(\boldsymbol{\alpha} | \tau^2) \times p(\boldsymbol{\delta} | \sigma^2)  \times p(\bfbeta_A, \bfbeta_N, \tau^2, \sigma^2)
\end{array}
\end{equation}
Several strategies for sampling from (\ref{post}) in a MCMC framework are as follows.

\subsection{Traditional Gibbs sampler with Metropolis-Hastings}

A traditional MCMC approach to sampling from (\ref{post}) is as follows:

\noindent\hrulefill

\noindent {\rm {\bf Traditional sampler:} }

\begin{itemize}
\item[\textit{Step 1}:] Draw $(\boldsymbol{\alpha}, \boldsymbol{\delta})$ from $p(\boldsymbol{\alpha}, \boldsymbol{\delta} | \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, {\bf Z} )$

\item[\textit{Step 2 (A)}:] Draw $( \bfbeta_A, \bfbeta_N)$ from $p( \bfbeta_A, \bfbeta_N | \boldsymbol{\alpha}, \boldsymbol{\delta}, \tau^2, \sigma^2, {\bf Z} ) = p( \bfbeta_A, \bfbeta_N | \boldsymbol{\alpha}, \boldsymbol{\delta}, {\bf Z} )$

\item[\textit{Step 3 (S)}:] 
\item[(a)] Draw $\tau^2$ from $p( \tau^2 | \bfbeta_A, \bfbeta_N, \boldsymbol{\alpha}, \boldsymbol{\delta}, \sigma^2, {\bf Z} ) = p( \tau^2 | \boldsymbol{\alpha} )$

\item[(b)] Draw $\sigma^2$ from $p( \sigma^2 | \bfbeta_A, \bfbeta_N, \boldsymbol{\alpha}, \boldsymbol{\delta}, \tau^2, {\bf Z} ) = p( \sigma^2 | \boldsymbol{\delta} )$

\end{itemize}

\vskip-2ex

\noindent\hrulefill

\noindent Conjugate priors allow for Gibbs steps to be used in steps 3 (a) and (b); regardless of prior choice, steps 1 and 2 will require Metropolis-Hastings steps. The \textit{A} and \textit{S} labels will be explained in the next section.

\subsection{Ancillarity-Sufficiency Interweaving Strategy (ASIS) MCMC}

Yu and Meng (2011) outline a different approach, designed to boost the efficiency of an MCMC algorithm. Their approach is based on the literature on comparing MCMC for multilevel models with either a centered parameterization (CP) or a non-centered parameterization (NCP), and proposes a strategy that alternates (``interweaves'') sampling from both parameterizations. The ASIS terminology replaces the CP/NCP terminology by framing the problem in terms of the mathematically equivalent data augmentation schemes of \textit{ancillary augmentation} (AA) and \textit{sufficient augmentation} (SA), arguing that this terminology better captures the ``essence'' of the method. Regardless, in the AA scheme (i.e., NCP), the missing data (i.e., latent variables) are an ancillary statistic for the parameter of interest, and in the SA scheme (i.e., CP), the missing data are a sufficient statistic for the parameter of interest.

Note now that the standard augmentation of $\boldsymbol{\alpha}$ and $\boldsymbol{\delta}$ are AA for $( \bfbeta_A, \bfbeta_N)$ but SA for $( \tau^2, \sigma^2)$ -- hence, the \textit{A} and \textit{S} notation in the traditional sampler. To implement the ASIS sampler, we need to find an AA for $( \tau^2, \sigma^2)$ and an SA for $( \bfbeta_A, \bfbeta_N)$.

\vskip3ex
\noindent \underline{SA for $( \bfbeta_A, \bfbeta_N)$:} 

\noindent Define new latent variables $\eta_t = {\bf x}_t^\top\bfbeta_N + \alpha_t$ and $\nu_t = {\bf w}_t^\top\bfbeta_A + \alpha_t + \delta_t$, so that the parameterization in (\ref{full_model}) can be rewritten as
\begin{equation} \label{model2a}
Z_{kt} | \boldsymbol{\eta}, \boldsymbol{\nu} \stackrel{\text{ind}}{\sim} \text{Binomial}\big( n_t, \logit^{-1}[ \eta_t \mathbbm{1}_{\{k=N\}} + \nu_t \mathbbm{1}_{\{k=A\}} ] \big) %\hskip3ex k \in \{A,N\}, t=1,\dots,T.
\end{equation}
and
\begin{equation} \label{model2b}
\eta_t \stackrel{\text{ind}}{\sim} N({\bf x}_t^\top\bfbeta_N, \tau^2) \hskip3ex \text{and} \hskip3ex \nu_{t} \stackrel{\text{ind}}{\sim} N(  {\bf w}_t^\top\bfbeta_A, \tau^2 + \sigma^2).
\end{equation}
Note that $p({\bf Z} | \boldsymbol{\eta}, \boldsymbol{\nu}, \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2) = p({\bf Z} | \boldsymbol{\eta}, \boldsymbol{\nu})$; therefore $(\boldsymbol{\eta}, \boldsymbol{\nu})$ are SA for $( \bfbeta_A, \bfbeta_N)$ (and, for that matter, also for $( \tau^2, \sigma^2)$).

The traditional sampler can be improved by adding a step that samples $( \bfbeta_A, \bfbeta_N)$ under the SA. Actually, this step will be split into two steps so that Gibbs steps can be used:

\begin{itemize}
\item[\textit{Step 2 (S)}:] 

\item[(a)] Draw $\bfbeta_A$ from $p( \bfbeta_A | \bfbeta_N,  \boldsymbol{\eta}, \boldsymbol{\nu}, \tau^2, \sigma^2, {\bf Z} ) = p( \bfbeta_A | \boldsymbol{\nu}, \tau^2, \sigma^2 )$

\item[(b)] Draw $\bfbeta_N$ from $p( \bfbeta_N | \bfbeta_A,  \boldsymbol{\eta}, \boldsymbol{\nu}, \tau^2, \sigma^2, {\bf Z} ) = p( \bfbeta_N | \boldsymbol{\eta}, \tau^2 )$

\end{itemize}

\noindent Using conjugate (Gaussian) priors for $\bfbeta_N$ and $\bfbeta_A$, these can be sampled in a Gibbs step.

\vskip3ex
\noindent \underline{AA for $( \tau^2, \sigma^2)$:} 

\noindent Define new latent variables $\kappa_t = \alpha_t/\tau$ and $\xi_t = \delta_t/\sigma$; the parameterization in (\ref{model1}) can be rewritten as
\begin{equation} \label{model3a}
Z_{kt} | \boldsymbol{\kappa}, \boldsymbol{\xi}, \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2 \stackrel{\text{ind}}{\sim} \text{Bin}\big( n_t, \logit^{-1}[ {\bf x}_t^\top\bfbeta_N\mathbbm{1}_{\{k=N\}} + \tau \kappa_t + ({\bf w}_t^\top\bfbeta_A + \sigma \xi_t ) \mathbbm{1}_{\{k=A\}} ] \big) %\hskip3ex k \in \{A,N\}, t=1,\dots,T.
\end{equation}
and
\begin{equation} \label{model3b}
\kappa_t \stackrel{\text{iid}}{\sim} N(0, 1) \hskip3ex \text{and} \hskip3ex \xi_{t} \stackrel{\text{iid}}{\sim} N(  0, 1).
\end{equation}
Because of (\ref{model3b}), we can see that $(\boldsymbol{\kappa}, \boldsymbol{\xi})$ are  AA for $( \tau^2, \sigma^2)$ (and $( \bfbeta_A, \bfbeta_N)$). Hence, we can add the following step:

\begin{itemize}
\item[\textit{Step 3 (A)}:] Draw $( \tau^2, \sigma^2)$ from $p(  \tau^2, \sigma^2 | \bfbeta_A , \bfbeta_N,  \boldsymbol{\kappa}, \boldsymbol{\xi}, {\bf Z} )$

\end{itemize}

\noindent This step now requires Metropolis-Hastings.

Combining all of the above, the following is a componentwise interweaving sampler (CIS) using ASIS. The intermediate latent variables are calculated between the $A$ and $S$ step, and the original variables are then updated after the $S$ step. For each of these calculations, the most recently sampled parameter values are used: the ``$*$'' sub- and superscripts are used to explicitly show this.

\noindent\hrulefill

\noindent {\rm {\bf CIS:} }

\begin{itemize}
\item[\textit{Step 1}:] Draw $(\boldsymbol{\alpha}^*, \boldsymbol{\delta}^*)$ from $p(\boldsymbol{\alpha}, \boldsymbol{\delta} | \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, {\bf Z} )$

\item[\textit{Step 2 (A)}:] Draw $( \bfbeta_A^*, \bfbeta_N^*)$ from $p( \bfbeta_A, \bfbeta_N | \boldsymbol{\alpha}^*, \boldsymbol{\delta}^*, \tau^2, \sigma^2, {\bf Z} ) = p( \bfbeta_A, \bfbeta_N | \boldsymbol{\alpha}^*, \boldsymbol{\delta}^*, {\bf Z} )$

\item[\textbf{Calculate:}]  $\eta_t = {\bf x}_t^\top\bfbeta_N^* + \alpha_t^*$ and $\nu_t = {\bf w}_t^\top\bfbeta_A^* + \alpha_t + \delta_t^*$ for $t=1,\dots, T$.

\item[\textit{Step 2 (S)}:] 

\item[(a)] Draw $\bfbeta_A^{**}$ from $p( \bfbeta_A | \bfbeta_N^*,  \boldsymbol{\eta}, \boldsymbol{\nu}, \tau^2, \sigma^2, {\bf Z} ) = p( \bfbeta_A | \boldsymbol{\nu}, \tau^2, \sigma^2 )$

\item[(b)] Draw $\bfbeta_N^{**}$ from $p( \bfbeta_N | \bfbeta_A^{**},  \boldsymbol{\eta}, \boldsymbol{\nu}, \tau^2, \sigma^2, {\bf Z} ) = p( \bfbeta_N | \boldsymbol{\eta}, \tau^2 )$

\item[\textbf{Update:}]  $\alpha_t^{**} = \eta_t - {\bf x}_t^\top\bfbeta_N^{**}$ and $\delta_t^{**} = \nu_t - {\bf w}_t^\top\bfbeta_A^{**} - \alpha_t^{**}$ for $t=1,\dots, T$.

\item[\textbf{Calculate:}]  $\kappa_t = \alpha_t^{**}/\tau_*$ and $\xi_t = \delta_t^{**}/\sigma_*$ for $t=1,\dots, T$.

\item[\textit{Step 3 (A)}:] Draw $( \tau^2_*, \sigma^2_*)$ from $p(  \tau^2, \sigma^2 | \bfbeta_A^{**} , \bfbeta_N^{**},  \boldsymbol{\kappa}, \boldsymbol{\xi}, {\bf Z} )$

\item[\textbf{Update:}]  $\alpha_t^{***} = \tau_{*}\kappa_t$ and $\delta_t^{***} = \sigma_{*}\xi_t$ for $t=1,\dots, T$.

\item[\textit{Step 3 (S)}:] 
\item[(a)] Draw $\tau^2_{**}$ from $p( \tau^2 | \bfbeta_A^{**}, \bfbeta_N^{**}, \boldsymbol{\alpha}^{***}, \boldsymbol{\delta}^{***}, \sigma^2_*, {\bf Z} ) = p( \tau^2 | \boldsymbol{\alpha}^{***} )$

\item[(b)] Draw $\sigma^2_{**}$ from $p( \sigma^2 | \bfbeta_A^{**}, \bfbeta_N^{**}, \boldsymbol{\alpha}^{***}, \boldsymbol{\delta}^{***}, \tau^2_{**}, {\bf Z} ) = p( \sigma^2 | \boldsymbol{\delta}^{***} )$



\end{itemize}

\vskip-2ex

\noindent\hrulefill

\noindent At the end of each iteration, save the most recently sampled/calculated values of the parameters: $(\{ \alpha^{***}_t, \delta^{***}_t : t= 1, \dots, T \}, \bfbeta_A^{**}, \bfbeta_N^{**}, \tau^2_{**}, \sigma^2_{**})$.

\subsection{Component-wise vs. block sampling}

Two versions of Step 1 will be used for implementing both algorithms, namely {component-wise}  and {block} Metropolis-Hastings (MH) steps.

\vskip3ex
\noindent \underline{Component-wise MH for $(\boldsymbol{\alpha}, \boldsymbol{\delta})$:} 

\noindent In this scheme, the individual components of $(\boldsymbol{\alpha}, \boldsymbol{\delta})$ will be sampled one at a time. That is, Step 1 will be replaced with

\begin{itemize}
\item[\textit{Step 1}:] For $t=1, \dots, T$, 

\item[(a)] Draw $\alpha_t$ from $p(\alpha_t | \boldsymbol{\delta}, \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, {\bf Z} ) = p(\alpha_t | \delta_t, \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, Z_{At}, Z_{Nt} )$

\item[(b)] Draw $\delta_t$ from $p(\delta_t | \boldsymbol{\alpha}, \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, {\bf Z} ) = p( \delta_t | \alpha_t, \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, Z_{At} )$

\end{itemize}

\vskip3ex
\noindent \underline{Block MH for $(\boldsymbol{\alpha}, \boldsymbol{\delta})$:} 

\noindent Alternatively, the yearly random effects ($\alpha_t$ and $\delta_t$) can be sampled together, as it was found that these are the components that are trading off with one another. Now, Step 1 will be replaced with

\begin{itemize}
\item[\textit{Step 1}:] For $t=1, \dots, T$, draw $(\alpha_t, \delta_t)$ from 

\[
p(\alpha_t, \delta_t | \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, {\bf Z} ) = p(\alpha_t, \delta_t | \bfbeta_A, \bfbeta_N, \tau^2, \sigma^2, Z_{At}, Z_{Nt} )
\]
\end{itemize}

 
\noindent \textbf{Note 1:} using conjugate priors, Step 2 (S) and Step 3 (S) can be done using a Gibbs sampler step. However, regardless of prior choice, Step 2 (A) and Step 3 (A) require MH; for these steps, component-wise MH steps will be used (e.g., in sampling $\bfbeta_A$, we will sequentially sample $\beta_{A0}, \dots, \beta_{Ap}$).

\noindent \textbf{Note 2:} All of the MH steps will be done using a random walk with Gaussian proposal distribution. The block sampler will use a bivariate Gaussian proposal distribution with a large negative correlation ($\approx -0.98$), as samples of the random effects common to a specific year ($\alpha_t$ and $\delta_t$) were found to be negatively correlated in the componentwise sampler (see Figure \ref{rand_eff}).
 
\section{Application and results}

We now compare the samplers for our real data example. The data consist of counts from a total of $T=54$ years; the covariate vectors consist of an intercept and a scenario-specific global mean temperature covariate.

The four samplers used are: 

\begin{itemize}
\item[\textbf{Sampler 1:}] the traditional sampler with component-wise MH for step 1.

\item[\textbf{Sampler 2:}] the traditional sampler with block MH for step 1.

\item[\textbf{Sampler 3:}] the CIS with component-wise MH for step 1.

\item[\textbf{Sampler 4:}] the CIS with block MH for step 1.

\end{itemize}

\begin{figure}[!t]
\begin{center}
\includegraphics[width = \textwidth]{mean_variance.pdf}
\caption{Trace plots of post burn-in samples for the mean coefficient and random effect variance parameters, for each of the four samplers. The parameters shown are (from the top): $\beta_{A0}$ (ALL intercept), $\beta_{N0}$ (NAT intercept), $\beta_{A1}$ (ALL global mean temperature coefficient), $\beta_{N1}$ (NAT global mean temperature coefficient), $\tau^2$, and $\sigma^2$.}
\label{mean_variance}
\end{center}
\end{figure}

\begin{figure}[!t]
\begin{center}
\includegraphics[width = \textwidth]{rand_effs.pdf}
\caption{Trace plots of post burn-in samples for a selection of the random effect parameters, for each of the four samplers. The parameters shown are (from the top): $\alpha_{29}$, $\delta_{29}$, $\alpha_{40}$, and $\delta_{40}$. Note that in Samplers 1 and 3 (with componentwise sampling), the $\alpha$ and $\delta$ parameters for the same $t$ are almost perfectly negatively correlated (hence the strong negative correlation in the block MH step).}
\label{rand_eff}
\end{center}
\end{figure}

The results are given in Figures \ref{mean_variance} and \ref{rand_eff}, which show traceplots of posterior samples after tuning and burn-in. Figure \ref{mean_variance} shows results for the mean coefficient and random effect variance parameters, for each of the four samplers. In this figure, it is clear that Samplers 1 and 2 sample the intercept parameters ($\beta_{A0}$ and $\beta_{N0}$) inefficiently, while Samplers 3 and 4 sample these parameters much more effectively. The trace plots for the variance parameters look okay across all samplers, but it looks like something strange is going in on Sampler 1 for $\sigma^2$. Furthermore, note that the samples of $\sigma^2$ are much larger in Samplers 3 and 4 (the $y$-axis goes from 2 to 12 instead of 1 to 5). For these parameters, we would prefer Sampler 3 or 4.

While Sampler 3 does a good job of sampling the mean coefficients, in Figure \ref{rand_eff} we see that it does a very poor job of sampling the random effect parameters (as does Sampler 1). Interestingly, Sampler 2 (which does not use ASIS techniques) does a fine job sampling the random effects. For these parameters, we would prefer Sampler 2 or 4.

Thus, both the block sampling and the CIS greatly improve the efficiency of the MCMC, but it is not until these strategies are combined that the sampler provides the best efficiency (Sampler 4).

As a final comparison, consider the computational times needed to compute 20,000 iterations of each sampler, after tuning and burn-in, given in Table \ref{comp_time}. Interestingly, Samplers 2 and 4 take significantly longer than 1 and 3 -- and even Sampler 2 takes longer than Sampler 3 in spite of bivariate sampling in the random effects (hence $T$ fewer MH steps at each iteration) and without using the CIS (i.e., without Steps 2 (A) and 3 (S)). Not sure why this might be the case -- the only thing I can think of is the use of {\tt mvrnorm} in Samplers 2 and 4. In any case, the computational time is quite fast for each of the samplers (and, the MCMC could certainly be made more efficient in other small ways).

\begin{table}[!t]
\begin{center}
\begin{tabular}{|c|c|c|}
\hline
\textbf{Sampler} & \textbf{Computational time (seconds)} & \textbf{Time per iteration (seconds)}\\ \hline \hline
1 & 67.24 & 0.0034 \\ \hline
2 & 99.77 & 0.0050 \\ \hline
3 & 76.38 & 0.0038 \\ \hline
4 & 115.97 & 0.0058 \\ \hline
\end{tabular}
\end{center}
\caption{Computational time needed to calculate 20,000 iterations of each sampler, after tuning and burn-in.}
\label{comp_time}
\end{table}%















\end{document}
