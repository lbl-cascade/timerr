\documentclass[times,12pt]{article}

%=================================================================================
% load packages
\usepackage{times,natbib}
\usepackage{balance}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{multirow}
\usepackage[margin=1in]{geometry}
\usepackage{subfigure}
\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{breakurl}
\usepackage{natbib,upgreek}
\usepackage{bbm}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{doi}

%=================================================================================
% define some new commands
\def\argmax{\mathop{\rm argmax}}
\def\argmin{\mathop{\rm argmin}}
\def\arginf{\mathop{\rm arginf}}
\newcommand{\bfsigma}{{\boldsymbol \sigma}}
\newcommand{\bftheta}{{\boldsymbol \theta}}
\newcommand{\bfbeta}{{\boldsymbol \beta}}
\def\bSig\mathbf{\Sigma}
\newcommand{\VS}{V\&S}
\newcommand{\tr}{\mbox{tr}}

%=================================================================================
% formatting
\topmargin=-0.5in \textheight=9in
\sloppy \hyphenpenalty=10000


%=================================================================================
\begin{document}

\doublespacing

%=================================================================================

\section{Modeling risk ratio over time} 

Suppose that we are interested in modeling the risk ratio over a period of $T$ years. The risk ratio involves the ratio of two probabilities, $p_A$ and $p_N$, which represent the probability of a predefined event over a particular region of interest under the world as it is (``ALL'') and a counterfactual world (``NAT''), respectively. The risk ratio for year $i$ is defined
\[
RR_i = \frac{p^{(i)}_A}{p^{(i)}_N}  \hskip3ex i = 1, \dots, T,
\]
where $p^{(i)}_A$ and $p^{(i)}_N$ are the probabilities of interest for year $i$. We are interested in modeling the risk ratio over time to determine if there exists any temporal trend, after possibly correcting for covariates.

Suppose that we have a total of $n$ simulations from each year for each of the forcings scenarios (ALL and NAT), such that each simulation is treated as an independent replicate. Define $Z_{jk}^{(i)}$ for $i = 1, \dots, T$, $j = 1, \dots, n$, and $k\in \{A, N\}$ to be a collection of random variables such that
\[
Z_{jk}^{(i)} = \left\{ \begin{array}{l} 1 \text{ if the event occurred in the $j$th simulation of year $i$ under forcing $k$, and} \\ 0 \text{ otherwise.} \end{array} \right.
\]
Then, we can model $Z_{jk}^{(i)} \sim \text{Bin}(1, p_{k}^{(i)})$, or equivalently $Z_{k}^{(i)} = \sum_j Z_{jk}^{(i)} \sim \text{Bin}(n, p_{k}^{(i)})$.

Several modeling strategies are as follows.

\subsection{Frequentist meta-analysis model} \label{MAmodel}

One way to model the time-varying risk ratio is in a meta-analytic framework, which requires effect size estimates of the log risk ratio for each year, as well as an estimate of the variability in each estimate. The log risk ratio can be estimated using a simple empirical estimate,
\begin{equation*}
\log \widehat{RR}_i = \log \widehat{p}_A^{(i)} - \log \widehat{p}_N^{(i)},
\end{equation*}
where $\widehat{p}_k^{(i)} = z_k^{(i)}/n$ for $k \in \{A,N\}$. An estimate of the variance of $\log \widehat{RR}_i$ can be obtained using the delta method, which yields the approximate (asymptotic) estimated variance
\begin{equation*} \label{DMvars}
\widehat{v}_i \equiv \widehat{\text{var}}(\log \widehat{RR}_i)= \frac{1-\widehat{p}^{(i)}_A}{n\cdot\widehat{p}^{(i)}_A} + \frac{1-\widehat{p}^{(i)}_N}{n\cdot\widehat{p}^{(i)}_N}. 
\end{equation*}
Using these estimates, a simple hierarchical statistical model would be
\[
\log \widehat{RR_i} = \theta_i + e_i,
\]
where $\theta_i$ represents the true (unknown) log risk ratio for year $i$, and $e_i$ is a random variable which represents our uncertainty in estimating $\log \widehat{RR_i}$, assumed to be independently distributed as $N(0, \widehat{v}_i)$. The second level of the hierarchy models the year-specific log risk ratios as
\[
\theta_i = {\bf x}_i^\top \bfbeta + \eta_i,
\]
where ${\bf x}_i = (1, x_{i1}, \dots, x_{ip-1})$ is a $p$-vector of covariate information (e.g., smoothed global mean temperature, ENSO index, etc.) including an intercept, $\bfbeta$ is a vector of unknown regression coefficients, and $\eta_i$ is a random effect for year $i$. The random effect accounts for the fact that, after correcting for the covariates, there are unaccounted-for differences in each year which introduce variability among the true log risk ratios; this framework models the additional variability as purely random. The $u_i$ are then modeled independently as $N(0, \tau^2)$. 

Using this model specification, the parameters $\bfbeta$ and $\tau^2$ can be estimated in a frequentist framework. Aside from considering the regression coefficient estimates, the statistical question of interest is whether $\tau^2=0$, which would indicate homogeneity among the (mean-corrected) true log risk ratios, i.e., that there is no temporal trend in the log risk ratios after accounting for covariates. Estimates of the random effect variance $\tau^2$ could be considered for several choices of the covariate vector, and then compared.

%===============================================
\subsection{Frequentist generalized linear mixed model} \label{glmm} % using {\tt glmer}}

Alternatively, we might implement a generalized linear model that includes a random effect directly for the probabilities, otherwise known as a generalized linear mixed model (GLMM). As before, define 
\begin{equation} \label{probDefs}
\begin{array}{rcl} 
p_A^{(i)} & = & \text{ the probability of the prescribed event under ALL forcings for year $i$, and} \\ 
p_N^{(i)} & = & \text{ the probability of the prescribed event under NAT forcings for year $i$,} \\ 
\end{array}
\end{equation}
along with corresponding random variables
\begin{equation} \label{zDefs}
\begin{array}{rcl} 
Z_A^{(i)} & = & \text{ the number of ALL simulations of year $i$ in which the event occurred, and} \\ 
Z_N^{(i)} & = & \text{ the number of NAT simulations of year $i$ in which the event.} \\ 
\end{array}
\end{equation}
The likelihood for all of the data is then
\begin{equation} \label{likelihood}
p({\bf Z} | {\bf p}) = \prod_{k \in \{A, N\}} \prod_{i=1}^T p(Z_k^{(i)} | p_k^{(i)}),
\end{equation}
where each $p(Z_k^{(i)} | p_k^{(i)}) = \text{Bin}(n, p_k^{(i)})$. With the Binomial likelihood and using a log link (also known as log-Binomial regression), the GLMM would proceed to model
\[
\log p_k^{(i)} = \mu_i + x_k \theta_i + {\bf x}_i \bfbeta + u_{ki}
\]
for $k \in \{A,N\}$, where $x_A = 1$, $x_N = 0$, the $u_{Ai}$ are iid $N(0,\sigma^2_A)$, the $u_{Ni}$ are iid $N(0,\sigma^2_N)$, and ${\bf x}_i$ and $\bfbeta$ are as before. The log-Binomial setup is helpful since we are interested in the risk ratio: ignoring the covariates and random effects, the model specifies $\log p^{(i)}_N = \mu_i$ and $\log p^{(i)}_A = \mu_i + \theta_i$, so that
\[
\log RR_i = \log p^{(i)}_A - \log p^{(i)}_N = \big(\mu_i + \theta_i \big) - \mu_i = \theta_i.
\]
Therefore, estimates of $\theta$ provide information about the risk ratios.

However, this model suffers from two theoretical setbacks: first, the random effects are modeled on the scale of the probabilities, and not the risk ratio, which is difficult to interpret. Furthermore (and more seriously), as in the previous meta-analysis model, the inferential goal would be to use the random effect variances for the probabilities ($\sigma^2_A$ and $\sigma^2_N$) to say something about the year-to-year variability in the risk ratios. This would be possible if, for example, both $\sigma^2_A=0$ and $\sigma^2_N=0$ (i.e., no year-to-year variability in the probabilities implies no  year-to-year variability in the risk ratio), but not if both $\sigma^2_A>0$ and $\sigma^2_N>0$. In this case, we would conclude that there is year-to-year variability in the probabilities, but this conclusion would tell us nothing about how the risk ratios are varying.

Due to these issues, the GLMM will (for now) be excluded from analyses.

%===============================================
\subsection{Bayesian model} \label{bayesianModel}

A Bayesian framework can allow us to avoid the above issue of using a two-stage model as well as estimating a sampling variance for the log risk ratio. %, while also taking care of the problem that observed zeros introduce in estimating the risk ratio.

Using the definitions in (\ref{probDefs}) and (\ref{zDefs}) and the likelihood (\ref{likelihood}), note that while the model is currently parameterized in terms of the year specific probabilities $p_A^{(i)}$ and $p_N^{(i)}$, we are actually concerned about the risk ratio $RR_i = p_A^{(i)}/p_N^{(i)}$. So, we can rewrite (\ref{likelihood}) in terms of $p_N^{(i)}$ and $RR_i$, using the relationship that $p_A^{(i)} = p_N^{(i)}\cdot RR_i$. However, we are also interested in borrowing strength over years and including covariates in modeling both $p_N^{(i)}$ and $RR_i$, so we can use the following models:
\begin{equation} \label{RRmodel}
\log RR_i = {\bf x}_i^\top \boldsymbol{\beta}_R + \alpha_i
\end{equation}
and
\begin{equation} \label{pNmodel}
\log \left( \frac{p_N^{(i)}}{1- p_N^{(i)}} \right) = {\bf w}_i^\top \boldsymbol{\beta}_N + \delta_i.
\end{equation}
Here, ${\bf x}_i = (1, x_{i1}, \dots, x_{ip-1})$ is a vector of covariates for the risk ratio with corresponding regression coefficients $\bfbeta_R$, ${\bf w}_i = (1, w_{i1}, \dots, w_{ip-1})$ is a vector of covariates for the NAT probabilities with corresponding regression coefficients $\bfbeta_N$ (most likely ${\bf x}_i = {\bf w}_i$), and $\alpha_i$ and $\delta_i$ are year-specific effects for the risk ratio and NAT probability, respectively, such that the $\alpha_i$ are iid $N(0, \tau^2_\alpha)$ and the $\delta_i$ are iid $N(0, \tau^2_\delta)$. The variances $\tau^2_\alpha$ and $\tau^2_\delta$ represent the variability in the year-specific effects. Using this parameterization, (\ref{likelihood}) can be rewritten to depend on the parameters $\bfbeta_R$, $\bfbeta_N$, $\boldsymbol{\alpha} = (\alpha_1, \dots, \alpha_T)$, $\tau^2_\alpha$, $\boldsymbol{\delta} = (\delta_1, \dots, \delta_T)$, and $\tau^2_\delta$.

To complete the specification of this Bayesian model, we simply need to specify a prior distribution for all parameters, which can be done as follows:
\begin{equation}\label{prior}
p(\bfbeta_R, \bfbeta_N, \boldsymbol{\alpha}, \tau^2_\alpha,  \boldsymbol{\delta}, \tau^2_\delta) = p(\bfbeta_R) \> p(\bfbeta_N) \> p(\boldsymbol{\alpha}| \tau^2_\alpha) \> p(\tau^2_\alpha) \> p(\boldsymbol{\delta}| \tau^2_\delta) \> p(\tau^2_\delta).
\end{equation}
Specifically, we can use $p(\bfbeta_R) = p(\bfbeta_N) = N_p({\bf 0}, c^2_\beta{\bf I}_p)$; $p(\boldsymbol{\alpha}| \tau^2_\alpha) = N_T({\bf 0}, \tau^2_\alpha{\bf I}_T)$ and $p(\boldsymbol{\delta}| \tau^2_\delta) = N_T({\bf 0}, \tau^2_\delta{\bf I}_T)$ (as already specified); $p(\tau^2_\alpha) = \text{Unif}(0, d_\alpha)$; and $p(\tau^2_\delta) = \text{Unif}(0, d_\delta)$. All hyperparameters will be fixed so that the priors are proper and noninformative.

Inference can proceed using Markov chain Monte Carlo methods.

\subsubsection{Predictive distribution for overall (time-averaged) risk ratio} \label{predDist}

One additional benefit of using a Bayesian approach is that we can easily obtain the predictive distribution of the overall time-averaged risk ratio, conditional on the data. Defining $\Delta = \log \overline{RR}$ and using $D$ to generically represent our data, we want
\begin{equation} \label{predRR}
p(\Delta | D) = \int_\bftheta p(\Delta, \bftheta | D) d\bftheta =  \int_\bftheta p(\Delta | \bftheta, D) p(\bftheta | D) d\bftheta =  \int_\bftheta p(\Delta | \bftheta) p(\bftheta | D) d\bftheta,
\end{equation}
where $\bftheta$ represents a generic vector of parameters and conditional independence of the data and the average log risk ratio (given parameters) establishes the final equality. Thus, (\ref{predRR}) suggests the following algorithm to sample from $p(\Delta | D)$:
\begin{enumerate}
\item Sample $\bftheta^*$ from $p(\bftheta | D)=$ the posterior distribution.
\item Sample $\Delta$ from $p(\Delta | \bftheta = \bftheta^*)$, specified by the model to be $N({\bf x}^\top \bfbeta, \tau^2_\alpha)$. 
\end{enumerate}
Here, ${\bf x}$ is a covariate vector for the log risk ratio. Repeating this algorithm many times gives an estimated predictive distribution for $\Delta = \log \overline{RR}$, and the samples may be exponentiated to obtain an estimated predictive distribution for $\overline{RR}$. Summaries, such as a 90\% credible interval, can be obtained from this estimated distribution. 

\section{Application: Texas precipitation extremes}

To illustrate the aforementioned approaches, consider climate model simulations for the Texas region for the March through August season of the years 1997 to 2013, for which we have 100 simulations for each year under both ALL and NAT. Suppose the event of interest is defined as having less than the 10th percentile of precipitation from all of the ALL simulations (in order to, for now, ensure no observed zeros), in this case 277.22 mm per 6 months. The empirical estimates and asymptotic delta method variances are given in Table \ref{table1}. The Ni\~no 3.4 seasonally-averaged anomaly values are plotted in Figure \ref{figure0}, and the Ni\~no 3.4 values are plotted against the empirical estimates of $p_A$, $p_N$, and $RR$ in Figure \ref{figure0a}.

\begin{table}[!t]
\caption{Empirical estimates of forcing-specific event probabilities, log risk ratios, and delta method approximate variances for each year. The Ni\~no 3.4 index is the anomaly value, averaged over the March--August monthly values.}
\label{table1}
\begin{center}
\begin{tabular}{|c||c|c|c|c||c|}
\hline
Year		& $\widehat{p}_A$	& $\widehat{p}_N$	& $\log \widehat{RR}$	& $\widehat{v}$ & Ni\~no 3.4 index \\ \hline \hline
1997 &0.07 &0.07 & 0.00 &0.27 & 0.97\\ \hline
1998 &0.13 &0.06 &0.77 &0.22 &-0.12\\ \hline
1999 &0.08 &0.04  &0.69 &0.36 &-0.99\\ \hline
2000 &0.07 &0.03  &0.85 &0.46 &-0.69\\ \hline
2001 &0.03 &0.03  &0.00 &0.65 &-0.12\\ \hline
2002 &0.03 &0.02  &0.41 &0.81 & 0.51\\ \hline
2003 &0.09 &0.04  &0.81 &0.34 & 0.03\\ \hline
2004 &0.10 &0.02  &1.61 &0.58 & 0.24\\ \hline
2005 &0.07 &0.01  &1.95 &1.12 & 0.28\\ \hline
2006 &0.07 &0.03  &0.85 &0.46 &-0.01\\ \hline
2007 &0.09 &0.02  &1.50 &0.59 &-0.24\\ \hline
2008 &0.22 &0.11  &0.69 &0.12 &-0.54\\ \hline
2009 &0.06 &0.07 &-0.15 &0.29 & 0.21\\ \hline
2010 &0.07 &0.05  &0.34 &0.32 &-0.27\\ \hline
2011 &0.25 &0.19  &0.27 &0.07 &-0.54\\ \hline
2012 &0.09 &0.02  &1.50 &0.59 & 0.09\\ \hline
2013 &0.18 &0.04  &1.50 &0.29 &-0.23\\ \hline
\end{tabular}
\end{center}
\end{table}%


\begin{figure}[!b]
\begin{center}
\includegraphics[width = \textwidth]{ENSO.pdf}
\caption{Ni\~no 3.4 anomaly values, averaged over the March to August season.}
\label{figure0}
\end{center}
\end{figure}

\begin{figure}[!t]
\begin{center}
\includegraphics[width = \textwidth]{scatterplots.pdf}
\caption{Ni\~no 3.4 anomaly values plotted against empirical estimates.}
\label{figure0a}
\end{center}
\end{figure}

\begin{table}[!t]
\caption{Parameter estimates and confidence intervals for the meta-analysis model with a constant mean.}
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
Parameter		& 	Estimate	&	Standard Error		& 95\% CI lower 	& 95\% CI upper  \\ \hline \hline
$\tau^2$		& 0.011		&	0.090			& --				& --			   \\ \hline
$\beta_0$ (intercept)		& 0.633		&	0.134	& 0.370			& 0.895		   \\ \hline
\end{tabular}
\end{center}
\label{table2}
\end{table}%


\begin{table}[!h]
\caption{Parameter estimates and confidence intervals for the meta-analysis model including the Ni\~no 3.4 index covariate.}
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
Parameter		& 	Estimate	&	Standard Error		& 95\% CI lower 	& 95\% CI upper  \\ \hline \hline
$\tau^2$		& 0.031		&	0.109			& --				& --			   \\ \hline
$\beta_0$ (intercept)		& 0.623		&	0.153	& 0.323			& 0.923		   \\ \hline
$\beta_1$ (Ni\~no coefficient)	& -0.118	&	0.306	& -0.717			& 0.482		   \\ \hline
\end{tabular}
\end{center}
\label{table3}
\end{table}%

\subsection{Meta-analysis results}
 
The meta-analysis model described in Section \ref{MAmodel} using the estimates summarized in Table \ref{table1} will be fit under two scenarios: first, with only a constant mean (i.e., ${\bf x}_i \equiv 1$), and second, including an intercept and the Ni\~no 3.4 index (i.e., ${\bf x}_i = (1, N_i)$). Parameter estimates are obtained using the {\tt rma} function in the {\tt metafor} package in {\tt R}, and are provided in Tables \ref{table2} and \ref{table3}.

In both cases, the estimate of the random effect variance $\tau^2$ is very small and, given the standard error, could conceivably be zero. Furthermore, the Ni\`no coefficient is negative, indicating that years with larger Ni\~no 3.4 anomaly values correspond to smaller risk ratios, that is, the event probabilities are more similar in years with large Ni\~no 3.4 anomalies. However, the coefficient estimate is nonsignificant.

%\subsection{GLMM results}
%
%The GLMM model described in Section \ref{glmm} was fit using the raw counts $z_A^{(i)}$ and $z_N^{(i)}$ and the {\tt glmer} function in the {\tt lme4} package for {\tt R}, again under scenarios which include both a constant mean and an effect for the Ni\~no 3.4 index. Parameter estimates are given in Tables \ref{table3} and \ref{table4}.
%
%\begin{table}[!t]
%\caption{Parameter estimates for the GLMM with a constant mean.}
%\begin{center}
%\begin{tabular}{|c|c|c|c|}
%\hline
%Parameter					& 	Estimate	&	Standard Error		& $P$-value   \\ \hline \hline
%$\sigma_A^2$				& 0.017		&	--				& --		      \\ \hline
%$\sigma_N^2$				& 0.327		&	--				& --		      \\ \hline
%$\mu$ 					& -3.174		&	0.185			& $<$0.001	      \\ \hline
%${\theta_\cdot}$ (overall RR)			& 0.768		&	0.150			& $<$0.001	      \\ \hline
%\end{tabular}
%\end{center}
%\label{table3}
%\end{table}%
%
%\begin{table}[!t]
%\caption{Parameter estimates for the GLMM including the Ni\~no 3.4 index.}
%\begin{center}
%\begin{tabular}{|c|c|c|c|}
%\hline
%Parameter					& 	Estimate	&	Standard Error		& $P$-value   \\ \hline \hline
%$\sigma_A^2$				& 0.027		&	--				& --		      \\ \hline
%$\sigma_N^2$				& 0.298		&	--				& --		      \\ \hline
%$\mu$ 					& -3.222		&	0.183			& $<$0.001    \\ \hline
%$\theta$ (overall RR)			& 0.778		&	0.151			& 0.067	      \\ \hline
%$\beta_1$ (coefficient for Ni\~no) & -0.483		&	0.263			& $<$0.001    \\ \hline
%\end{tabular}
%\end{center}
%\label{table4}
%\end{table}%
%
%First, note that in both models, $\theta_\cdot$ is positive (significantly so) which indicates an overall risk ratio that is significantly greater than 1. Again, the Ni\~no coefficient is negative but nonsignificant. [MDR note: I'm not sure how to extract the year-specific RR estimates -- if this model is even calculating these -- or quite how to interpret the random effect variances.]

\subsection{Bayesian model results}

The Bayesian model outlined in Section \ref{bayesianModel} was fit using JAGS. The fixed hyperparameter values are $c_\beta^2 = 10000^2$, $d_\alpha = d_\delta = 10$. The MCMC was run for 100,000 iterations of adaptation, 30,000 burn-in iterations, and 20,000 saved iterations (for a total run time of approximately 54 seconds on a MacBook Pro laptop). Summaries and estimated posteriors for the regression coefficients and variance parameters are given in Table \ref{table6} and Figures \ref{figure1} and \ref{figure2}. 95\% credible intervals for the year-specific event probabilities and risk ratios are given in Figure \ref{figure3}. 

In general, the results agree with the meta analysis results: the Ni\~no 3.4 coefficient for the log risk ratio is negative (although zero with high probability), and the variance for the year-specific risk ratio effects ($\tau^2_\alpha$) is estimated to be very small.

%\begin{table}[t]
%\caption{Fixed hyperparameter values.}
%\begin{center}
%\begin{tabular}{|c|c|}
%\hline
%Parameter 	& Value 		\\ \hline \hline
%$c_\beta^2$  	& $10000^2$ 	\\ \hline
%$d_\alpha$ 	& $10$		\\ \hline
%$d_\delta$ 	& $10$		\\ \hline
%\end{tabular}
%\end{center}
%\label{table5}
%\end{table}%

\begin{table}[!t]
\caption{Posterior summaries and credible intervals for parameters in the Bayesian model.}
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
Parameter			& Posterior mean (median)	& 95\% CI lower 	& 95\% CI upper  \\ \hline \hline
$\beta^R_0$ 		& 0.67 (0.67)				& 0.34			& 1.01		   \\ \hline
$\beta^R_1$ 		& -0.09 (-0.09)				& -0.80			& 0.61		   \\ \hline
$\beta^N_0$ 		& -3.14 (-3.13)				& -3.57			& -2.75		   \\ \hline
$\beta^N_1$ 		& -0.42 (-0.42)				& -1.29			& 0.42		   \\ \hline
$\tau^2_\alpha$	& 0.12 (0.08)				& 0.00			& 0.48		   \\ \hline
$\tau^2_\delta$		& 0.42 (0.36)				& 0.13			& 1.09		   \\ \hline
\end{tabular}
\end{center}
\label{table6}
\end{table}%


\begin{figure}[!t]
\begin{center}
\includegraphics[width = \textwidth]{NinoCoefEstimates.pdf}
\caption{Posterior densities and box plots for the Ni\~no regression coefficients.}
\label{figure1}
\end{center}
\end{figure}

\begin{figure}[!t]
\begin{center}
\includegraphics[width = \textwidth]{TausqEstimates.pdf}
\caption{Posterior densities and box plots for the variance parameters.}
\label{figure2}
\end{center}
\end{figure}

\begin{figure}[h]
\begin{center}
\includegraphics[width = 0.85\textwidth]{BayesianCIs_perYr.pdf}
\caption{95\% credible intervals for the year-specific event probabilities under ALL and NAT forcings, as well as for the risk ratios.}
\label{figure3}
\end{center}
\end{figure}

Finally, to more explicitly summarize the overall time-averaged risk ratio, consider the estimated 95\% credible intervals taken from the estimated predictive distributions as described in Section \ref{predDist}, with the Ni\~no 3.4 anomaly value fixed at zero. The credible intervals are given in Table \ref{table7} and the estimated distributions (with credible intervals labelled) are given in Figure \ref{figure4}.

\clearpage

\begin{table}[h]
\caption{Posterior predictive summaries and credible intervals for the time-averaged risk ratio.}
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
Quantity			& Posterior mean (median)	& 95\% CI lower 	& 95\% CI upper 	& CI width  \\ \hline \hline
$\log \overline{RR}$	& 0.67 (0.67)				& -0.12			& 1.46			& 1.58  	\\ \hline
$\overline{RR}$		& 2.11 (1.96)				& 0.89			& 4.29		   	& 3.40	\\ \hline
\end{tabular}
\end{center}
\label{table7}
\end{table}%

\begin{figure}[h]
\begin{center}
\includegraphics[width = \textwidth]{predRR.pdf}
\caption{Predictive distributions for the time-averaged risk ratio.}
\label{figure4}
\end{center}
\end{figure}



\end{document}






