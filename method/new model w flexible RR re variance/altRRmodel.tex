\documentclass[times,12pt]{article}

%=================================================================================
% load packages
\usepackage{times,natbib}
\usepackage{balance}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath, amsthm}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{multirow}
\usepackage[margin=1in]{geometry}
\usepackage{subfigure}
\RequirePackage[colorlinks,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{breakurl}
\usepackage{natbib,upgreek}
\usepackage{bbm}
\usepackage{multirow}
\usepackage{hyperref}
\usepackage{doi}

\DeclareMathOperator{\logit}{logit}
\DeclareMathOperator{\Var}{Var}


%=================================================================================
% define some new commands
\def\argmax{\mathop{\rm argmax}}
\def\argmin{\mathop{\rm argmin}}
\def\arginf{\mathop{\rm arginf}}
\newcommand{\bfsigma}{{\boldsymbol \sigma}}
\newcommand{\bftheta}{{\boldsymbol \theta}}
\newcommand{\bfbeta}{{\boldsymbol \beta}}
\def\bSig\mathbf{\Sigma}
\newcommand{\VS}{V\&S}
\newcommand{\tr}{\mbox{tr}}

%=================================================================================
% formatting
\topmargin=-0.5in \textheight=9in
\sloppy \hyphenpenalty=10000


%=================================================================================
\begin{document}

\doublespacing

%=================================================================================

\section{Model for risk ratio over time} 


\subsection{NAT probabilities}

Because the NAT scenario temperature variable is essentially constant (or at least shows no significant trend over time), we can leave it out of the model and specify
\[
\logit p_{Nt} = \beta_0^N + \alpha_t,
\]
where the $\alpha_t \stackrel{\text{iid}}{\sim} N(0,\tau^2)$. Such a specification is appropriate for the $\alpha_t$ because the $p_N$'s can be thought of as arising from a ``stationary'' climate regime.

\subsection{ALL probabilities}

Now, model
\begin{equation} \label{pA}
\logit p_{At} = \beta_0^A + \beta_1 x_t +\alpha_t + \delta_t,
\end{equation}
where the $\alpha_t$ are as before, $x_t = (\text{ALL mean temp})_t - (\text{NAT mean temp})_t$, and $\delta_t \stackrel{\text{ind}}{\sim} N(0,\sigma_t^2)$. 

\subsection{Implications for the risk}

Using the models given above, and approximating $\logit^{-1}(\cdot) \approx \exp\{ \cdot \}$ (for small values of the $\cdot$ argument), the year-specific risk is 
\[
RR_t \approx \frac{ \exp\{ \beta_0^A + \beta_1 x_t +\alpha_t + \delta_t \} }{\exp\{ \beta_0^N + \alpha_t \}} =  \exp\{ \beta_0^A - \beta_0^N+ \beta_1 x_t + \delta_t \}
\]
\begin{equation} \label{RRt}
=  RR_0 \times  \exp\{ \beta_1 x_t \} \times  \exp\{  \delta_t \}.
\end{equation}
So, the risk is approximately modeled by a ``baseline'' risk ($RR_0 = \exp\{ \beta_0^A - \beta_0^N\}$), which is scaled by anthropogenic influence ($\exp\{ \beta_1 x_t \}$) and also a year-specific random effect ($\exp\{  \delta_t \}$) with time-varying variance ($\sigma^2_t$).

\subsection{Modeling approaches for $\sigma^2_t$}

\subsubsection{Constant variance} \label{consVar}

The simplest approach would be to model $\sigma^2_t \equiv \sigma^2$ as constant over time, as was done previously. The reason for considering alternatives is that if we go far enough back in time (albeit potentially farther back than 1960, the range of our data), at some point there should be no (or at the least, very little) variability in the risk. This approach forces the variance in the $\delta_t$ (i.e., variance in the risk) to be the same for years as different as 1960 and 2013, which might not be appropriate.

\subsubsection{Time period-specific variance}

A simple alternative to Section \ref{consVar} would be to set
\begin{equation} \label{discreteVar}
\sigma^2_t = \left\{
\begin{array}{cl}
\sigma^2_1 & \text{if $t \leq Y$} \\
\sigma^2_2 & \text{if $t  > Y$}
\end{array}
\right.
\end{equation}
where the year $Y$ is chosen such that the overall time period 1960-2013 is separated into two time periods that are expected to have constant variance (i.e., $Y=1980$). This approach gains flexibility and only adds one additional parameter to estimate, but requires the subjective choice of $Y$.

\subsubsection{Covariate scaling, a.k.a. random effect interaction}

Another option would be to put the burden of specifying nonstationarity in $\delta_t$ completely on the covariate $x_t$. That is, set $\sigma^2_t = x_t^2 \sigma^2$, which is equivalent to including the interaction term $x_t\times \delta_t$ in (\ref{pA}). This approach allows the variability to change as a function of the difference in scenario-specific temperature (a factor known to impact changes in risk) and adds no additional parameters to estimate. However, this approach is not overly flexible -- the model forces a fixed (direct) relationship between $x_t$ and the variance of the risk ratio random effect, and forces the variance to be zero when $x_t=0$ (which may or may not be reasonable).

\subsubsection{Log regression for $\sigma_t^2$}

Next, we might use log regression for the variance, i.e.,
\begin{equation} \label{varReg}
\log \sigma_t^2 = \mu_0 + \mu_1 x_t.
\end{equation}
That is,
\[
\Var \delta_t = \sigma_0^2 \cdot \exp\{ \mu_1x_t \},
\]
where $\sigma^2_0 = \exp\{ \mu_0\}$. This model gives a more flexible relationship between $\Var \delta_t$ and $x_t$ and, furthermore, using (\ref{varReg}) yields additional information through inference on $\mu_0$ and $\mu_1$ (e.g., is $\mu_1\neq 0$; in other words, is there a change in the interannual variability in risk due to anthropogenic influences?). Another negative of using the interaction approach $x_t \times \delta_t$: what if anthropogenic influences \textit{decrease} the variability in risk?

\subsubsection{Truncated data}

Finally, we might simply use the original model over a shorter time interval (e.g., 1980-2013), when the interannual variability in risk ratio might more appropriately modeled as constant. However, using all years gives us more information to model the baseline risk parameters as well as variability in the $p_{Nt}$ (by way of the $\alpha_t$). Furthermore, the original model still forces a constant variability in the $\delta_t$ -- even a period like 1980-2013 might have show differing levels of variability in risk, and in any case how is the cutoff year (1980) chosen? -- while other models given above allow for a more flexible variance. And, note that the original model is a special case of (\ref{varReg}): when $\mu_1=0$.













\end{document}






